import React, { Component } from 'react';
import {
    Text,
    View,
    StatusBar,
    Image, StyleSheet, BackHandler, TouchableOpacity, Alert, TextInput, FlatList, DeviceEventEmitter, KeyboardAvoidingView, Switch
} from 'react-native';
import { Colors } from '../assets/Colors';
import { Dims } from '../components/Dims';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as RNLocalize from 'react-native-localize';
import { CustomPicker } from 'react-native-custom-picker';
import nodeEmoji from 'node-emoji';
import { getAllCountries } from 'react-native-country-picker-modal';
import { ScrollView } from 'react-native-gesture-handler';
import LocationServicesDialogBox from 'react-native-android-location-services-dialog-box';
import Geolocation from 'react-native-geolocation-service';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Footer from '../components/Footer';
import Header from '../components/Header';
import Fontisto from 'react-native-vector-icons/Fontisto';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { Card } from 'react-native-shadow-cards';
var moment = require('moment');
var upcomingData = [{
    "id": 1,
    "name": 'Mrs. Rani',
    "age": '25 Years',
    "distance": "2 kms away",
    "time": '2.00 hrs',
    "role": 'Head Nurse',
    "patients": 'Served 300 Patients',
    "image": require('../assets/images/icon1.png')

}, {
    "id": 2,
    "name": 'Mrs. Anjali',
    "age": '29 Years',
    "distance": "5 kms away",
    "time": '2.00 hrs',
    "role": 'Nurse',
    "patients": 'Served 300 Patients',
    "image": require('../assets/images/icon6.png')

}]
class Home extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            options: [],
            locationAddress: params && params.locationAddress ? params.locationAddress : [],
            getData: params && params.getData ? params.getData : [],
            upcomingData: upcomingData,

        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.navigate('Location')
        return true;
    };
    renderSideMenuContent = () => {
        return (
            <>
                <ScrollView>
                    <Text>'Hi'</Text>
                </ScrollView>
            </>
        );
    };
    renderMainContent = () => {
        if (!this.state.drawerOpen) {
            return (
                <View>
                    <Entypo name="menu" size={38} color={CustomColors.Black} />
                </View>
            );
        }
    };
    _renderItem = (item) => {
        return (
            <Card style={{ marginBottom: 10, alignSelf: 'center' }}>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('SubScription')} style={{ flex: 1, flexDirection: 'row', padding: 10 }}>
                    <Image source={item.image} style={{ height: 50, width: 50, }} />
                    <View style={{ flexDirection: 'column', marginHorizontal: 20 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{ fontFamily: 'Poppins-Medium', letterSpacing: 1, }}>{item.name}{'\n'}<Text style={{ fontSize: 10 }}>{item.role}</Text></Text>
                            <Text style={{ alignSelf: 'center', width: 100, textAlign: 'right', fontFamily: 'Poppins-Medium' }}>
                                <Feather
                                    name="phone-call"
                                    size={25}
                                    color={Colors.Primary}
                                    style={{}}
                                /></Text>
                        </View>
                        <View style={{ flexDirection: 'row', top: 10 }}>
                            <Text style={{ alignSelf: 'center', width: 30 }}>
                                <MaterialIcons
                                    name="add-location"
                                    size={20}
                                    color={Colors.lightOrangeColor}
                                    style={{}}
                                />{'    '}</Text>
                            <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 10, alignSelf: 'center' }}>{item.distance}</Text>

                            <Text style={{ alignSelf: 'center', width: wp('40%'), textAlign: 'right', fontSize: 10, fontFamily: 'Poppins-Medium', alignSelf: 'center' }}><Image source={require('../assets/images/like.png')} style={{ height: 10, width: 10 }} />{'  '}{item.patients}
                            </Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </Card>
        )
    }


    render() {
        let { locationAddress, getData } = this.state;
        let locatedAddress = [locationAddress.locality, locationAddress.country, locationAddress.adminArea]
        return (
            <>
                <StatusBar style={{ backgroundColor: Colors.Primary }} />
                <Header navigation={this.props.navigation} listView={true} title={'Nursing Care'} pageName={'Location'} />

                <ScrollView style={{ height: Dims.DeviceHeight }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Card style={{ margin: 20, backgroundColor: 'white', height: 40, }}>
                            <TextInput
                                style={styles.inputText}
                                placeholder={'Search '}
                                returnKeyType="next"
                                placeholderTextColor={Colors.placeholderColor}
                                onChangeText={(email) => this.setState({ email })}
                            />

                        </Card>
                    </View>
                    <FlatList
                        style={{}}
                        keyExtractor={(item, index) => item.id}
                        renderItem={({ item, index }) => this._renderItem(item, index)}
                        data={this.state.upcomingData}
                    />
                    <View style={{ height: 100 }}></View>

                </ScrollView>
                <View style={styles.btn}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Location')} style={[styles.confirmButton, { zIndex: 9999999999999999, flexDirection: 'row', justifyContent: 'center' }]}>
                        <Text style={[styles.loginText, { color: Colors.White }]}><Fontisto
                            name="arrow-swap"
                            size={20}
                            color={'white'}
                            style={{}}
                        /></Text>
                        <Text style={[styles.loginText, { color: Colors.White, marginLeft: 10 }]}>{' '}Map View</Text>

                    </TouchableOpacity>
                </View>
                <Footer navigation={this.props.navigation} discountData={this.state.discountData} nearByData={this.state.nearByShopData} />
                <DropdownAlert
                    ref={(ref) => (this.dropdown = ref)}
                    containerStyle={{
                        backgroundColor: '#FF0000',
                    }}
                    imageSrc={'https://facebook.github.io/react/img/logo_og.png'}
                />
            </>
        );
    }
}

const styles = StyleSheet.create({
    logo: {
        height: 120,
        width: 120,
        borderRadius: 100,
    },
    container: {
        flexDirection: 'row',
    },
    btn: {
        width: 120,
        height: 50,
        alignSelf: 'flex-end',
        marginHorizontal: 20,
        position: 'absolute',
      
        top: Dims.DeviceHeight/1.25,
        right:30
    },
    confirmButton: {
        width: wp('35%'),
        paddingVertical: 10,
        borderRadius: 5,
        backgroundColor: Colors.Primary,
    },
});

export default Home;
