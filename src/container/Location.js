import React, { Component } from 'react';
import { Text, TextInput, Button, ActivityIndicator, DeviceEventEmitter, Platform, View, BackHandler, FlatList, StatusBar, ImageBackground, TouchableNativeFeedback, SafeAreaView, Keyboard, Alert, KeyboardAvoidingView, PermissionsAndroid, StyleSheet, Image, TouchableOpacity, Dimensions, ViewPropTypes } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import MapView, {
    AnimatedRegion,
    PROVIDER_GOOGLE,
    Marker,
} from "react-native-maps";
import { Colors } from '../assets/Colors';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { FontAwesome } from 'react-native-vector-icons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import { Dims } from '../components/Dims';
import Geolocation from 'react-native-geolocation-service';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
const googleApiKey = 'AIzaSyAFn3Q1LhhC0C8pfWhNyHwfHWYFT7S2s_M';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

import FloatingLabel from 'react-native-floating-labels';
import Fontisto from 'react-native-vector-icons/Fontisto';
import { ScrollView } from 'react-native-gesture-handler';
import LocationServicesDialogBox from 'react-native-android-location-services-dialog-box';
import Geocoder from 'react-native-geocoder';
import Footer from '../components/Footer';
import { Card } from 'react-native-shadow-cards';
import { color } from 'react-native-reanimated';
import SplashScreen from 'react-native-splash-screen';
import DeviceInfo from 'react-native-device-info';
import RtcEngine, {
    ChannelProfile,
    ClientRole,
    RtcEngineContext, RtcLocalView, RtcRemoteView
} from 'react-native-agora';
import Item from '../common/item';
import { HttpHelper } from '../HelperApi/Api/HTTPHelper';
import { AgoraUrl, AgoraRetriveUrl } from '../HelperApi/Api/APIConfig';
import "react-native-get-random-values";
import { v4 as uuid } from "uuid";
const ASPECT_RATIO = Dims.DeviceWidth / Dims.DeviceHeight;
const LATITUDE_DELTA = 0.039999;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const CARD_HEIGHT = Dims.DeviceHeight / 6;
const CARD_WIDTH = CARD_HEIGHT - 50;
var initialRegion;
var markerRegion;



class map extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        console.log(params, 'pas');
        this.state = {

            markerData: {
                latitude: 35.1790507,
                longitude: -6.1389008,
            },
            keyboardState: 'closed',
            getAddress: [],
            isChangeLocation: false,
            changeData: [],
            address1: '',
            address2: '',
            city: '',
            state: '',
            pinCode: '',
            getDataAddress: [],
            getIconData: params && params.getBookData ? params.getBookData : [],
            selectedData: [],
            rippleColor: Colors.blueColor,
            rippleOverflow: false,
            appId: '49c0dbb169294ddca113f24be7f2d848',
            token: '00649c0dbb169294ddca113f24be7f2d848IADONZuXud8r/Ow/LPgMjZOYex96SGTjARfetTWii86FHgx+f9gAAAAAEAC7nPWLRor2YQEAAQBGivZh',
            channelName: 'test',
            joinSucceed: false,
            peerIds: [],
            uid: 123456,
            isJoined: false,
            openMicrophone: true,
            enableSpeakerphone: true,
            playEffect: false,
            channelId: '',
            getTokenData: {},
            _remoteUid: null,
            type: ""

        };
    }

    componentDidMount() {
        SplashScreen.hide();
        // to do for ios permission
        this.requestCameraAndAudioPermission()
        this.showLocationPopup()
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
        this.createUUID()
        this._initEngine();
    }
    UNSAFE_componentWillMount() {

    }

    async requestCameraAndAudioPermission() {
        try {
            const granted = await PermissionsAndroid.requestMultiple([
                PermissionsAndroid.PERMISSIONS.CAMERA,
                PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
            ]);
            if (
                granted["android.permission.RECORD_AUDIO"] === PermissionsAndroid.RESULTS.GRANTED &&
                granted["android.permission.CAMERA"] === PermissionsAndroid.RESULTS.GRANTED
            ) {
                console.log("You can use the cameras & mic");
            } else {
                console.log("Permission denied");
            }
        } catch (err) {
            console.warn(err);
        }
    }
    createUUID = () => {
        let createUid = uuid();
        console.log(createUid, 'createUid');

    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
        SplashScreen.hide();
        this._engine?.destroy();
    }


    _initEngine = async () => {
        let { appId } = this.state;
        this._engine = await RtcEngine.create(appId)

        await this._engine.enableAudio();
        this._addListeners();
        await this._engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
        await this._engine.setClientRole(ClientRole.Broadcaster);
    };

    _addListeners = () => {
        let { type } = this.state;
        this._engine.addListener('Warning', (warningCode) => {
            console.info('Warning', warningCode);
        });
        this._engine.addListener('Error', (errorCode) => {
            console.info('Error', errorCode);
        });
        this._engine.addListener('UserJoined', (uid, elapsed) => {
            console.log('UserJoined', uid, elapsed);
            // Get current peer IDs
            const { peerIds, _remoteUid } = this.state;
            // If new user
            if (peerIds.indexOf(uid) === -1) {
                this.setState({
                    // Add peer ID to state array
                    peerIds: [...peerIds, uid],
                    _remoteUid: uid
                }, () => {
                    this.props.navigation.navigate('CallScreen', { type: type, uuid: this.state._remoteUid, status: this.state.isJoined })

                });
            }
        });
        this._engine.addListener('UserOffline', (uid, reason) => {
            console.log('UserOffline', uid, reason);
            const { peerIds } = this.state;
            this.setState({
                // Remove peer ID from state array
                peerIds: peerIds.filter((id) => id !== uid),
                _remoteUid: null
            });
        });
        this._engine.addListener('JoinChannelSuccess', (channel, uid, elapsed) => {
            console.info('JoinChannelSuccess', channel, uid, elapsed);
            this.setState({ isJoined: true, isSelect: false }, () => {

                this.props.navigation.navigate('CallScreen', { type: type, uuid: this.state._remoteUid, status: this.state.isJoined })
            });
        });
        this._engine.addListener('LeaveChannel', (stats) => {
            console.info('LeaveChannel', stats);
            this.setState({ isJoined: false });
        });
    };

    _joinChannel = async () => {
        this.generateToken()
        let { getTokenData, token, channelName, uid } = this.state;

        // if (Platform.OS === 'android') {
        //     await PermissionsAndroid.request(
        //         PermissionsAndroid.PERMISSIONS.RECORD_AUDIO
        //     );
        // }
    };
    generateToken = (type) => {
        console.log(type, 'type');
        let obj = {
            "providerId": "8056292394145939571",
            "customerId": "C1354684"
        }
        obj = JSON.stringify(obj)
        if (type === "create") {
            let getToken = HttpHelper(AgoraUrl, 'POST', obj);
            getToken.then(response => {
                console.log(response, 'getToken');
                if (response) {
                    this.setState({
                        getTokenData: response,
                        type: type
                    }, () => {
                        let { getTokenData } = this.state;
                        console.log(getTokenData, 'getTokenData');
                        this._engine.joinChannel(
                            getTokenData.token,
                            getTokenData.channelName,
                            getTokenData.role,
                            getTokenData.uid
                        );
                    })
                }
            })
        } else {
            let getToken = HttpHelper(AgoraRetriveUrl, 'GET', '');
            getToken.then(response => {
                console.log(response, 'retriveToken');
                if (response) {
                    this.setState({
                        getTokenData: response,
                        type: type
                    }, () => {
                        let { getTokenData } = this.state;
                        this._engine.joinChannel(
                            getTokenData.token,
                            getTokenData.channelName,
                            getTokenData.role,
                            getTokenData.uid
                        );
                    })
                }
            })
        }
    }
    _onChangeRecordingVolume = (value) => {
        this._engine?.adjustRecordingSignalVolume(value * 400);
    };

    _onChangePlaybackVolume = (value) => {
        this._engine?.adjustPlaybackSignalVolume(value * 400);
    };

    _toggleInEarMonitoring = (isEnabled) => {
        this._engine?.enableInEarMonitoring(isEnabled);
    };

    _onChangeInEarMonitoringVolume = (value) => {
        this._engine?.setInEarMonitoringVolume(value * 400);
    };

    _leaveChannel = async () => {
        await this._engine?.leaveChannel();
        this.setState({
            isModalVisible: true,
            isOpacity: true,
        }, () => {
            this.props.navigation.navigate('OrderStatus', { getData: this.state.selectedData, isModal: true })

        })
    };

    _switchMicrophone = () => {
        const { openMicrophone } = this.state;
        this._engine
            ?.enableLocalAudio(!openMicrophone)
            .then(() => {
                this.setState({ openMicrophone: !openMicrophone });
            })
            .catch((err) => {
                console.warn('enableLocalAudio', err);
            });
    };

    _switchSpeakerphone = () => {
        const { enableSpeakerphone } = this.state;
        this._engine
            ?.setEnableSpeakerphone(!enableSpeakerphone)
            .then(() => {
                this.setState({ enableSpeakerphone: !enableSpeakerphone });
            })
            .catch((err) => {
                console.warn('setEnableSpeakerphone', err);
            });
    };
    _switchEffect = () => {
        const { playEffect } = this.state;
        if (playEffect) {
            this._engine
                ?.stopEffect(1)
                .then(() => {
                    this.setState({ playEffect: false });
                })
                .catch((err) => {
                    console.warn('stopEffect', err);
                });
        } else {
            this._engine
                ?.playEffect(
                    1,
                    Platform.OS === 'ios'
                        ? `${RNFS.MainBundlePath}/Sound_Horizon.mp3`
                        : '/assets/Sound_Horizon.mp3',
                    -1,
                    1,
                    1,
                    100,
                    true,
                    0
                )
                .then(() => {
                    this.setState({ playEffect: true });
                })
                .catch((err) => {
                    console.warn('playEffect', err);
                });
        }
    };
    showLocationPopup() {
        LocationServicesDialogBox.checkLocationServicesIsEnabled({
            message:
                "<h2 color='#eeeeee'>MedXForce would like to access your location?</h2>This app wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/>",
            ok: 'YES',
            cancel: 'NO',
            enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
            showDialog: true, // false => Opens the Location access page directly
            openLocationServices: true, // false => Directly catch method is called if location services are turned off
            preventOutSideTouch: false, //true => To prevent the location services popup from closing when it is clicked outside
            preventBackClick: false, //true => To prevent the location services popup from closing when it is clicked back button
            providerListener: true, // true ==> Trigger "locationProviderStatusChange" listener when the location state changes
        })
            .then(
                function (success) {
                    if (success.status === 'enabled') {
                        this.requestPermissions();
                    } else {
                        // this.getAuthorizedLogin();
                        // this.interval = setInterval(() => this.onNavigate(), 3000);
                    }
                    // success => {alreadyEnabled: true, enabled: true, status: "enabled"}
                    // this.getCurrentLocation();
                }.bind(this),
            )
            .catch(error => {
            });
        BackHandler.addEventListener('hardwareBackPress', () => {
            //(optional) you can use it if you need it
            //do not use this method if you are using navigation."preventBackClick: false" is already doing the same thing.
            LocationServicesDialogBox.forceCloseDialog();
        });
        DeviceEventEmitter.addListener('locationProviderStatusChange', function (
            status,
        ) {
            // only trigger when "providerListener" is enabled
            console.log(status); //  status => {enabled: false, status: "disabled"} or {enabled: true, status: "enabled"}
        });
    }
    _keyboardDidShow = () => {
        this.setState({ keyboardState: 'opened' });
    }

    _keyboardDidHide = () => {
        this.setState({ keyboardState: 'closed' });
    }

    handleBackPress = () => {
        this.props.navigation.navigate('Home')
        return true;
    };
    async requestPermissions() {
        try {
            const granted = await PermissionsAndroid.requestMultiple(
                [PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, PermissionsAndroid.PERMISSIONS.RECORD_AUDIO],
            );

            this.getCurrentLocation();
        } catch (err) {

        }
    }
    getCurrentLocation() {
        Geolocation.getCurrentPosition(
            position => {
                console.log(position, 'llll');
                initialRegion = {
                    latitude: 17.3850,
                    longitude: 78.4867,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                };
                // initialRegion = {
                //     latitude: position.coords.latitude,
                //     longitude: position.coords.longitude,
                //     latitudeDelta: LATITUDE_DELTA,
                //     longitudeDelta: LONGITUDE_DELTA,
                // };
                // markerRegion = {
                //     latitude: Number(position.coords.latitude),
                //     longitude: Number(position.coords.longitude),
                // };
                markerRegion = {
                    latitude: Number(17.3850),
                    longitude: Number(78.4867),
                };
                this.setState({
                    mapData: initialRegion,
                    isLoading: false,
                    markerData: markerRegion,
                }, () => {
                    this.gettingLatLng()
                });

            },
            error => console.log(error),
            { enableHighAccuracy: false, timeout: 5000, distanceFilter: 400 },
        );
    }
    gettingLatLng = (initialRegion) => {
        var NY = {
            lat: this.state.mapData.latitude,
            lng: this.state.mapData.longitude
        };
        Geocoder.geocodePosition(NY).then(res => {
            if (res) {
                this.setState({
                    getDataAddress: res ? res[0] : [],
                    isCurrent: true
                })
            } else {
            }
        })
            .catch(err => console.log(err))

    }
    handleRegionChange = mapData => {
        this.setState({
            markerData: { latitude: mapData.latitude, longitude: mapData.longitude },
            mapData,
        });
    };
    onHandleAddress = (shopAddress) => {
        this.setState({ shopAddress: shopAddress, }, () => {
            var url =
                'https://maps.googleapis.com/maps/api/geocode/json?address=' + shopAddress + '&key=' +
                googleApiKey;
            fetch(url)
                .then(response => response.json())
                .then(responseJson => {
                    if (responseJson && responseJson.results[0] && responseJson.results[0].geometry) {
                        this.setState({
                            region: {
                                latitude: responseJson.results[0].geometry.location.lat,
                                longitude: responseJson.results[0].geometry.location.lng,
                                latitudeDelta: LATITUDE_DELTA,
                                longitudeDelta: LONGITUDE_DELTA,
                            },
                            isManualAddress: true
                        })
                    }
                })
        })
    }
    selectAddress = (e) => {
        var dataLatLng = {
            lat: Number(e.holder.location.lattitude),
            lng: Number(e.holder.location.longitude)
        };
        Geocoder.geocodePosition(dataLatLng).then(responseJson => {
            if (responseJson) {
                this.setState(
                    {
                        selectAddress: responseJson && responseJson[0] ? responseJson[0] : '',
                        getDataAddress: responseJson && responseJson[0] ? responseJson[0] : [],
                        isSelect: e.id === "6" ? false : true,
                        selectedData: e,
                        rippleColor: Colors.redColor,
                        rippleOverflow: !this.state.rippleOverflow
                    });
            } else {
            }
        })
            .catch(err => console.log(err))
    }
    onLocationHandle = (mapData, address) => {
        let obj = {
            "lat": this.state.mapData && this.state.mapData != null && this.state.mapData.latitude ? String(this.state.mapData.latitude) : '',
            "lng": this.state.mapData && this.state.mapData != null && this.state.mapData.longitude ? String(this.state.mapData.longitude) : '',
            "distance": 5
        }
        var locBody = JSON.stringify(obj)
        let locationData = HttpHelper(LocationUrl, 'POST', locBody);
        locationData.then(locationResponse => {
            this.selectAddress({
                modalData: {
                    streetName: address
                }
            })
            this.state.modalData.streetName = address
            if (this.state.isAddress) {
                this.props.navigation.navigate('Address', { locationAddress: address, mapData: mapData, isModalVisible: true, modalData: this.state.modalData })
            } else {
                this.props.navigation.navigate('DeliveryAddress', { locationAddress: address, mapData: mapData })
            }
        })
    }
    handleChange = (event, type) => {
        if (type === "houseNo") {
            this.setState({ houseNo: event.nativeEvent.text });
        } else if (type === "address1") {
            this.setState({ address1: event.nativeEvent.text });
        } else if (type === "address2") {
            this.setState({ address2: event.nativeEvent.text });
        } else if (type === "city") {
            this.setState({ city: event.nativeEvent.text });
        } else if (type === "state") {
            this.setState({ state: event.nativeEvent.text });
        } else if (type === "pincode") {
            this.setState({ pincode: event.nativeEvent.text });
        } else if (type === "save") {
            this.setState({ save: event.nativeEvent.text });
        }
    };

    onPressZoomIn() {
        this.region = {
            latitude: this.state.mapData.latitude,
            longitude: this.state.mapData.longitude,
            latitudeDelta: this.state.mapData.latitudeDelta / 5,
            longitudeDelta: this.state.mapData.longitudeDelta / 5
        }
        this.setState({
            mapData: {
                latitudeDelta: this.region.latitudeDelta,
                longitudeDelta: this.region.longitudeDelta,
                latitude: this.region.latitude,
                longitude: this.region.longitude
            }
        })
        this.map.animateToRegion(this.region, 100);
    }

    onPressZoomOut() {
        this.region = {
            latitude: this.state.mapData.latitude,
            longitude: this.state.mapData.longitude,
            latitudeDelta: this.state.mapData.latitudeDelta * 5,
            longitudeDelta: this.state.mapData.longitudeDelta * 5
        }
        this.setState({
            mapData: {
                latitudeDelta: this.region.latitudeDelta,
                longitudeDelta: this.region.longitudeDelta,
                latitude: this.region.latitude,
                longitude: this.region.longitude
            }
        })
        this.map.animateToRegion(this.region, 100);
    }
    _renderItem = (item, index) => {
        return (
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <View style={{ flex: 0.8 }}>
                    <View style={{ flexDirection: 'row', paddingVertical: 5 }}>
                        <Text style={{}}>
                            <Icon
                                name="add-location"
                                size={30}
                                color={Colors.lightOrangeColor}
                                style={{}}
                            /></Text>
                        <Text style={{ fontFamily: 'Poppins-Bold', letterSpacing: 2 }}>{item.postalCode}</Text>
                    </View>
                    <View style={{ left: 10 }}>
                        <Text style={{ fontFamily: 'Poppins-Medium', letterSpacing: 1 }}>{item}</Text>
                    </View>
                </View>
                <View>
                    <TouchableOpacity onPress={() => { this.gettingData(item) }}>
                        <Text style={{ fontFamily: 'Poppins-Medium', letterSpacing: 0.5, color: Colors.Primary }}>CHANGE</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    gettingData = () => {
        let { getDataAddress } = this.state;
        if (getDataAddress) {
            this.setState({
                isChangeLocation: !this.state.isChangeLocation,
                houseNo: getDataAddress ? getDataAddress.streetNumber : '',
                address1: getDataAddress ? getDataAddress.streetName : '',
                address2: '',
                city: getDataAddress ? getDataAddress.locality : '',
                state: getDataAddress ? getDataAddress.adminArea : '',
                pincode: getDataAddress ? getDataAddress.postalCode : '',
                save: '',
            })
        }
    }
    closeCancelModal = () => {
        this.setState({
            isSelect: false
        })
    }

    relocatingAddress = () => {
        let { getDataAddress, address1, address2, city, state, pincode, save, houseNo } = this.state;
        if (address1 && city && state && pincode && save && houseNo) {
            getDataAddress.streetNumber = houseNo,
                getDataAddress.streetName = address1 + address2,
                getDataAddress.locality = city,
                getDataAddress.adminArea = state,
                getDataAddress.postalCode = pincode,
                this.setState({
                    getDataAddress,
                    isChangeLocation: false,
                })
        } else {
            this.dropdown.alertWithType('error', 'Error!', 'Please Enter All Fields');
        }
    }
    startCall = async () => {
        let { getTokenData } = this.state;
        // Join Channel using null token and channel name
        this._engine.joinChannel(
            getTokenData.token,
            getTokenData.channel,
            null,
            getTokenData.uid
        );
    };
    onHandleConfirm = (type) => {
        let { selectedData } = this.state;

        this.generateToken(type)
        // this.startBasicCall()
        // if (selectedData.gender === "Male") {
        //     this.props.navigation.navigate('OrderStatus', { getData: this.state.selectedData })
        // }
    }

    render() {
      
        let { getAddress, getDataAddress, locationAddress, selectedData, rippleColor, rippleOverflow } = this.state
        let locatedAddress = [getDataAddress.locality, getDataAddress.adminArea]
        return (
            <>
                <StatusBar
                    backgroundColor={Colors.Primary}
                    barStyle="light-content"
                />
                <SafeAreaView backgroundColor={Colors.Primary}></SafeAreaView>


                <View style={{ flex: 1 }}>
                    <View style={{ height: 70, backgroundColor: Colors.Primary, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text onPress={this.handleBackPress} style={{ alignSelf: 'center', marginLeft: 10 }}>
                            <AntDesign
                                name="menu-unfold"
                                size={25}
                                color={Colors.White}
                                style={{}}
                            /></Text>
                        <View style={{ flexDirection: 'column', paddingVertical: 5 }}>
                            <Text style={{ textAlign: 'center', color: Colors.White, fontFamily: 'Poppins-Medium', fontSize: 10 }}>Current Location</Text>
                            <Text numberOfLines={2} style={{ maxWidth: 220, textAlign: 'center', color: Colors.White, fontFamily: 'Poppins-Medium', fontSize: 14 }}>{locatedAddress ? locatedAddress.join(',') : 'Hyderabad, India'}</Text>
                        </View>
                        <View style={{ flexDirection: 'column', marginRight: 10, alignSelf: 'center' }}>
                            <Text style={{ textAlign: 'center' }}>
                                <FontAwesome5
                                    name="headset"
                                    size={20}
                                    color={Colors.White}
                                    style={{}}
                                /></Text>
                            <Text style={{ textAlign: 'center', color: Colors.White, fontFamily: 'Poppins-Medium', fontSize: 10 }}>Help me</Text>
                        </View>
                    </View>

                    <MapView
                        style={this.state.isSelect ? { height: Dims.DeviceHeight / 1.3, } : { height: Dims.DeviceHeight, top: -10 }}
                        provider={PROVIDER_GOOGLE}
                        showsUserLocation={true}
                        showsMyLocationButton={true}
                        zoomEnabled={true}
                        scrollEnabled={true}
                        initialRegion={this.state.mapData}
                        onMapReady={this.onMapReady}
                        onRegionChangeComplete={this.handleRegionChange}
                        maxZoomLevel={5.3}
                        title={this.state.selectAddress ? this.state.selectAddress.formattedAddress : ''}
                        ref={ref => {
                            this.map = ref;
                        }}>
                        {this.state.getIconData && this.state.getIconData.map((v) => {
                            console.log(v, 'kkk');
                            return (
                                <>
                                    <MapView.Marker coordinate={{ "latitude": Number(v.holder.location.lattitude), "longitude": Number(v.holder.location.longitude) }}
                                        key={v.id}
                                        background={TouchableNativeFeedback.Ripple(rippleColor, rippleOverflow)}
                                        title={this.state.selectAddress ? this.state.selectAddress.formattedAddress : ''}
                                        onPress={() => this.selectAddress(v)}>
                                        <View style={selectedData.id === v.id ? { borderWidth: 1.1, borderRadius: 400, borderColor: Colors.Primary } : { borderWidth: 1, borderRadius: 400, borderColor: Colors.placeholderColor }}>
                                            <Image style={{ height: 100, width: 100, opacity: 0.3 }}
                                                source={{ uri: `data:image/jpeg;base64,${v.holder.profilePic}` }} />
                                        </View>

                                        <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 8, top: -5 }}><Icon
                                            name="add-location"
                                            size={10}
                                            color={'orange'}
                                            style={{}}
                                        />{2} Km Away</Text>

                                    </MapView.Marker>
                                </>
                            )
                        })}
                    </MapView>
                    <Card style={{ marginHorizontal: 20, zIndex: 9999999999999999, backgroundColor: 'white', height: 40, position: 'absolute', marginTop: 120, top: 0, }}>
                        <TextInput
                            style={styles.inputText}
                            placeholder={'Search Nearby Medical Travel Assistent'}
                            returnKeyType="next"
                            placeholderTextColor={Colors.placeholderColor}
                            onChangeText={(email) => this.setState({ email })}
                        />
                    </Card>
                    <View style={styles.btn}>
                        <TouchableOpacity style={[styles.confirmButton, { flexDirection: 'row', justifyContent: 'center' }]}>
                            <Text style={[styles.loginText, { color: Colors.White }]}><Fontisto
                                name="arrow-swap"
                                size={20}
                                color={'white'}
                                style={{}}
                            /></Text>
                            <Text style={[styles.loginText, { color: Colors.White, marginLeft: 10 }]}>{' '}List View</Text>

                        </TouchableOpacity>
                    </View>
                    {this.state.isSelect && (
                        <View style={{ height: 250, position: 'absolute', width: Dims.DeviceWidth, paddingHorizontal: 10, borderTopLeftRadius: 30, borderTopRightRadius: 30, backgroundColor: Colors.White, bottom: 100 }}>
                            <TouchableOpacity onPress={() => { this.closeCancelModal() }} style={{ alignItems: 'flex-end', margin: 10 }}>
                                <AntDesign color={Colors.Primary} size={22} name="closecircle" />
                            </TouchableOpacity>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderRadius: 200 }}>
                                <View style={{ flex: 0.3 }}>
                                    <Image style={{ height: 80, width: 80 }} resizeMode={'contain'}
                                        source={{ uri: `data:image/jpeg;base64,${selectedData.holder.profilePic}` }} />
                                </View>
                                <View style={{ flex: 1, flex: 0.7 }}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <View style={{ flex: 0.5 }}>
                                            <Text numberOfLines={1} style={{ fontFamily: 'Poppins-Bold', fontSize: 16, color: Colors.Primary, letterSpacing: 1 }}>{selectedData.title}{selectedData.firstName}</Text>
                                            <Text numberOfLines={1} style={{ fontFamily: 'Poppins-Medium', fontSize: 10, }}>{selectedData.service}</Text>
                                        </View>
                                        <View style={{ flex: 0.4 }}>
                                            <Text style={{ fontFamily: 'Poppins-Medium ', fontSize: 16, letterSpacing: 1 }}><Text style={{ color: Colors.Primary }}>{'\u20B9'}</Text>{' '}{selectedData.holder.location.minPricePerVisit}/- Visit</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 5 }}>
                                        <View style={{ flexDirection: 'row', flex: 0.45 }}>
                                            <Image style={{ height: 15, width: 15 }}
                                                source={require('../assets/images/favourites.png')} />
                                            <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 8, textAlign: 'center', top: 1, left: 5 }}>15 years of Experience</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', flex: 0.45 }}>
                                            <Image style={{ height: 14, width: 14 }}
                                                source={require('../assets/images/like.png')} />
                                            <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 8, textAlign: 'center', top: 1, left: 5 }}>served 300 patients</Text>
                                        </View>
                                        <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 10, flex: 0.1 }}></Text>
                                    </View>
                                    <View style={{ borderWidth: 1, borderColor: Colors.greyColor, height: 0.2, right: 10 }}></View>
                                </View>
                                <View>
                                </View>
                            </View>
                            <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 12, padding: 10 }}>{selectedData ? selectedData.description : ''}</Text>
                            <View style={{
                                width: Dims.DeviceWidth - 50,
                                height: 50,
                                alignSelf: 'center',
                                marginHorizontal: 20,

                            }}>
                                <TouchableOpacity onPress={() => this.onHandleConfirm('create')} style={{
                                    paddingVertical: 10,
                                    borderRadius: 2,
                                    backgroundColor: Colors.Primary, borderRadius: 10,
                                }}>
                                    <Text style={[styles.loginText, { color: Colors.White, fontFamily: 'Poppins-Bold' }]}>Call Now</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.onHandleConfirm('join')} style={{
                                    paddingVertical: 10,
                                    borderRadius: 2,
                                    backgroundColor: Colors.Primary, borderRadius: 10,
                                    marginTop: 10
                                }}>
                                    <Text style={[styles.loginText, { color: Colors.White, fontFamily: 'Poppins-Bold' }]}>Join Now</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    )}
                    <Footer navigation={this.props.navigation} discountData={this.state.discountData} nearByData={this.state.nearByShopData} />

                </View>
                <DropdownAlert
                    ref={(ref) => (this.dropdown = ref)}
                    containerStyle={{ backgroundColor: '#FF0000' }}
                    imageSrc={'https://facebook.github.io/react/img/logo_og.png'}
                />
            </>
        );

    };
}
export default map;
const styles = StyleSheet.create({
    deliver: {
        position: 'relative',
        bottom: 0,
        backgroundColor: Colors.Primary,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        height: Dims.DeviceHeight / 2.5,
        width: Dims.DeviceWidth,
        // top:-30
        marginTop: -40
    },
    deliver1: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        top: 30,
        backgroundColor: Colors.Primary,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        height: Dims.DeviceHeight / 0.2,
        width: Dims.DeviceWidth,
        marginBottom: 100
    },
    location: {
        position: 'relative',
        bottom: 0,
        backgroundColor: Colors.redColor,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        width: Dims.DeviceWidth,
    },
    location1: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        top: 30,
        backgroundColor: Colors.Primary,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        height: 100,
        width: Dims.DeviceWidth,
        marginBottom: 100
    },
    loginBtn: {
        width: Dims.DeviceWidth - 50,
        paddingVertical: 10,
        borderRadius: 2,
        backgroundColor: Colors.white
    },
    confirmButton: {
        width: Dims.DeviceWidth - 250,
        paddingVertical: 10,
        borderRadius: 2,
        backgroundColor: Colors.Primary,
        marginTop: -70
    },
    container2: {
        alignSelf: 'center'
    },
    gradient: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 7,
    },
    btn: {
        width: 170,
        height: 50,
        alignSelf: 'flex-end',
        marginHorizontal: 20
    },
    loginText: {
        color: Colors.Primary,
        alignSelf: 'center',
        // justifyContent: 'center',
        fontFamily: 'Poppins-Medium',
        letterSpacing: 3,
        textAlign: 'center',
    },
    closePanel: {
        flexWrap: 'wrap',
        // top: 20

    },
    labelStyle: {
        color: Colors.textColor,
        fontSize: 10,
        fontFamily: 'Poppins-Bold',
        borderBottomColor: Colors.White,
        letterSpacing: 1
    },
    floatingLabelView: {
        marginHorizontal: 20,
    },
    hidePasswordText: {
        fontSize: 16,
        textAlign: 'center', fontFamily: 'Poppins-Medium',
        textAlignVertical: 'center',
        // color:Colors.placeholderColor
        fontSize: 16,

    },
    inputStyle: {
        borderWidth: 0,
        // margin: 10,
        height: 45,
        fontFamily: 'Poppins-Medium',
        fontSize: 12,
        borderBottomColor: Colors.white,
        borderBottomWidth: 1,
        color: Colors.White

    }, busIcon: {
        // paddingTop: 10,
        color: Colors.Primary
    },
    inputText: {
        fontFamily: 'Poppins-Medium',
        left: 10,
        top: 5
    },
    max: {
        flex: 1,
    },
    buttonHolder: {
        height: 100,
        alignItems: 'center',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    button: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        backgroundColor: '#0093E9',
        borderRadius: 25,
    },
    buttonText: {
        color: '#fff',
    },
    fullView: {
        width: Dims.DeviceWidth,
        height: Dims.DeviceHeight - 100,
    },
    remoteContainer: {
        width: '100%',
        height: 150,
        position: 'absolute',
        top: 5
    },
    remote: {
        width: 150,
        height: 150,
        marginHorizontal: 2.5
    },
    noUserText: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        color: '#0093E9',
    },
    container: {
        flex: 1,
    },
    float: {
        width: '100%',
        position: 'absolute',
        alignItems: 'center',
        bottom: 20,
    },
    top: {
        width: '100%',
    },
    input: {
        borderColor: 'gray',
        borderWidth: 1,
    },

    fullscreen: {
        width: Dims.DeviceWidth,
        height: Dims.DeviceHeight,
    },
});