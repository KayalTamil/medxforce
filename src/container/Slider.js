import React, { Component, useState } from 'react';
import {
    Text,
    View,
    StatusBar,
    Image, StyleSheet, BackHandler, TouchableOpacity, Alert, TextInput, FlatList, DeviceEventEmitter, KeyboardAvoidingView, Switch, requireNativeComponent
} from 'react-native';
import { Colors } from '../assets/Colors';
import { Dims } from '../components/Dims';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as RNLocalize from 'react-native-localize';
import { CustomPicker } from 'react-native-custom-picker';
import nodeEmoji from 'node-emoji';
import { getAllCountries } from 'react-native-country-picker-modal';
import { ScrollView } from 'react-native-gesture-handler';
import LocationServicesDialogBox from 'react-native-android-location-services-dialog-box';
import Geolocation from 'react-native-geolocation-service';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Footer from '../components/Footer';
import { Card } from 'react-native-shadow-cards';
import Swiper from 'react-native-swiper';
import SplashScreen from 'react-native-splash-screen';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Entypo from 'react-native-vector-icons/Entypo';
import OTPTextView from 'react-native-otp-textinput';

var moment = require('moment');
var sliderData = [{
    id: '1',
    latitude: 17.4317,
    longitude: 78.5654,
    km: 6,
    name: 'Mrs. Tamizh',
    image: require("../assets/images/home1.png"),
    appointmentDate: '10/01/2022',
    refNo: '#202209501',
    gender: 'Female',
    role: 'Head Nurse Medical Travel Assistant',
    description: "She is friendly, caring and punctual."
},
{
    id: '2',
    latitude: 17.4657,
    longitude: 78.4283,
    km: 10,
    name: 'Mrs. Srinija',
    image: require("../assets/images/home2.jpg"),
    appointmentDate: '12/01/2022',
    refNo: '#202209502',
    gender: 'Female',
    role: 'Head Nurse',
    description: "She is friendly, caring and punctual"
},
{
    id: '3',
    latitude: 17.3990,
    longitude: 78.4867,
    km: 2,
    name: 'Mr. Arjun',
    image: require("../assets/images/home3.png"),
    appointmentDate: '09/01/2022',
    refNo: '#202209503',
    gender: 'Male',
    role: "Medical Travel Assistant",
    description: "He is friendly, caring and punctual."
},

]


class Home extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            options: [],
            locationAddress: params && params.locationAddress ? params.locationAddress : [],
            getData: params && params.getData ? params.getData : [],
            getBannerImage: sliderData,
            isLogin:true,
            isOtpVerify:false,
            timer: null,
            counter: 30

        };
    }

    componentDidMount() {
        SplashScreen.hide();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        let timer = setInterval(this.tick, 1000);
        this.setState({ timer });
        const deviceCountry = RNLocalize.getCountry();
        getAllCountries().then(cl => {
            this.setState({ options: cl });
            var userCountryData = cl
                .filter(country => country.name === "India")
                .pop();
            this.setState({ value: userCountryData }, () => {
            });
            // this.props.onPickerLoad(userCountryData);
        });
    }

    componentWillUnmount() {
        SplashScreen.hide();
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.navigate('Location')
        return true;
    };
    tick = () => {
        this.setState({
            counter: this.state.counter === 0 ? this.state.counter : this.state.counter - 1
        });
    }
    renderSideMenuContent = () => {
        return (
            <>
                <ScrollView>
                    <Text>'Hi'</Text>
                </ScrollView>
            </>
        );
    };
    renderMainContent = () => {
        if (!this.state.drawerOpen) {
            return (
                <View>
                    <Entypo name="menu" size={38} color={Colors.Black} />
                </View>
            );
        }
    };
    getBannerImage = () => {
        let { getBannerImage } = this.state;
        if (getBannerImage && getBannerImage.length > 0) {
            return (
                <View><Text>Hi</Text></View>
            )
        }
    }
    _renderItem = (item) => {
        return (
            <Card style={{ marginBottom: 10 }}>
                <Text style={{ textAlign: 'right', color: Colors.Primary, fontFamily: 'Poppins-Medium', marginRight: Dims.DeviceWidth * 0.020, top: 10, fontSize: 12, paddingHorizontal: 10 }}>in {item.time}</Text>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('SubScription')} style={{ flex: 1, flexDirection: 'row', padding: 10 }}>
                    <Text style={{ alignSelf: 'center', width: 50 }}>
                        <FontAwesome
                            name="user-circle"
                            size={30}
                            color={Colors.placeholderColor}
                            style={{}}
                        /></Text>
                    <View style={{ flexDirection: 'column', }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ fontFamily: 'Poppins-Medium', letterSpacing: 1, }}>{item.name} <Text style={{ color: Colors.greyColor }}>Age: {item.age}</Text></Text>
                        </View>
                        <View style={{ flexDirection: 'row', top: 10 }}>
                            <Text style={{ alignSelf: 'center', width: 30 }}>
                                <MaterialIcons
                                    name="add-location"
                                    size={25}
                                    color={Colors.lightOrangeColor}
                                    style={{}}
                                />{'    '}</Text>
                            <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.greyColor }}> locate ( {item.distance} )</Text>
                            <Text style={{ alignSelf: 'center', width: 100, textAlign: 'right', fontFamily: 'Poppins-Medium' }}>
                                <Ionicons
                                    name="call"
                                    size={20}
                                    color={Colors.Primary}
                                    style={{}}
                                />{' '} call</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </Card>
        )
    }
    onHandleNavigate = (item, index) => {
        if (index === 0) {
            this.props.navigation.navigate('Location')
        }
    }
    returnText = item => {
        const emoji = nodeEmoji.get(item.flag);
        return (
            <View style={{ flexDirection: 'row', }}>
                <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.Primary, top: 5 }}>+ {item.callingCode}</Text>
                <Text style={{ left: 10, top: 5 }}>
                    <Entypo
                        name="triangle-down"
                        size={20}
                        color={Colors.Primary}
                    />
                </Text>
            </View>
        )
    };
    onValueChange = selectedValue => {
        this.setState({ value: selectedValue });
    };
    renderOption(settings) {
        const { item, getLabel } = settings;
        const emoji = nodeEmoji.get(item.flag);
        return (
            <View style={{ padding: 5 }}>
                <View style={styles.innerContainer}>
                    <Text
                        style={{ fontSize: 20, paddingLeft: 20 }}
                        allowFontScaling={false}>
                        {emoji}
                    </Text>
                    <Text
                        style={{
                            fontSize: 18,
                            alignSelf: 'flex-start',
                            paddingLeft: 20,
                            color: Colors.Primary, width: 100
                        }}>
                        +{item.callingCode}{' '}
                    </Text>
                    <Text style={{ fontSize: 18, alignSelf: 'flex-start', fontFamily: 'Poppins-Bold', width: 200 }}>
                        {item.name}
                    </Text>
                </View>
            </View>
        );
    }
    renderHeader = () => {
        return (
            <View style={styles.headerFooterContainer}>
                <Text style={{ fontSize: 18, padding: 5, fontFamily: 'Poppins-Medium', color: Colors.Primary }}>Select your Country</Text>
            </View>
        );
    };
    renderField(settings) {
        const { selectedItem, defaultText, getLabel, clear } = settings;
        return (
            <View style={styles.container}>
                <View>
                    {!selectedItem && <Text style={[{ fontSize: 18 }]}>{defaultText}</Text>}
                    {selectedItem && (
                        <View style={styles.innerContainer}>
                            <Text style={[{ fontSize: 15, color: Colors.Text, textAlign: 'center', alignSelf: 'center' }]}>
                                {getLabel(selectedItem)}
                            </Text>
                        </View>
                    )}
                </View>
            </View>
        );
    }
    renderLoginPanel() {
        return (
            <>
                <View style={[styles.container, { width: wp('25%'), borderWidth: 1, borderColor: Colors.lightGreyColor, borderTopLeftRadius: 30, borderBottomLeftRadius: 30 }]}>
                    <View
                        style={[styles.textInputView, {  height: hp('7%'), flexDirection: 'row', }]}>
                        <CustomPicker
                            modalStyle={styles.countryModalStyle}
                            placeholder={'Select your Country'}
                            style={[styles.defaultInput]}
                            value={this.state.value}
                            options={this.state.options}
                            getLabel={item => this.returnText(item)}
                            headerTemplate={this.renderHeader}
                            fieldTemplate={this.renderField}
                            optionTemplate={this.renderOption}
                            onValueChange={selectedCode => {
                                this.onValueChange(selectedCode);
                            }}
                        />
                    </View>
                </View>
            </>
        );
    }
    _onLoginHandle = () =>{
        this.setState({
            isLogin:false,
            isOtpVerify:true
        })
    }
    loginViewHandel = () =>{
        return(
            <View style={{ padding: wp('10%'),  height: hp('45%') }}>
                        <Text style={{ fontFamily: 'Poppins-Medium',fontSize:12 }}>Let's get started! Enter your mobile number</Text>
                        <View style={[styles.container, { marginTop: 15, }]}>
                            {this.renderLoginPanel()}
                            <View style={[styles.inputText, { flexDirection: 'row' }]}>
                                <TextInput
                                    style={styles.hidePasswordText}
                                    placeholder={'Phone Number'}
                                    autoCapitalize="none"
                                    returnKeyType="send"
                                    maxLength={15}
                                    keyboardType={'number-pad'}
                                    placeholderTextColor={Colors.lightGreyColor}
                                    onChangeText={(mobileNo) => this.setState({ mobileNo, isPassword: false, isPressed: false })}
                                    ref={(input) => (this.passWord = input)}
                                    defaultValue={this.state.mobileNo}
                                    onSubmitEditing={() => this._onLoginHandle()}
                                />
                            </View>
                        </View>
                        <TouchableOpacity onPress={() => { this._onLoginHandle() }} style={{ alignContent: 'flex-end', marginTop: hp('1%') }}>
                            <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 12, textAlign: 'right', color: Colors.Primary, textDecorationLine: 'underline', textDecorationColor: Colors.Primary }}>Request OTP</Text>

                        </TouchableOpacity>
                        <Text style={{ textAlign: 'center', fontFamily: 'Poppins-Medium', marginTop: hp('2%'), letterSpacing: 0.9 }}>-   Or sign in with   -</Text>
                        <TouchableOpacity style={{ flexDirection: 'row', alignSelf: 'center', padding: 20 }}>
                            <Card style={{ paddingHorizontal: 20, height: 60, width: 60, margin: 5, alignItems: 'center', justifyContent: 'center' }}>
                                <Image resizeMode={"contain"} style={{ height: 50, width: 50 }} source={require('../assets/images/google.jpeg')} />
                            </Card>
                            <Text style={{ width: 40 }}></Text>
                            <Card style={{ height: 60, width: 60, margin: 5, alignItems: 'center', justifyContent: 'center' }}>
                                <Image resizeMode={"contain"} style={{ height: 50, width: 50 }} source={require('../assets/images/fb.png')} />
                            </Card>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ alignSelf: 'flex-end', flexDirection: 'row' }}>
                            <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 10, color: Colors.Primary, textAlign: 'right', }}>SKIP LOGIN</Text>
                            <Text style={{ width: 20, height: 20, borderRadius: 50, backgroundColor: Colors.Primary, left: 15, top: -5 }}>
                                <MaterialIcons
                                    name="arrow-right-alt"
                                    size={20}
                                    color={Colors.White}
                                // style={{ top:-5 }}
                                /></Text>
                        </TouchableOpacity>
                        <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 8, textAlign: 'center', }}>By Continuing you agree to our
                            <Text style={{ fontFamily: 'Poppins-Bold', textDecorationLine: 'underline' }}> Terms & Conditions  </Text> and <Text style={{ fontFamily: 'Poppins-Bold', textDecorationLine: 'underline' }}>Payment Policy </Text> </Text>

                    </View>
        )
    }
    onVerifyHandle = () =>{
        this.props.navigation.navigate('UserLocation')
    }
    otpViewHandel = () =>{
        let {counter} = this.state;
        return(
            <View style={{ padding: wp('10%'),  height: hp('45%') }}>
                        <TouchableOpacity onPress={() => { this.setState({isLogin:true,isOtpVerify:false,counter:30}) }}>
                            <Text style={{ fontFamily: 'Poppins-Medium',  textAlign: 'center', margin: 10 }}>Enter the <Text style={{ fontFamily: 'Poppins-Bold' }}>One Time Password</Text>{'\n'}we just sent to mobile number {this.state.mobileNo}  <Entypo
                                name="edit"
                                size={15}
                            /></Text>
                        </TouchableOpacity>
                        <OTPTextView
                            handleTextChange={(text) => this.setState({ otpInput: text })}
                            containerStyle={styles.textInputContainer}
                            textInputStyle={styles.roundedTextInput}
                            inputCount={4}
                            inputCellLength={1}
                            tintColor={Colors.Primary}
                            offTintColor={'black'}
                        />
                        <TouchableOpacity onPress={() => { this.onVerifyHandle() }} style={{ flexDirection: 'row',left: -20,  marginTop: hp('2%'),alignSelf:'flex-end' }}>
                            <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 14, color: Colors.Primary, }}>Verify</Text>
                            <Text style={{ alignSelf: 'center',  textAlign: 'center', left: 20, top: -5 }}>
                                <MaterialIcons
                                    name="arrow-right-alt"
                                    size={28}
                                    color={Colors.Primary}
                                // style={{top: -15 }}
                                /></Text>
                        </TouchableOpacity>
                        {counter !== 0 ? (
                            <Text style={{ textAlign: 'center', fontFamily: 'Poppins-Bold', }}>Auto Reading OTP {'  '} <Text style={{ color: Colors.redColor }}>{"00 : "}{this.state.counter} s</Text></Text>) : (
                            <Text>''</Text>
                        )}

                        {counter == 0 ? (
                            <TouchableOpacity>

                                <Text style={{ fontFamily: 'Poppins-Medium',  margin: 10, left: 30, top: Dims.DeviceHeight * 0.04, fontSize: 12 }}>Didn't receive the OTP? <Text style={{ fontFamily: 'Poppins-Bold', textDecorationLine: 'underline' }}>Click to resend</Text></Text>


                            </TouchableOpacity>
                        ) :
                            <Text></Text>}

                    </View>
        )
    }
    render() {
        let { getBannerImage,isLogin,isOtpVerify } = this.state;
        return (
            <>
                <StatusBar style={{ backgroundColor: Colors.Primary }} />
                <ScrollView style={{ backgroundColor: '#FFFFFFF',height:hp('100%') }}>
                    <View style={{ height: hp('55%') }}>


                        <Swiper
                            autoplay
                            showsPagination={false}
                        autoplayTimeout={5}
                        // activeDot={
                        //     <View
                        //         style={{
                        //             backgroundColor:
                        //                 Colors.Primary,
                        //             width: 6,
                        //             height: 6,
                        //             borderRadius: 4,
                        //             marginLeft: 3,
                        //             marginRight: 3,
                        //             marginTop: 3,
                        //             marginBottom: 3,
                        //             flexDirection: 'row'
                        //         }}
                        //     />
                        // }
                        >
                            {(
                                getBannerImage && getBannerImage.map((item, index) => {
                                    return (
                                        <TouchableOpacity disabled={index == 0 ? false : true} onPress={() => { this.onHandleNavigate(item, index) }} style={{ height: Dims.DeviceHeight }}>
                                            <Image
                                                style={{
                                                    height: hp('55%'),
                                                    width: Dims.DeviceWidth
                                                }}
                                                resizeMode='cover'
                                                source={item.image}
                                            />
                                        </TouchableOpacity>
                                    )

                                })
                            )}
                        </Swiper>
                    </View>
{isLogin && (
    this.loginViewHandel()
)}
{isOtpVerify && (
    this.otpViewHandel()
)}
                    

                </ScrollView>

            </>
        );
    }
}

const styles = StyleSheet.create({
    logo: {
        height: 120,
        width: 120,
        borderRadius: 100,
    },
    container: {
        flexDirection: 'row',
    },
    dotView: {
        flexDirection: 'row',
        alignSelf: 'flex-end'
    },
    logo: {
        height: Dims.DeviceHeight / 4 - 100,
        width: Dims.DeviceWidth - 50
    },
    nurseLogo: {
        height: Dims.DeviceHeight / 2.5,
        width: Dims.DeviceWidth / 2
        // width:130

    },
    welcomeText:
        { fontFamily: 'Poppins-Medium', fontSize: 12, color: Colors.Primary, letterSpacing: 2, top: 10, marginLeft: '7%', marginBottom: 10 },
    defaultInput: {
        color: Colors.Black,
        fontSize: 12,
        top: 10,
        justifyContent: 'center',
        width: 80

    },
    textInputView: {
        paddingLeft: 15,
    },

    innerContainer: {
        flexDirection: 'row',
        alignItems: 'stretch',

    },
    hidePasswordText: {
        fontSize: 16,
        left: 10,
        fontFamily: 'Poppins-Medium',
        flex: 0.9
    },
    headerFooterContainer: {
        alignItems: 'center',
    },
    inputText: {
        alignSelf: 'center',
        width: wp('60%'),
        height: hp('7%'),
        borderColor: Colors.lightGreyColor,
        borderWidth: 1,
        fontSize: 12,
        textAlign: 'center',
        backgroundColor: '#fcfcfc',
        fontFamily: 'Poppins-Medium',
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30
    },
    countryModalStyle: {
        // width:80
    },
    textInputContainer: {
        // marginHorizontal: 50,
    },
    roundedTextInput: {
        height: 60,
        fontFamily: 'Poppins-Medium',
        color: Colors.Primary
    },
});

export default Home;
