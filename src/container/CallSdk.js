import React, { Component } from 'react';
import { Text, TextInput, Button, ActivityIndicator, DeviceEventEmitter, Platform, View, BackHandler, FlatList, StatusBar, ImageBackground, TouchableNativeFeedback, SafeAreaView, Keyboard, Alert, KeyboardAvoidingView, PermissionsAndroid, StyleSheet, Image, TouchableOpacity, Dimensions, ViewPropTypes } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import MapView, {
    AnimatedRegion,
    PROVIDER_GOOGLE,
    Marker,
} from "react-native-maps";
import { Colors } from '../assets/Colors';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import { Dims } from '../components/Dims';
import Geolocation from 'react-native-geolocation-service';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
const googleApiKey = 'AIzaSyAFn3Q1LhhC0C8pfWhNyHwfHWYFT7S2s_M';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

import FloatingLabel from 'react-native-floating-labels';
import Fontisto from 'react-native-vector-icons/Fontisto';
import { ScrollView } from 'react-native-gesture-handler';
import LocationServicesDialogBox from 'react-native-android-location-services-dialog-box';
import Geocoder from 'react-native-geocoder';
import Footer from '../components/Footer';
import { Card } from 'react-native-shadow-cards';
import { color } from 'react-native-reanimated';
import SplashScreen from 'react-native-splash-screen';
import DeviceInfo from 'react-native-device-info';
import RtcEngine, {
    ChannelProfile,
    ClientRole,
    RtcEngineContext, RtcLocalView, RtcRemoteView
} from 'react-native-agora';
import Item from '../common/item';
import { HttpHelper } from '../HelperApi/Api/HTTPHelper';
import { AgoraUrl } from '../HelperApi/Api/APIConfig';
import "react-native-get-random-values";
import { v4 as uuid } from "uuid";



class map extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        console.log(params,'params');
        this.state = {
            appId: '49c0dbb169294ddca113f24be7f2d848',
            token: '00649c0dbb169294ddca113f24be7f2d848IADONZuXud8r/Ow/LPgMjZOYex96SGTjARfetTWii86FHgx+f9gAAAAAEAC7nPWLRor2YQEAAQBGivZh',
            channelName: 'test',
            joinSucceed: false,
            peerIds: [],
            uid: 123456,
            isJoined: params && params.status ? params.status : false,
            openMicrophone: true,
            enableSpeakerphone: true,
            playEffect: false,
            channelId: '',
            getTokenData: {},
            _remoteUid: params && params.uuid && params.uuid,

        };
    }

    componentDidMount() {
        SplashScreen.hide();
        this.requestCameraAndAudioPermission()
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
        this.createUUID()
        this._initEngine();
    }
    UNSAFE_componentWillMount() {

    }

    async requestCameraAndAudioPermission() {
        try {
            const granted = await PermissionsAndroid.requestMultiple([
                PermissionsAndroid.PERMISSIONS.CAMERA,
                PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
            ]);
            if (
                granted["android.permission.RECORD_AUDIO"] === PermissionsAndroid.RESULTS.GRANTED &&
                granted["android.permission.CAMERA"] === PermissionsAndroid.RESULTS.GRANTED
            ) {
                console.log("You can use the cameras & mic");
            } else {
                console.log("Permission denied");
            }
        } catch (err) {
            console.warn(err);
        }
    }
    createUUID = () => {
        let createUid = uuid();
        console.log(createUid, 'createUid');

    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
        SplashScreen.hide();
        this._engine?.destroy();
    }


    _initEngine = async () => {
        let { appId } = this.state;
        this._engine = await RtcEngine.create(appId)

        await this._engine.enableAudio();
        this._addListeners();
        await this._engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
        await this._engine.setClientRole(ClientRole.Broadcaster);
    };

    _addListeners = () => {
        this._engine.addListener('Warning', (warningCode) => {
            console.info('Warning', warningCode);
        });
        this._engine.addListener('Error', (errorCode) => {
            console.info('Error', errorCode);
        });
        this._engine.addListener('UserJoined', (uid, elapsed) => {
            console.log('UserJoined', uid, elapsed);
            // Get current peer IDs
            const { peerIds, _remoteUid } = this.state;
            // If new user
            if (peerIds.indexOf(uid) === -1) {
                this.setState({
                    // Add peer ID to state array
                    peerIds: [...peerIds, uid],
                    _remoteUid: uid
                });
            }
        });
        this._engine.addListener('UserOffline', (uid, reason) => {
            console.log('UserOffline', uid, reason);
            const { peerIds } = this.state;
            this.setState({
                // Remove peer ID from state array
                peerIds: peerIds.filter((id) => id !== uid),
                _remoteUid: null
            });
        });
        this._engine.addListener('JoinChannelSuccess', (channel, uid, elapsed) => {
            console.info('JoinChannelSuccess', channel, uid, elapsed);
            this.setState({ isJoined: true, isSelect: false });
        });
        this._engine.addListener('LeaveChannel', (stats) => {
            console.info('LeaveChannel', stats);
            this.setState({ isJoined: false });
        });
    };

    _joinChannel = async () => {
        this.generateToken()
        let { getTokenData, token, channelName, uid } = this.state;

        // if (Platform.OS === 'android') {
        //     await PermissionsAndroid.request(
        //         PermissionsAndroid.PERMISSIONS.RECORD_AUDIO
        //     );
        // }
    };

    _onChangeRecordingVolume = (value) => {
        this._engine?.adjustRecordingSignalVolume(value * 400);
    };

    _onChangePlaybackVolume = (value) => {
        this._engine?.adjustPlaybackSignalVolume(value * 400);
    };

    _toggleInEarMonitoring = (isEnabled) => {
        this._engine?.enableInEarMonitoring(isEnabled);
    };

    _onChangeInEarMonitoringVolume = (value) => {
        this._engine?.setInEarMonitoringVolume(value * 400);
    };

    _leaveChannel = async () => {
        await this._engine?.leaveChannel();
        this.setState({
            isModalVisible: true,
            isOpacity: true,
        }, () => {
            this.props.navigation.navigate('OrderStatus', { getData: this.state.selectedData, isModal: true })

        })
    };

    _switchMicrophone = () => {
        const { openMicrophone } = this.state;
        this._engine
            ?.enableLocalAudio(!openMicrophone)
            .then(() => {
                this.setState({ openMicrophone: !openMicrophone });
            })
            .catch((err) => {
                console.warn('enableLocalAudio', err);
            });
    };

    _switchSpeakerphone = () => {
        const { enableSpeakerphone } = this.state;
        this._engine
            ?.setEnableSpeakerphone(!enableSpeakerphone)
            .then(() => {
                this.setState({ enableSpeakerphone: !enableSpeakerphone });
            })
            .catch((err) => {
                console.warn('setEnableSpeakerphone', err);
            });
    };
    _switchEffect = () => {
        const { playEffect } = this.state;
        if (playEffect) {
            this._engine
                ?.stopEffect(1)
                .then(() => {
                    this.setState({ playEffect: false });
                })
                .catch((err) => {
                    console.warn('stopEffect', err);
                });
        } else {
            this._engine
                ?.playEffect(
                    1,
                    Platform.OS === 'ios'
                        ? `${RNFS.MainBundlePath}/Sound_Horizon.mp3`
                        : '/assets/Sound_Horizon.mp3',
                    -1,
                    1,
                    1,
                    100,
                    true,
                    0
                )
                .then(() => {
                    this.setState({ playEffect: true });
                })
                .catch((err) => {
                    console.warn('playEffect', err);
                });
        }
    };

    handleBackPress = () => {
        this.props.navigation.navigate('Location')
        return true;
    };
    async requestPermissions() {
        try {
            const granted = await PermissionsAndroid.requestMultiple(
                [PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, PermissionsAndroid.PERMISSIONS.RECORD_AUDIO],
            );

            this.getCurrentLocation();
        } catch (err) {

        }
    }

    startCall = async () => {
        let { getTokenData } = this.state;
        // Join Channel using null token and channel name
        this._engine.joinChannel(
            getTokenData.token,
            getTokenData.channel,
            null,
            getTokenData.uid
        );
    };


    render() {
        const {
            channelId,
            isJoined,
            openMicrophone,
            enableSpeakerphone,
            playEffect,
            getTokenData,
            _remoteUid
        } = this.state;
        console.log(isJoined && _remoteUid, 'isJoined && _remoteUid');
        return (
            <>
                <StatusBar
                    backgroundColor={Colors.Primary}
                    barStyle="light-content"
                />
                <SafeAreaView backgroundColor={Colors.Primary}></SafeAreaView>

                {isJoined && _remoteUid === null ? (
                    <RtcLocalView.SurfaceView
                        remote="create"
                        style={styles.fullscreen}
                        channelId={getTokenData.channelName}
                    />
                ) : (
                    <RtcRemoteView.SurfaceView
                        remote="join"
                        uid={getTokenData.uid}
                        style={styles.fullscreen}
                        channelId={getTokenData.channelName}
                    />
                )}
                {/* {isJoined && (
                    <>

                        <ImageBackground source={selectedData.image} style={{ width: Dims.DeviceWidth, height: Dims.DeviceHeight, backgroundColor: 'black' }}>

                        </ImageBackground>
                        <View style={{ position: 'absolute', width: Dims.DeviceWidth, paddingHorizontal: 10, borderTopLeftRadius: 30, borderTopRightRadius: 30, top: 100 }}>
                            <Text style={{ fontSize: 25, fontFamily: 'Poppins-Bold', alignSelf: 'center', color: Colors.White }}> {selectedData.name}</Text>
                            <Text style={{ fontSize: 18, fontFamily: 'Poppins-Bold', alignSelf: 'center', color: Colors.White }}>{selectedData.role}</Text>

                        </View>
                        <View style={{ position: 'absolute', width: Dims.DeviceWidth, paddingHorizontal: 10, borderTopLeftRadius: 30, borderTopRightRadius: 30, bottom: 70 }}>
                            <TouchableOpacity onPress={isJoined ? this._leaveChannel : this._joinChannel} style={{ alignSelf: 'center', justifyContent: 'center', height: 80, width: 80, borderRadius: 100, backgroundColor: 'red' }}>
                                <Text style={{ textAlign: 'center', textAlignVertical: 'center' }}>
                                    <MaterialIcons
                                        name={"call-end"}
                                        size={35}
                                        color={Colors.White}
                                        style={{}}
                                    /></Text>
                            </TouchableOpacity>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 40 }}>
                                <TouchableOpacity onPress={this._switchMicrophone} >
                                    <Text style={{ alignSelf: 'center', marginLeft: 10 }}>
                                        <FontAwesome5
                                            name={openMicrophone ? "volume-mute" : "volume-off"}
                                            size={35}
                                            color={Colors.White}
                                            style={{}}
                                        /></Text>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={this._switchSpeakerphone}
                                >
                                    <Text style={{ alignSelf: 'center', marginLeft: 10 }}>
                                        <FontAwesome
                                            name={enableSpeakerphone ? "microphone" : 'microphone-slash'}
                                            size={35}
                                            color={Colors.White}
                                            style={{}}
                                        /></Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </>
                )} */}

                <DropdownAlert
                    ref={(ref) => (this.dropdown = ref)}
                    containerStyle={{ backgroundColor: '#FF0000' }}
                    imageSrc={'https://facebook.github.io/react/img/logo_og.png'}
                />
            </>
        );

    };
}
export default map;
const styles = StyleSheet.create({
    deliver: {
        position: 'relative',
        bottom: 0,
        backgroundColor: Colors.Primary,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        height: Dims.DeviceHeight / 2.5,
        width: Dims.DeviceWidth,
        // top:-30
        marginTop: -40
    },
    deliver1: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        top: 30,
        backgroundColor: Colors.Primary,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        height: Dims.DeviceHeight / 0.2,
        width: Dims.DeviceWidth,
        marginBottom: 100
    },
    location: {
        position: 'relative',
        bottom: 0,
        backgroundColor: Colors.redColor,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        width: Dims.DeviceWidth,
    },
    location1: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        top: 30,
        backgroundColor: Colors.Primary,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        height: 100,
        width: Dims.DeviceWidth,
        marginBottom: 100
    },
    loginBtn: {
        width: Dims.DeviceWidth - 50,
        paddingVertical: 10,
        borderRadius: 2,
        backgroundColor: Colors.white
    },
    confirmButton: {
        width: Dims.DeviceWidth - 250,
        paddingVertical: 10,
        borderRadius: 2,
        backgroundColor: Colors.Primary,
        marginTop: -70
    },
    container2: {
        alignSelf: 'center'
    },
    gradient: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 7,
    },
    btn: {
        width: 170,
        height: 50,
        alignSelf: 'flex-end',
        marginHorizontal: 20
    },
    loginText: {
        color: Colors.Primary,
        alignSelf: 'center',
        // justifyContent: 'center',
        fontFamily: 'Poppins-Medium',
        letterSpacing: 3,
        textAlign: 'center',
    },
    closePanel: {
        flexWrap: 'wrap',
        // top: 20

    },
    labelStyle: {
        color: Colors.textColor,
        fontSize: 10,
        fontFamily: 'Poppins-Bold',
        borderBottomColor: Colors.White,
        letterSpacing: 1
    },
    floatingLabelView: {
        marginHorizontal: 20,
    },
    hidePasswordText: {
        fontSize: 16,
        textAlign: 'center', fontFamily: 'Poppins-Medium',
        textAlignVertical: 'center',
        // color:Colors.placeholderColor
        fontSize: 16,

    },
    inputStyle: {
        borderWidth: 0,
        // margin: 10,
        height: 45,
        fontFamily: 'Poppins-Medium',
        fontSize: 12,
        borderBottomColor: Colors.white,
        borderBottomWidth: 1,
        color: Colors.White

    }, busIcon: {
        // paddingTop: 10,
        color: Colors.Primary
    },
    inputText: {
        fontFamily: 'Poppins-Medium',
        left: 10,
        top: 5
    },
    max: {
        flex: 1,
    },
    buttonHolder: {
        height: 100,
        alignItems: 'center',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    button: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        backgroundColor: '#0093E9',
        borderRadius: 25,
    },
    buttonText: {
        color: '#fff',
    },
    fullView: {
        width: Dims.DeviceWidth,
        height: Dims.DeviceHeight - 100,
    },
    remoteContainer: {
        width: '100%',
        height: 150,
        position: 'absolute',
        top: 5
    },
    remote: {
        width: 150,
        height: 150,
        marginHorizontal: 2.5
    },
    noUserText: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        color: '#0093E9',
    },
    container: {
        flex: 1,
    },
    float: {
        width: '100%',
        position: 'absolute',
        alignItems: 'center',
        bottom: 20,
    },
    top: {
        width: '100%',
    },
    input: {
        borderColor: 'gray',
        borderWidth: 1,
    },

    fullscreen: {
        width: Dims.DeviceWidth,
        height: Dims.DeviceHeight,
    },
});