import React, { Component } from 'react';
import {
    Text,
    View,
    StatusBar,
    Image, StyleSheet, BackHandler, TouchableOpacity, Alert, Modal, TextInput, FlatList, DeviceEventEmitter, KeyboardAvoidingView, Switch
} from 'react-native';
import { Colors } from '../assets/Colors';
import { Dims } from '../components/Dims';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as RNLocalize from 'react-native-localize';
import { CustomPicker } from 'react-native-custom-picker';
import nodeEmoji from 'node-emoji';
import { getAllCountries } from 'react-native-country-picker-modal';
import { ScrollView } from 'react-native-gesture-handler';
import LocationServicesDialogBox from 'react-native-android-location-services-dialog-box';
import Geolocation from 'react-native-geolocation-service';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Footer from '../components/Footer';
import { Card } from 'react-native-shadow-cards';
import DateTimePicker from "react-native-modal-datetime-picker";
var moment = require('moment');
const date = new Date();
class Home extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            options: [],
            locationAddress: params && params.locationAddress ? params.locationAddress : [],
            getData: params && params.getData ? params.getData : [],
            isModalVisible: params && params.isModal ? params.isModal : false,
            isOpacity: params && params.isModal ? params.isModal : false,
            time: ''
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.navigate('Location')
        return true;
    };
    renderSideMenuContent = () => {
        return (
            <>
                <ScrollView>
                    <Text>'Hi'</Text>
                </ScrollView>
            </>
        );
    };
    renderMainContent = () => {
        if (!this.state.drawerOpen) {
            return (
                <View>
                    <Entypo name="menu" size={38} color={CustomColors.Black} />
                </View>
            );
        }
    };


    handleDob = () => {
        return (
            <TouchableOpacity onPress={() => this.setState({ showDate: true, visible: true })} style={{ flex: 1, flexDirection: 'row', alignSelf: 'center' }}>

                <Text style={{ textAlign: 'center', alignSelf: 'center', fontFamily: 'Poppins-Bold' }}><AntDesign color={Colors.greyColor} size={30} name="calendar" /></Text>
                <Text style={{ textAlign: 'center', alignSelf: 'center', fontFamily: 'Poppins-Bold', color: Colors.greyColor, marginHorizontal: 10 }}>{this.state.dateTime ? this.state.dateTime : 'DD-MM-YYYY'}</Text>

            </TouchableOpacity>
        )
    }
    handleDatePicker = (date) => {

        let properDate = moment(date).format('DD-MM-YYYY');
        this.setState({ dateTime: properDate })
        this.hideDatePicker();
    }
    hideDatePicker = () => {
        this.setState({ visible: false });
    }
    closeCancelModal = () => {
        this.setState({
            isModalVisible: false,
            isConfirm: false
        })
    }
    onSubmit = (type) => {
        if (type === "submit") {
            this.setState({
                isConfirm: false,
                isModalVisible: false,
                isConfirmed: true
            })
        } else {
            this.setState({
                isConfirm: true,
                isModalVisible: false,
            })
        }
    }
    render() {
        let { locationAddress, getData } = this.state;
        let locatedAddress = [locationAddress.locality, locationAddress.country, locationAddress.adminArea]
        return (
            <>
                <StatusBar style={{ backgroundColor: Colors.Primary }} />
                <ScrollView style={{ height: Dims.DeviceHeight }}>
                    <View style={{ height: 150, backgroundColor: Colors.Primary, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Location')}>
                            <Text style={{ alignSelf: 'flex-start', marginLeft: 10, padding: 3, top: 5 }}>
                                <MaterialCommunityIcons
                                    name="arrow-left"
                                    size={25}
                                    color={Colors.White}
                                    style={{}}
                                /></Text>
                        </TouchableOpacity>
                        <View style={{ flexDirection: 'column', paddingVertical: 5, alignSelf: 'flex-start', padding: 3 }}>
                            <Text style={{ textAlign: 'center', color: Colors.White, fontFamily: 'Poppins-Medium', fontSize: 16, letterSpacing: 0.8, top: 3 }}>ORDER STATUS</Text>
                            <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 14, color: Colors.White, marginTop: 30, letterSpacing: 0.1 }}> Request No:MEDEXFORCE {getData ? getData.refNo : ''} </Text>
                            <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 14, color: Colors.White }}> Date:{getData ? getData.appointmentDate : ''} </Text>
                        </View>
                        <View style={{ flexDirection: 'column', margin: 10, padding: 3 }}>
                            <Text style={{ alignSelf: 'flex-start', }}>
                                <FontAwesome5
                                    name="headset"
                                    size={20}
                                    color={Colors.White}
                                    style={{}}
                                /></Text>
                        </View>
                    </View>
                    <Card style={{ height: 220, backgroundColor: 'white', width: Dims.DeviceWidth / 1.4, alignSelf: 'center', marginTop: -20 }}>
                        <View style={{ alignSelf: 'center', margin: 20 }}>
                            <Image style={{ height: 70, width: 70, alignSelf: 'center', }}
                                resizeMode={'contain'}
                                source={require("../assets/images/icon2.png")} />
                            <Text style={{ color: Colors.Primary, textAlign: 'center', fontSize: 16, fontFamily: 'Poppins-Bold', letterSpacing: 0.4 }}>{getData ? getData.name : ''}</Text>
                            <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                                <Image style={{ height: 14, width: 14 }}
                                    source={require('../assets/images/like.png')} />
                                <Text style={{ color: Colors.greyColor, fontFamily: 'Poppins-Medium', fontSize: 10, textAlign: 'center', left: 5 }}>Served 300 patients</Text>
                            </View>
                        </View>
                        <Text style={{ borderWidth: 0.4, borderColor: Colors.greyColor, height: 0.2, marginHorizontal: 30 }}></Text>
                        <Text style={{ color: Colors.darkGreyColor, fontFamily: 'Poppins-Bold', fontSize: 12, textAlign: 'center', marginTop: 10 }}>{getData ? getData.role : ''}</Text>
                        <Text style={{ color: Colors.greyColor, fontFamily: 'Poppins-Medium', fontSize: 10, textAlign: 'center', }}>{getData ? getData.description : ''}</Text>
                    </Card>
                    <View style={{ height: 40, borderWidth: 1, borderColor: Colors.Primary, width: 1, alignSelf: 'center' }} />
                    <Card style={{ height: 100, backgroundColor: 'white', width: Dims.DeviceWidth / 1.4, alignSelf: 'center', marginTop: 13 }}>
                        <View style={{ fontFamily: 'Poppins-Medium', fontSize: 8, alignSelf: 'center', height: 30, width: 30, borderRadius: 30, borderColor: Colors.Primary, borderWidth: 1, top: -15 }}>
                            <Text style={{ textAlign: 'center', justifyContent: 'center', top: 5, }}><MaterialCommunityIcons
                                name="coffee"
                                size={15}
                                color={Colors.Primary}
                                style={{}}
                            /></Text></View>
                        <View style={{ alignSelf: 'center' }}>
                            <Text style={{ color: Colors.greyColor, textAlign: 'center', fontSize: 13, fontFamily: 'Poppins-Medium', letterSpacing: 0.4 }}>Appoinment  Confirmation</Text>
                            <Text style={{ color: Colors.Primary, textAlign: 'center', fontSize: 16, fontFamily: 'Poppins-Bold', letterSpacing: 0.4 }}>{this.state.isConfirmed ? "CONFIRMED" : "PENDING"}</Text>
                        </View>
                    </Card>
                    {this.state.time && this.state.period ? (
                        <>
                            <View style={{ height: 40, borderWidth: 1, borderColor: Colors.Primary, width: 1, alignSelf: 'center' }} />
                            <Card style={{ height: 100, backgroundColor: 'white', width: Dims.DeviceWidth / 1.4, alignSelf: 'center', marginTop: 13 }}>
                                <View style={{ fontFamily: 'Poppins-Medium', fontSize: 8, alignSelf: 'center', height: 30, width: 30, borderRadius: 30, borderColor: Colors.Primary, borderWidth: 1, top: -15 }}>
                                    <Text style={{ textAlign: 'center', justifyContent: 'center', top: 5, }}><MaterialIcons
                                        name="confirmation-num"
                                        size={15}
                                        color={Colors.Primary}
                                        style={{}}
                                    /></Text></View>
                                <View style={{ alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View>
                                        <Image style={{ height: 50, width: 50, alignSelf: 'center', }}
                                            resizeMode={'contain'}
                                            source={require("../assets/images/icon2.png")} />
                                    </View>
                                    <View style={{ left: 10 }}>
                                        <Text numberOfLines={2} style={{ color: Colors.greyColor, textAlign: 'center', fontSize: 13, fontFamily: 'Poppins-Medium', letterSpacing: 0.1 }}>{getData ? getData.name : ''} is on {getData && getData.gender === "Female" ? "her" : "his"} way</Text>
                                        <Text numberOfLines={2} style={{ color: Colors.Primary, textAlign: 'center', fontSize: 16, fontFamily: 'Poppins-Bold', letterSpacing: 0.4 }}>TODAY AT {this.state.time + ' ' + this.state.period}</Text>
                                    </View>
                                </View>
                            </Card>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 10, top: 20 }}>
                                <View style={{ height: 40, backgroundColor: Colors.Primary, flexDirection: 'row', flex: 0.3, alignSelf: 'center', justifyContent: 'center', borderRadius: 10 }}>
                                    <Text style={{ textAlign: 'center', alignSelf: 'center', }}><MaterialIcons
                                        name="call"
                                        size={15}
                                        color={Colors.White}
                                        style={{}}
                                    /></Text>
                                    <Text style={{ fontSize: 12, textAlign: 'center', alignSelf: 'center', fontFamily: 'Poppins-Bold', color: Colors.white, letterSpacing: 0.2 }}> {'  '}CALL</Text>
                                </View>
                                <View style={{ height: 40, backgroundColor: 'orange', flexDirection: 'row', flex: 0.3, alignSelf: 'center', justifyContent: 'center', borderRadius: 10 }}>
                                    <Text style={{ textAlign: 'center', alignSelf: 'center', }}><MaterialIcons
                                        name="add-location"
                                        size={15}
                                        color={'white'}
                                        style={{}}
                                    /></Text>
                                    <Text style={{ fontSize: 12, textAlign: 'center', alignSelf: 'center', fontFamily: 'Poppins-Bold', color: Colors.white, letterSpacing: 0.2 }}> {'  '}LOCATE</Text>
                                </View>
                                <View style={{ height: 35, backgroundColor: 'red', flexDirection: 'row', flex: 0.3, alignSelf: 'center', justifyContent: 'center', borderRadius: 10 }}>
                                    <Text style={{ textAlign: 'center', alignSelf: 'center', }}><AntDesign color={Colors.White} size={15} name="closecircle" /></Text>
                                    <Text style={{ fontSize: 12, textAlign: 'center', alignSelf: 'center', fontFamily: 'Poppins-Bold', color: Colors.white, letterSpacing: 0.2 }}> {'  '}CANCEL</Text>
                                </View>
                            </View>
                        </>
                    ) : (
                        <Text></Text>
                    )}
                    <View style={{ height: 100 }}></View>
                    <View style={styles.centeredView}>
                        <Modal
                            animated
                            animationType="slide"
                            transparent={true}
                            visible={this.state.isModalVisible || this.state.isConfirm}
                            style={{
                                opacity: 0.5,
                            }}
                            onRequestClose={() => {
                                this.setState({ isModalVisible: !this.state.isModalVisible, isConfirm: !this.state.isConfirm, isOpacity: false })
                            }}>
                            {!this.state.isConfirm ? (
                                <View style={styles.centeredView}>
                                    <View style={styles.modalView}>
                                        <TouchableOpacity onPress={() => { this.closeCancelModal() }} style={{ alignItems: 'flex-end', top: -20, left: 20 }}>
                                            <AntDesign color={Colors.greyColor} size={22} name="closecircle" />
                                        </TouchableOpacity>
                                        <View style={{ height: Dims.DeviceHeight / 4, width: Dims.DeviceWidth - 40, borderTopLeftRadius: 60, borderTopRightRadius: 60, alignSelf: 'center' }}>
                                            <Text style={{ fontFamily: 'Poppins-Bold', textAlign: 'center', margin: 10, fontSize: 20, color: Colors.orangeColor }}>{'Confirmation Needed'}</Text>
                                            <Text style={{ fontFamily: 'Poppins-Medium', textAlign: 'center', margin: 10, fontSize: 15 }}>Kindly click <Text style={{ textDecorationLine: 'underline' }}>YES,</Text> if you agree to {'\n'} proceed with the service provider {'\n'}  or click <Text style={{ textDecorationLine: 'underline' }}>NO</Text> to go back to search</Text>
                                            <TouchableOpacity style={{ backgroundColor: Colors.Primary, width: 150, borderRadius: 10, alignSelf: 'center', top: 10 }} onPress={() => { this.onSubmit('confirm') }}>
                                                <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.White, textAlign: 'center', margin: 10 }}>YES ! CONFIRM</Text>
                                            </TouchableOpacity>
                                            <Text style={{ fontFamily: 'Poppins-Medium', textAlign: 'center', margin: 10, fontSize: 10, textDecorationLine: 'underline', top: 10 }}>NO, I WANT TO OPT ANOTHER SERVICE PROVIDER</Text>
                                        </View>
                                    </View>
                                </View>
                            ) : (
                                <View style={styles.centeredView}>
                                    <View style={styles.modalView}>
                                        <TouchableOpacity onPress={() => { this.closeCancelModal() }} style={{ alignItems: 'flex-end', top: -20, left: 20 }}>
                                            <AntDesign color={Colors.greyColor} size={22} name="closecircle" />
                                        </TouchableOpacity>
                                        <View style={{ height: Dims.DeviceHeight / 4, width: Dims.DeviceWidth - 40, borderTopLeftRadius: 60, borderTopRightRadius: 60, alignSelf: 'center' }}>
                                            <Text style={{ fontFamily: 'Poppins-Bold', textAlign: 'center', margin: 10, fontSize: 20, color: Colors.orangeColor }}>{'Confirm Visit Time'}</Text>
                                            <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                                                <TextInput style={{ height: 38, width: 50, marginLeft: 10, borderWidth: 1, textAlign: 'center' }}
                                                    keyboardType='number-pad'
                                                    onChangeText={(time) => this.setState({ time })}
                                                    value={this.state.time} />
                                                <Text style={{ textAlign: 'center', padding: 10, left: 5 }}>:</Text>
                                                <TextInput style={{ height: 38, width: 50, marginLeft: 10, borderWidth: 1, textAlign: 'center' }}
                                                    onChangeText={(period) => this.setState({ period })}
                                                    value={this.state.period} />
                                            </View>
                                            {this.handleDob()}
                                            <TouchableOpacity style={{ backgroundColor: Colors.Primary, width: 150, borderRadius: 10, alignSelf: 'center', top: 10 }} onPress={() => { this.onSubmit('submit') }}>
                                                <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.White, textAlign: 'center', margin: 10 }}>Submit</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            )}
                        </Modal>
                    </View>
                    <DateTimePicker
                        isVisible={this.state.visible}
                        onCancel={this.hideDatePicker}
                        onConfirm={this.handleDatePicker}
                        date={date}
                    />
                </ScrollView>
                <Footer navigation={this.props.navigation} discountData={this.state.discountData} nearByData={this.state.nearByShopData} />
                <DropdownAlert
                    ref={(ref) => (this.dropdown = ref)}
                    containerStyle={{
                        backgroundColor: '#FF0000',
                    }}
                    imageSrc={'https://facebook.github.io/react/img/logo_og.png'}
                />
            </>
        );
    }
}

const styles = StyleSheet.create({
    logo: {
        height: 120,
        width: 120,
        borderRadius: 100,
    },
    container: {
        flexDirection: 'row',
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        // alignItems: "center",
        // marginTop: 22,
        // marginHorizontal: 10,
    },
    modalView: {
        marginHorizontal: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        // alignItems: "center",
        shadowColor: Colors.greyColor,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    inputWraps: {
        flexDirection: 'row',
        flex: 1,
        // paddingHorizontal: 10
    },
    TextInput: {
        flex: 1,
        // padding: 10,
        fontSize: 12,
        fontFamily: 'Poppins-Medium',
        borderColor: Colors.placeholderColor,
        // borderWidth: 0.2
        color: Colors.White
    },
});

export default Home;
