import React, { Component } from 'react';
import { Text, TextInput, Button, ActivityIndicator, DeviceEventEmitter, View, BackHandler, FlatList, StatusBar, SafeAreaView, Keyboard, Alert, KeyboardAvoidingView, PermissionsAndroid, StyleSheet, Image, TouchableOpacity, Dimensions, ViewPropTypes } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import MapView, {
    AnimatedRegion,
    PROVIDER_GOOGLE,
    Marker,
} from "react-native-maps";
import { Colors } from '../assets/Colors';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { Dims } from '../components/Dims';
import Geolocation from 'react-native-geolocation-service';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
const googleApiKey = 'AIzaSyAFn3Q1LhhC0C8pfWhNyHwfHWYFT7S2s_M';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

import FloatingLabel from 'react-native-floating-labels';
import Fontisto from 'react-native-vector-icons/Fontisto';
import { ScrollView } from 'react-native-gesture-handler';
import LocationServicesDialogBox from 'react-native-android-location-services-dialog-box';
import Geocoder from 'react-native-geocoder';
import SplashScreen from 'react-native-splash-screen';
import { HttpHelper } from '../HelperApi/Api/HTTPHelper';
import { LocationUrl } from '../HelperApi/Api/APIConfig';

const ASPECT_RATIO = Dims.DeviceWidth / Dims.DeviceHeight;
const LATITUDE_DELTA = 0.0899;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const CARD_HEIGHT = Dims.DeviceHeight / 6;
const CARD_WIDTH = CARD_HEIGHT - 50;
var initialRegion;
var markerRegion;


class map extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            hidePassword: true,
            email: '',
            rememberText: true,
            passWord: '',
            isEmail: false,
            isPassword: false,
            isPressed: false,
            markerData: {
                latitude: 35.1790507,
                longitude: -6.1389008,
            },
            keyboardState: 'closed',
            getAddress: [],
            isChangeLocation: false,
            changeData: [],
            address1: '',
            address2: '',
            city: '',
            state: '',
            pinCode: '',
            getDataAddress: [],
            position :{}
        };
    }
    componentDidMount() {
        this.showLocationPopup()
        SplashScreen.hide();

        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }
    showLocationPopup() {
        LocationServicesDialogBox.checkLocationServicesIsEnabled({
            message:
                "<h2 color='#eeeeee'>MedXForce would like to access your location?</h2>This app wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/>",
            ok: 'YES',
            cancel: 'NO',
            enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
            showDialog: true, // false => Opens the Location access page directly
            openLocationServices: true, // false => Directly catch method is called if location services are turned off
            preventOutSideTouch: false, //true => To prevent the location services popup from closing when it is clicked outside
            preventBackClick: false, //true => To prevent the location services popup from closing when it is clicked back button
            providerListener: true, // true ==> Trigger "locationProviderStatusChange" listener when the location state changes

        })
            .then(
                function (success) {
                    if (success.status === 'enabled') {
                        // this.props.navigation.navigate('Location')
                        this.requestPermissions();
                    } else {
                        // this.getAuthorizedLogin();
                        // this.interval = setInterval(() => this.onNavigate(), 3000);

                    }
                    // success => {alreadyEnabled: true, enabled: true, status: "enabled"}
                    // this.getCurrentLocation();
                }.bind(this),
            )
            .catch(error => {

            });
        BackHandler.addEventListener('hardwareBackPress', () => {
            //(optional) you can use it if you need it
            //do not use this method if you are using navigation."preventBackClick: false" is already doing the same thing.
            LocationServicesDialogBox.forceCloseDialog();
        });
        DeviceEventEmitter.addListener('locationProviderStatusChange', function (
            status,
        ) {
            // only trigger when "providerListener" is enabled
            console.log(status); //  status => {enabled: false, status: "disabled"} or {enabled: true, status: "enabled"}
        });
    }
    _keyboardDidShow = () => {
        this.setState({ keyboardState: 'opened' });
    }

    _keyboardDidHide = () => {
        this.setState({ keyboardState: 'closed' });
    }

    handleBackPress = () => {
        this.props.navigation.navigate('Slider')
        return true;
    };
    async requestPermissions() {
        await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );
        this.getCurrentLocation();
    }
    getCurrentLocation() {
        Geolocation.getCurrentPosition(
            position => {
                initialRegion = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                };
                markerRegion = {
                    latitude: Number(position.coords.latitude),
                    longitude: Number(position.coords.longitude),
                };
                this.setState({
                    mapData: initialRegion,
                    isLoading: false,
                    markerData: markerRegion,
                }, () => {
                    this.gettingLatLng()
                });

            },
            error => console.log(error),
            { enableHighAccuracy: false, timeout: 5000, distanceFilter: 0 },
        );
    }
    gettingLatLng = (initialRegion) => {
        var NY = {
            lat: this.state.mapData.latitude,
            lng: this.state.mapData.longitude
        };

        Geocoder.geocodePosition(NY).then(res => {
            // res is an Array of geocoding object (see below)
            console.log(res, 'getDataAddress');

            if (res) {
                this.setState({
                    getDataAddress: res ? res[0] : [],
                    isCurrent: true,
                    position:res ? res[0].position  : [],
                })
            } else {
            }
            console.log(this.state.getAddress, 'hhhhh');
        })
            .catch(err => console.log(err))

    }
    handleRegionChange = mapData => {
        this.setState({
            markerData: { latitude: mapData.latitude, longitude: mapData.longitude },
            mapData,
        });
    };
    onHandleAddress = (shopAddress) => {
        this.setState({ shopAddress: shopAddress, }, () => {
            var url =
                'https://maps.googleapis.com/maps/api/geocode/json?address=' + shopAddress + '&key=' +
                googleApiKey;
            fetch(url)
                .then(response => response.json())
                .then(responseJson => {
                    if (responseJson && responseJson.results[0] && responseJson.results[0].geometry) {
                        this.setState({
                            region: {
                                latitude: responseJson.results[0].geometry.location.lat,
                                longitude: responseJson.results[0].geometry.location.lng,
                                latitudeDelta: LATITUDE_DELTA,
                                longitudeDelta: LONGITUDE_DELTA,
                            },
                            isManualAddress: true
                        })
                    }
                })
        })
    }
    selectAddress = (e) => {
        if (e.nativeEvent && e.nativeEvent.action === "marker-press" && e.nativeEvent.coordinate)
            var dataLatLng = {
                lat: e.nativeEvent.coordinate.latitude,
                lng: e.nativeEvent.coordinate.longitude
            };
        Geocoder.geocodePosition(dataLatLng).then(responseJson => {
            console.log(responseJson, 'responseJson');
            if (responseJson) {
                this.setState(
                    {
                        selectAddress: responseJson && responseJson[0] ? responseJson[0] : '',
                        getDataAddress: responseJson && responseJson[0] ? responseJson[0] : [],
                        isSelect: true,
                        position:responseJson[0].position
                    });
            } else {
            }
        })
            .catch(err => console.log(err))
    }
    onLocationHandle = (mapData, address) => {
        let obj = {
            "lat": this.state.mapData && this.state.mapData != null && this.state.mapData.latitude ? String(this.state.mapData.latitude) : '',
            "lng": this.state.mapData && this.state.mapData != null && this.state.mapData.longitude ? String(this.state.mapData.longitude) : '',
            "distance": 5
        }
        var locBody = JSON.stringify(obj)
        let locationData = HttpHelper(LocationUrl, 'POST', locBody);
        locationData.then(locationResponse => {
            this.selectAddress({
                modalData: {
                    streetName: address
                }
            })
            this.state.modalData.streetName = address
            if (this.state.isAddress) {
                this.props.navigation.navigate('Address', { locationAddress: address, mapData: mapData, isModalVisible: true, modalData: this.state.modalData })
            } else {
                this.props.navigation.navigate('DeliveryAddress', { locationAddress: address, mapData: mapData })
            }
        })
    }
    handleChange = (event, type) => {
        if (type === "houseNo") {
            this.setState({ houseNo: event.nativeEvent.text });
        } else if (type === "address1") {
            this.setState({ address1: event.nativeEvent.text });
        } else if (type === "address2") {
            this.setState({ address2: event.nativeEvent.text });
        } else if (type === "city") {
            this.setState({ city: event.nativeEvent.text });
        } else if (type === "state") {
            this.setState({ state: event.nativeEvent.text });
        } else if (type === "pincode") {
            this.setState({ pincode: event.nativeEvent.text });
        } else if (type === "save") {
            this.setState({ save: event.nativeEvent.text });
        }
    };

    onPressZoomIn() {
        this.region = {
            latitude: this.state.mapData.latitude,
            longitude: this.state.mapData.longitude,
            latitudeDelta: this.state.mapData.latitudeDelta / 5,
            longitudeDelta: this.state.mapData.longitudeDelta / 5
        }
        this.setState({
            mapData: {
                latitudeDelta: this.region.latitudeDelta,
                longitudeDelta: this.region.longitudeDelta,
                latitude: this.region.latitude,
                longitude: this.region.longitude
            }
        })
        this.map.animateToRegion(this.region, 100);
    }

    onPressZoomOut() {
        this.region = {
            latitude: this.state.mapData.latitude,
            longitude: this.state.mapData.longitude,
            latitudeDelta: this.state.mapData.latitudeDelta * 5,
            longitudeDelta: this.state.mapData.longitudeDelta * 5
        }
        this.setState({
            mapData: {
                latitudeDelta: this.region.latitudeDelta,
                longitudeDelta: this.region.longitudeDelta,
                latitude: this.region.latitude,
                longitude: this.region.longitude
            }
        })
        this.map.animateToRegion(this.region, 100);
    }
    _renderItem = (item, index) => {
        console.log(item, 'lllll');
        return (
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <View style={{ flex: 0.8 }}>
                    <View style={{ flexDirection: 'row', paddingVertical: 5 }}>
                        <Text style={{}}>
                            <Icon
                                name="add-location"
                                size={30}
                                color={Colors.lightOrangeColor}
                                style={{}}
                            /></Text>
                        <Text style={{ fontFamily: 'Poppins-Bold', letterSpacing: 2 }}>{item.postalCode}</Text>
                    </View>
                    <View style={{ left: 10 }}>
                        <Text style={{ fontFamily: 'Poppins-Medium', letterSpacing: 1 }}>{item}</Text>

                    </View>
                </View>
                <View>
                    <TouchableOpacity onPress={() => { this.gettingData(item) }}>
                        <Text style={{ fontFamily: 'Poppins-Medium', letterSpacing: 0.5, color: Colors.Primary }}>CHANGE</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    gettingData = () => {
        let { getDataAddress } = this.state;
        if (getDataAddress) {
            this.setState({
                isChangeLocation: !this.state.isChangeLocation,
                houseNo: getDataAddress ? getDataAddress.streetNumber : '',
                address1: getDataAddress ? getDataAddress.streetName : '',
                address2: '',
                city: getDataAddress ? getDataAddress.locality : '',
                state: getDataAddress ? getDataAddress.adminArea : '',
                pincode: getDataAddress ? getDataAddress.postalCode : '',
                save: '',

            })
        }
    }
    relocatingAddress = () => {
        let { getDataAddress, address1, address2, city, state, pincode, save, houseNo } = this.state;
        // this.setState({ isChangeLocation: false, })
        if (address1 && city && state && pincode && save && houseNo) {
            getDataAddress.streetNumber = houseNo,
                getDataAddress.streetName = address1 + address2,
                getDataAddress.locality = city,
                getDataAddress.adminArea = state,
                getDataAddress.postalCode = pincode,
                this.setState({
                    getDataAddress,
                    isChangeLocation: false,
                })
        } else {
            this.dropdown.alertWithType('error', 'Error!', 'Please Enter All Fields');
            // this.setState({ isChangeLocation: false })
        }
    }
    onHandleConfirm = () => {
        let { getAddress, getDataAddress ,position} = this.state;
        this.setState({ isChangeLocation: false }, () => {
            let obj = {
                "lattitude": String(position.lat),
                "longitude": String(position.lng),
                "personId": "8054140508766732296",
            }
            var locationBody = JSON.stringify(obj)
            let locationData = HttpHelper(LocationUrl, 'POST', locationBody);
            locationData.then(locationResponse => {
                console.log(locationResponse,'locationResponse');
                this.props.navigation.navigate('Home', { locationAddress: getAddress && getAddress.length > 0 ? getAddress : getDataAddress })

            })
        })
    }
    render() {
        let { getAddress, getDataAddress } = this.state
        return (
            <>
                <StatusBar
                    backgroundColor={Colors.Primary}
                    barStyle="light-content"
                />
                <SafeAreaView backgroundColor={Colors.Primary}></SafeAreaView>
                <View style={{ flex: 1, backgroundColor: 'white' }}>
                    <TouchableOpacity onPress={() => this.handleBackPress()} style={{ padding: 10, zIndex: 9999999, flex: 0.2 }}>
                        <TouchableOpacity
                            style={[styles.closePanel, { alignSelf: 'flex-start', flex: 0.1 }]}
                            >
                            <Text style={{ width: 30, height: 30, textAlign: 'center', left: 20, alignSelf: 'center' }}>
                                <Fontisto
                                    name="arrow-left-l"
                                    size={27}
                                    color={Colors.Primary}
                                    style={{ top: 5 }}
                                /></Text>
                        </TouchableOpacity>
                    </TouchableOpacity>
                    <MapView
                        style={this.state.isChangeLocation ? { flex: 1, borderBottomColor: "orange", marginTop: -90 } : { flex: 1, borderBottomColor: "orange", marginTop: -150 }}
                        provider={PROVIDER_GOOGLE}
                        showsUserLocation={true}
                        showsMyLocationButton={true}
                        zoomEnabled={true}
                        scrollEnabled={true}
                        initialRegion={this.state.mapData}
                        onMapReady={this.onMapReady}
                        onRegionChangeComplete={this.handleRegionChange}
                        ref={ref => {
                            this.map = ref;
                        }}>
                        <MapView.Marker
                            coordinate={this.state.markerData}
                            title={this.state.selectAddress ? this.state.selectAddress.formattedAddress : ''}
                            onDragEnd={(e) => { e.stopPropagation(); this.selectAddress(e) }}
                            tracksViewChanges={true}
                            tracksInfoWindowChanges={false}
                            isPreselected={true}
                            onPress={(e) => { e.stopPropagation(); this.selectAddress(e) }}
                        />
                    </MapView>
                    
                    {!this.state.isChangeLocation && (
                        <View style={{ borderTopLeftRadius: 30, borderTopRightRadius: 30, padding: 10, backgroundColor: Colors.White, marginTop: -0 }}>
                            <Text style={{ left: '5%', fontFamily: 'Poppins-Medium', letterSpacing: 1, fontSize: 10, color: Colors.textColor1 }} >SELECT YOUR ACTIVE LOCATION</Text>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View style={{ flex: 0.8 }}>
                                    <Text style={{ fontFamily: 'Poppins-Bold', letterSpacing: 2 }}>
                                        <Icon
                                            name="add-location"
                                            size={30}
                                            color={Colors.lightOrangeColor}
                                            style={{}}
                                        />{getDataAddress ? getDataAddress.streetNumber + "," + getDataAddress.streetName : ''},</Text>
                                    <View style={{ left: 10 }}>
                                        <Text style={{ fontFamily: 'Poppins-Bold', letterSpacing: 2 }}>{getDataAddress ? getDataAddress.locality + ',' + getDataAddress.country : ''},</Text>
                                        <Text style={{ fontFamily: 'Poppins-Medium', letterSpacing: 1 }}>{getDataAddress ? getDataAddress.postalCode : ''},</Text>
                                        <Text style={{ fontFamily: 'Poppins-Medium', letterSpacing: 1 }}>{getDataAddress ? getDataAddress.adminArea : ''}.</Text>
                                    </View>
                                </View>
                                <View>
                                    <TouchableOpacity onPress={() => { this.gettingData(getAddress) }}>
                                        <Text style={{ fontFamily: 'Poppins-Medium', letterSpacing: 0.5, color: Colors.Primary }}>CHANGE</Text>
                                    </TouchableOpacity>
                                </View>
                                <View>
                                </View>
                            </View>
                            <View style={styles.btn}>
                                <TouchableOpacity onPress={() => this.onHandleConfirm()} style={styles.confirmButton}>
                                    <Text style={[styles.loginText, { color: Colors.White }]}>CONFIRM</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    )}
                    {this.state.isChangeLocation && (
                        <ScrollView style={this.state.keyboardState === "opened" ? styles.deliver1 : styles.deliver}>
                            <Text style={{ height: 20 }}></Text>
                            <View style={{ flexDirection: 'row', }}>
                                <View style={{ width: Dims.DeviceWidth }}>
                                    <FloatingLabel
                                        labelStyle={[styles.labelStyle, {}]}
                                        inputStyle={styles.inputStyle}
                                        onChange={event => this.handleChange(event, 'houseNo')}
                                        style={styles.floatingLabelView}
                                        value={this.state.houseNo}
                                    >
                                        HOUSE/FLAT/FLOOR NO.
                                    </FloatingLabel>
                                </View>
                                <View style={{ alignSelf: 'center', left: -40 }}>
                                    <Text style={{ textAlign: 'center', alignSelf: 'center', }}>
                                        <Icon
                                            name="add-location"
                                            size={27}
                                            color={Colors.white}
                                            style={{ top: 5 }}
                                        /></Text>
                                </View>
                            </View>
                            <FloatingLabel
                                labelStyle={styles.labelStyle}
                                inputStyle={styles.inputStyle}
                                onChange={event => this.handleChange(event, 'address1')}
                                style={styles.floatingLabelView}
                                value={this.state.address1}
                            >
                                ADDRESS LINE 1
                            </FloatingLabel>
                            <FloatingLabel
                                labelStyle={styles.labelStyle}
                                inputStyle={styles.inputStyle}
                                onChange={event => this.handleChange(event, 'address2')}
                                style={styles.floatingLabelView}
                                value={this.state.address2}
                            >
                                ADDRESS LINE 2(OPTIONAL)
                            </FloatingLabel>
                            <FloatingLabel
                                editable={false}
                                labelStyle={styles.labelStyle}
                                inputStyle={styles.inputStyle}
                                onChange={event => this.handleChange(event, 'city')}
                                style={styles.floatingLabelView}
                                value={this.state.city}
                            >
                                CITY
                            </FloatingLabel>
                            <FloatingLabel
                                editable={false}
                                labelStyle={styles.labelStyle}
                                inputStyle={styles.inputStyle}
                                onChange={event => this.handleChange(event, 'state')}
                                style={styles.floatingLabelView}
                                value={this.state.state}
                            >
                                STATE
                            </FloatingLabel>
                            <FloatingLabel
                                labelStyle={styles.labelStyle}
                                inputStyle={styles.inputStyle}
                                onChange={event => this.handleChange(event, 'pincode')}
                                style={styles.floatingLabelView}
                                value={this.state.pincode}
                            >
                                PINCODE
                            </FloatingLabel>
                            <FloatingLabel
                                labelStyle={styles.labelStyle}
                                inputStyle={styles.inputStyle}
                                onChange={event => this.handleChange(event, 'save')}
                                style={[styles.floatingLabelView, { marginBottom: 20 }]}
                                value={this.state.save}
                            >
                                SAVE AS(eg.Home,Work etc)
                            </FloatingLabel>
                            <View style={styles.btn}>
                                <TouchableOpacity onPress={() => this.relocatingAddress()} style={styles.loginBtn}>
                                    <Text style={styles.loginText}>CONFIRM LOCATION</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>)
                    }
                </View>
                <DropdownAlert
                    ref={(ref) => (this.dropdown = ref)}
                    containerStyle={{ backgroundColor: '#FF0000' }}
                    imageSrc={'https://facebook.github.io/react/img/logo_og.png'}
                />
            </>
        );

    };
}
export default map;
const styles = StyleSheet.create({
    deliver: {
        position: 'relative',
        bottom: 0,
        backgroundColor: Colors.Primary,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        height: Dims.DeviceHeight / 2.5,
        width: Dims.DeviceWidth,
        // top:-30
        marginTop: -40
    },
    deliver1: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        top: 30,
        backgroundColor: Colors.Primary,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        height: Dims.DeviceHeight / 0.2,
        width: Dims.DeviceWidth,
        marginBottom: 100
    },
    location: {
        position: 'relative',
        bottom: 0,
        backgroundColor: Colors.redColor,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        width: Dims.DeviceWidth,
    },
    location1: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        top: 30,
        backgroundColor: Colors.Primary,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        height: 100,
        width: Dims.DeviceWidth,
        marginBottom: 100
    },
    loginBtn: {
        width: Dims.DeviceWidth - 50,
        paddingVertical: 10,
        borderRadius: 2,
        backgroundColor: Colors.white
    },
    confirmButton: {
        width: Dims.DeviceWidth - 50,
        paddingVertical: 10,
        borderRadius: 2,
        backgroundColor: Colors.Primary
    },
    container2: {
        alignSelf: 'center'
    },
    gradient: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 7,
    },
    btn: {
        height: 50,
        alignSelf: 'center',
        margin: 20
    },
    loginText: {
        color: Colors.Primary,
        alignSelf: 'center',
        // justifyContent: 'center',
        fontFamily: 'Poppins-Medium',
        letterSpacing: 3,
        textAlign: 'center',
    },
    closePanel: {
        flexWrap: 'wrap',
        top: 20

    },
    labelStyle: {
        color: Colors.textColor,
        fontSize: 10,
        fontFamily: 'Poppins-Bold',
        borderBottomColor: Colors.White,
        letterSpacing: 1
    },
    floatingLabelView: {
        marginHorizontal: 20,
    },
    hidePasswordText: {
        fontSize: 16,
        textAlign: 'center', fontFamily: 'Poppins-Medium',
        textAlignVertical: 'center',
        // color:Colors.placeholderColor
        fontSize: 16,

    },
    inputStyle: {
        borderWidth: 0,
        // margin: 10,
        height: 45,
        fontFamily: 'Poppins-Medium',
        fontSize: 12,
        borderBottomColor: Colors.white,
        borderBottomWidth: 1,
        color: Colors.White

    },
});