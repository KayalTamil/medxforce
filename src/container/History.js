import React, { Component } from 'react';
import {
    Text,
    View,
    StatusBar,
    Image, StyleSheet, BackHandler, TouchableOpacity, Alert, TextInput, FlatList, DeviceEventEmitter, KeyboardAvoidingView, Switch
} from 'react-native';
import { Colors } from '../assets/Colors';
import { Dims } from '../components/Dims';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as RNLocalize from 'react-native-localize';
import { CustomPicker } from 'react-native-custom-picker';
import nodeEmoji from 'node-emoji';
import { getAllCountries } from 'react-native-country-picker-modal';
import { ScrollView } from 'react-native-gesture-handler';
import LocationServicesDialogBox from 'react-native-android-location-services-dialog-box';
import Geolocation from 'react-native-geolocation-service';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Footer from '../components/Footer';
import { Card } from 'react-native-shadow-cards';
import { HttpHelper } from '../HelperApi/Api/HTTPHelper';
import { LocationUrl } from '../HelperApi/Api/APIConfig';
var moment = require('moment');
import Geocoder from 'react-native-geocoder';
import SplashScreen from 'react-native-splash-screen';
import Swiper from 'react-native-swiper';
import Header from '../components/Header';

var sliderData = [{
    id: '1',
    latitude: 17.4317,
    longitude: 78.5654,
    km: 6,
    name: 'Mrs. Tamizh',
    image: require("../assets/images/home1.png"),
    appointmentDate: '10/01/2022',
    refNo: '#202209501',
    gender: 'Female',
    role: 'Nursing  Care',
    description: "She is friendly, caring and punctual."
},
{
    id: '2',
    latitude: 17.4657,
    longitude: 78.4283,
    km: 10,
    name: 'Mrs. Srinija',
    image: require("../assets/images/home2.jpg"),
    appointmentDate: '12/01/2022',
    refNo: '#202209502',
    gender: 'Female',
    role: 'Head Nurse',
    description: "She is friendly, caring and punctual"
},
{
    id: '3',
    latitude: 17.3990,
    longitude: 78.4867,
    km: 2,
    name: 'Mr. Arjun',
    image: require("../assets/images/home3.png"),
    appointmentDate: '09/01/2022',
    refNo: '#202209503',
    gender: 'Male',
    role: "Medical Travel Assistant",
    description: "He is friendly, caring and punctual."
},
{
    id: '3',
    latitude: 17.3990,
    longitude: 78.4867,
    km: 2,
    name: 'Mr. Arjun',
    image: require("../assets/images/home4.png"),
    appointmentDate: '09/01/2022',
    refNo: '#202209503',
    gender: 'Male',
    role: "Medical Travel Assistant",
    description: "He is friendly, caring and punctual."
},

]
var historyData = [{
    id: '1',

    name: 'Active',
    isActive: true

},
{
    id: '2',
    name: 'Completed',
    isActive: false

},
{
    id: '3',
    name: 'Cancelled',
    isActive: false

},


]


class History extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            options: [],
            locationAddress: params && params.locationAddress ? params.locationAddress : [],
            historyData: historyData,
            selectedId: 1,
            isActive: true

        };
    }

    componentDidMount() {
        SplashScreen.hide();

        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        let locationData = HttpHelper(`${LocationUrl}/8054140508766732296`, 'GET', '');
        locationData.then(locationResponse => {
            console.log(locationResponse.lattitude, '11');
            if (locationResponse) {
                var dataLatLng = {
                    lat: Number(locationResponse.lattitude),
                    lng: Number(locationResponse.longitude)
                };
                Geocoder.geocodePosition(dataLatLng).then(responseJson => {
                    console.log(responseJson, 'responseJson');
                    if (responseJson) {
                        this.setState(
                            {
                                locationAddress: responseJson && responseJson[0] ? responseJson[0] : '',
                                selectAddress: responseJson && responseJson[0] ? responseJson[0] : '',
                                getDataAddress: responseJson && responseJson[0] ? responseJson[0] : [],
                                isSelect: true,
                                position: responseJson[0].position
                            });
                    } else {
                    }
                })
                    .catch(err => console.log(err))
            }
        })

    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        Alert.alert(
            '',
            'Are you sure you want to exit the application ?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            },], {
            cancelable: false
        }
        )
        return true;
    };
    onHandle = (item) => {
        item.isActive = item.isActive ? false : true
        this.setState({
            selectedId: item.id,
        })
    }
    _renderItem = (item) => {
        let { selectedId } = this.state;
        return (
            <TouchableOpacity style={selectedId === item.id ? [styles.tabView, { backgroundColor: Colors.Primary }] : [styles.tabView]} onPress={() => this.onHandle(item)}>
                <Text style={selectedId === item.id ? [styles.tabText] : [styles.tabText, { color: Colors.Primary }]}>{item.name}</Text>
            </TouchableOpacity>
        )
    }
    historyHandle = () => {
        return (
            <View>
                {this.state.historyData && this.state.historyData.length > 0 && (
                    <FlatList
                        horizontal={true}
                        style={{ marginTop: 10 }}
                        showsVerticalScrollIndicator={true}
                        keyExtractor={(item, index) => item.id}
                        renderItem={({ item }) => this._renderItem(item)}
                        data={this.state.historyData}
                        extraData={this.state}
                    />
                )}
            </View>
        )
    }
    onHandleOrders = () => {
        return (
            <View style={{ justifyContent: 'center', flex: 1, height: Dims.DeviceHeight / 1.5, alignSelf: 'center' }}>
                <Image source={require('../assets/images/order.png')} style={{ height: 250, width: 150 }} resizeMode='cover' />
                <Text style={{ fontFamily: 'Poppins-Medium', marginTop: 20 }}>No Orders Raised</Text>
            </View>
        )
    }
    render() {
        let { locationAddress, getBannerImage } = this.state;
        let locatedAddress = [locationAddress.locality, locationAddress.country, locationAddress.adminArea]
        return (
            <>
                <StatusBar style={{ backgroundColor: Colors.Primary }} />

                <ScrollView style={{}}>
                    <Header navigation={this.props.navigation} history={true} title={'History'} pageName ={'Home'} />
                    {this.historyHandle()}
                    {this.onHandleOrders()}
                    <View style={{ height: 200 }}></View>
                </ScrollView>
                <Footer navigation={this.props.navigation} discountData={this.state.discountData} nearByData={this.state.nearByShopData} history={true} />
                <DropdownAlert
                    ref={(ref) => (this.dropdown = ref)}
                    containerStyle={{
                        backgroundColor: '#FF0000',
                    }}
                    imageSrc={'https://facebook.github.io/react/img/logo_og.png'}
                />
            </>
        );
    }
}

const styles = StyleSheet.create({
    tabView: {
        padding: 5,
        borderRadius: 30,
        marginHorizontal: 10,
        borderColor: Colors.Primary,
        borderWidth: 1,

    },
    tabText: { width: 120, textAlign: 'center', fontFamily: 'Poppins-Medium', color: Colors.White },


});

export default History;
