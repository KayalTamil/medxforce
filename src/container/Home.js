import React, { Component } from 'react';
import {
    Text,
    View,
    StatusBar,
    Image, StyleSheet, BackHandler, TouchableOpacity, Alert, TextInput, Modal, FlatList, DeviceEventEmitter, KeyboardAvoidingView, Switch
} from 'react-native';
import { Colors } from '../assets/Colors';
import { Dims } from '../components/Dims';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as RNLocalize from 'react-native-localize';
import { CustomPicker } from 'react-native-custom-picker';
import nodeEmoji from 'node-emoji';
import { getAllCountries } from 'react-native-country-picker-modal';
import { ScrollView } from 'react-native-gesture-handler';
import LocationServicesDialogBox from 'react-native-android-location-services-dialog-box';
import Geolocation from 'react-native-geolocation-service';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Footer from '../components/Footer';
import { Card } from 'react-native-shadow-cards';
import { HttpHelper } from '../HelperApi/Api/HTTPHelper';
import { LocationUrl, FindAllUrl, PersonUrl } from '../HelperApi/Api/APIConfig';
var moment = require('moment');
import Geocoder from 'react-native-geocoder';
import SplashScreen from 'react-native-splash-screen';
import Header from '../components/Header';
import { ActivityIndicator } from 'react-native-paper';


var expertiseData = [{
    id: '1',
    latitude: 17.4317,
    longitude: 78.5654,
    km: 6,
    name: 'Cardioac',
    image: require("../assets/images/group1.png"),
    appointmentDate: '10/01/2022',
    refNo: '#202209501',
    gender: 'Female',
    role: 'Nursing  Care',
    description: "She is friendly, caring and punctual."
},
{
    id: '2',
    latitude: 17.4657,
    longitude: 78.4283,
    km: 10,
    name: 'Numerology',
    image: require("../assets/images/group2.png"),
    appointmentDate: '12/01/2022',
    refNo: '#202209502',
    gender: 'Female',
    role: 'Head Nurse',
    description: "She is friendly, caring and punctual"
},
{
    id: '3',
    latitude: 17.3990,
    longitude: 78.4867,
    km: 2,
    name: 'Neprology',
    image: require("../assets/images/group3.png"),
    appointmentDate: '09/01/2022',
    refNo: '#202209503',
    gender: 'Male',
    role: "Medical Travel Assistant",
    description: "He is friendly, caring and punctual."
},
{
    id: '3',
    latitude: 17.3990,
    longitude: 78.4867,
    km: 2,
    name: 'Orthos',
    image: require("../assets/images/group4.png"),
    appointmentDate: '09/01/2022',
    refNo: '#202209503',
    gender: 'Male',
    role: "Medical Travel Assistant",
    description: "He is friendly, caring and punctual."
},
{
    id: '5',
    latitude: 17.3990,
    longitude: 78.4867,
    km: 2,
    name: 'Neprology',
    image: require("../assets/images/group5.png"),
    appointmentDate: '09/01/2022',
    refNo: '#202209503',
    gender: 'Male',
    role: "Medical Travel Assistant",
    description: "He is friendly, caring and punctual."
},
]
var upcomingData = [{
    "id": 1,
    "name": 'Tamizh Prithivi',
    "age": '25 Years',
    "distance": "5 kms away",
    "time": '2.00 hrs'
}, {
    "id": 2,
    "name": 'Srinija',
    "age": '29 Years',
    "distance": "5 kms away",
    "time": '2.00 hrs'
}]

class Home1 extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            options: [],
            upcomingData: upcomingData,
            locationAddress: params && params.locationAddress ? params.locationAddress : [],
            getAllData: [],
            getExpertiseData: expertiseData,
            getBookData: {},
            isModalVisible: false,
            selectId: ""
        };
    }

    componentDidMount() {
        SplashScreen.hide();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.getLocationData()
        this.getAllData()
    }
    getLocationData = () => {
        let locationData = HttpHelper(`${LocationUrl}/8054140508766732296`, 'GET', '');
        locationData.then(locationResponse => {
            console.log(locationResponse.lattitude, '11');
            if (locationResponse) {
                var dataLatLng = {
                    lat: Number(locationResponse.lattitude),
                    lng: Number(locationResponse.longitude)
                };
                Geocoder.geocodePosition(dataLatLng).then(responseJson => {
                    if (responseJson) {
                        this.setState(
                            {
                                locationAddress: responseJson && responseJson[0] ? responseJson[0] : '',
                                selectAddress: responseJson && responseJson[0] ? responseJson[0] : '',
                                getDataAddress: responseJson && responseJson[0] ? responseJson[0] : [],
                                isSelect: true,
                                position: responseJson[0].position
                            });
                    } else {
                    }
                })
                    .catch(err => console.log(err))
            }
        })
    }

    getAllData = () => {
        let { getAllData } = this.state;
        let getData = HttpHelper(FindAllUrl, 'GET', '');
        getData.then(response => {
            console.log(response, 'getAllData');
            if (response && response.length > 0) {
                this.setState({
                    getAllData: response
                })
            } else {
                this.setState({
                    getAllData: []
                })
            }
        })

    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        Alert.alert(
            '',
            'Are you sure you want to exit the application ?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            },], {
            cancelable: false
        }
        )
        return true;
    };
    renderSideMenuContent = () => {
        return (
            <>
                <ScrollView>
                    <Text>'Hi'</Text>

                </ScrollView>

            </>
        );
    };
    renderMainContent = () => {
        if (!this.state.drawerOpen) {
            return (
                <View>
                    <Entypo name="menu" size={38} color={CustomColors.Black} />
                </View>
            );
        }
    };
    _renderItem = (item) => {
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Location')} style={{ alignSelf: 'center' }}>
                <Card style={{ marginBottom: 10, height: 60, width: 60, margin: 5, alignItems: 'center', justifyContent: 'center' }}>
                    <Image resizeMode={"contain"} style={{ height: 30, width: 30 }} source={item.image} />
                    <Text style={{ fontFamily: 'Poppins-Bold', fontSize: 8, textAlign: 'center', top: 5 }}>{item.name}</Text>
                </Card>
            </TouchableOpacity>

        )
    }
    onViewOffer = () => {
        return (
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
                <View style={{ backgroundColor: '#ccece1', flex: 0.3, height: 80, borderRadius: 10, alignSelf: 'center' }}>
                    <Text style={{ textAlign: 'center', marginTop: 7 }}>
                        <Ionicons
                            name="calendar-outline"
                            size={25}
                            color={'yellow'}
                            style={{}}
                        /></Text>
                    <Text style={{ textAlign: 'center', fontFamily: 'Poppins-Medium', fontSize: 10, color: Colors.greyColor }}>15 Years {'\n'} Experience</Text>
                </View>
                <View style={{ backgroundColor: '#f6dec6', flex: 0.3, height: 80, borderRadius: 10, alignSelf: 'center' }}>
                    <Text style={{ textAlign: 'center', marginTop: 7 }}>
                        <Ionicons
                            name="heart"
                            size={25}
                            color={'red'}
                            style={{}}
                        /></Text>
                    <Text style={{ textAlign: 'center', fontFamily: 'Poppins-Medium', fontSize: 10, color: Colors.greyColor }}>Served {'\n'} 200 patients</Text>
                </View>
                <View style={{ backgroundColor: '#c3e4d9', flex: 0.3, height: 80, borderRadius: 10, alignSelf: 'center' }}>
                    <Text style={{ textAlign: 'center', marginTop: 7 }}>
                        <MaterialIcons
                            name="thumb-up"
                            size={25}
                            color={'blue'}
                            style={{}}
                        /></Text>
                    <Text style={{ textAlign: 'center', fontFamily: 'Poppins-Medium', fontSize: 10, color: Colors.greyColor }}>Recommended {'\n'}by 200 patients</Text>
                </View>

            </View>
        )
    }
    onHandleNavigate = (item) => {
        let { getBookData, selectId } = this.state;
        this.setState({
            selectId: item.id
        })
        let getDatas = HttpHelper(`${PersonUrl}serviceName=${item.name}`, 'GET', '');
        getDatas.then(response => {
            if (response && response.length > 0) {
                this.setState({
                    getBookData: response,
                    isModalVisible: false
                }, () => {
                    this.props.navigation.navigate('Location', { getBookData: this.state.getBookData })
                })
            } else {
                this.setState({
                    getBookData: [],
                    isModalVisible: true,
                    selectId: ''
                })
            }
        })
    }
    _renderSliderItem = (item, index) => {
        let { selectId } = this.state;
        return (
            <View style={{ alignSelf: 'center', padding: 10 }}  >
                <Card style={{ marginTop: 20, height: Dims.DeviceHeight / 6, width: Dims.DeviceWidth / 2.5, }}>
                    <View>
                        <Image
                            style={{
                                height: '70%',
                                width: '100%',
                            }}
                            resizeMode="cover"
                            source={{ uri: `data:image/jpeg;base64,${item.image}` }}
                        />
                        <TouchableOpacity onPress={() => { this.onHandleNavigate(item, index) }} style={{ backgroundColor: Colors.Primary, borderRadius: 30, width: '40%', alignSelf: 'flex-end', marginTop: -10 }}>
                            <Text style={{ color: Colors.White, fontFamily: 'Poppins-Bold', fontSize: 12, textAlign: 'center' }}>{selectId === item.id ? <ActivityIndicator color='white' size='small' style={{}} /> : 'BOOK'}</Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontFamily: 'Poppins-Bold', left: 10, fontSize: 10, marginTop: -20 }}>{item.name}</Text>
                    <Text numberOfLines={2} style={{ left: 10, fontFamily: 'Poppins-Medium', maxWidth: Dims.DeviceWidth / 2, fontSize: 10 }}>{"She is friendly, caring and punctual"}</Text>
                </Card>
            </View>
        )
    }
    swiperonHandle = () => {
        let { getAllData } = this.state;
        if (getAllData && getAllData.length > 0) {
            return (
                <FlatList
                    horizontal
                    style={{ marginTop: 5, flexDirection: 'row' }}
                    showsVerticalScrollIndicator={true}
                    keyExtractor={(item, index) => item.id}
                    renderItem={({ item }) => this._renderSliderItem(item)}
                    data={this.state.getAllData}
                    extraData={this.state}
                />
            )
        } else {
            return (
                <ActivityIndicator color={Colors.Primary} size={'large'} style={{ padding: 10 }} />
            )
        }
    }
    expertiseHandle = () => {
        return (
            <View>
                <Text style={{ fontFamily: 'Poppins-Bold', fontSize: 16 }}>Search by expertise</Text>
                <View>
                    {this.state.getExpertiseData && this.state.getExpertiseData.length > 0 && (
                        <FlatList
                            horizontal={true}
                            style={{ marginTop: 10 }}
                            showsVerticalScrollIndicator={true}
                            keyExtractor={(item, index) => item.id}
                            renderItem={({ item }) => this._renderItem(item)}
                            data={this.state.getExpertiseData}
                            extraData={this.state}
                        />
                    )}
                </View>
            </View>
        )
    }
    upcomingService = () => {
        return (
            <>
                <View style={{ marginTop: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ height: 1, width: '30%', borderColor: Colors.placeholderColor, borderWidth: 1 }}></Text>
                    <Text style={{ fontFamily: 'Poppins-Medium', width: '40%', justifyContent: 'flex-start', top: -10, left: 20 }}>OTHER SERVICES</Text>
                    <Text style={{ height: 1, width: '30%', borderColor: Colors.placeholderColor, borderWidth: 1 }}></Text>
                </View>
                <View>
                    <Card style={{ flexDirection: 'row', backgroundColor: Colors.Primary, width: Dims.DeviceWidth - 50, height: 50, borderRadius: 10 }}>
                        <Image source={require('../assets/images/ambulance.png')} resizeMode='contain' style={{ height: 80, width: 80, top: -30 }} />
                        <Text style={{ fontFamily: 'Poppins-Medium', width: '40%', alignSelf: 'center', left: 20, color: Colors.White }}>
                            Book an Ambulance</Text>
                    </Card>
                </View>
                <View style={{ marginTop: 20 }}>
                    <Card style={{ flex: 1, flexDirection: 'row', backgroundColor: Colors.Primary, width: Dims.DeviceWidth - 50, height: 50, borderRadius: 10, justifyContent: 'space-between' }}>
                        <Text style={{ fontFamily: 'Poppins-Medium', width: '40%', alignSelf: 'center', left: 20, color: Colors.White }}>
                            Book a Lab Test</Text>
                        <Image source={require('../assets/images/man.png')} style={{ top: -20 }} />
                    </Card>
                </View>
            </>
        )
    }
    upcomingHandle = () => {
        return (
            <View style={{ marginTop: 20 }}>
                <Text style={{ fontFamily: 'Poppins-Bold', fontSize: 16 }}>Upcoming Appointment</Text>
                <View style={{ flexDirection: 'row', padding: 10 }}>
                    <Text style={{ height: 40, borderColor: Colors.Primary, borderWidth: 3 }}></Text>
                    <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 16, alignSelf: 'center', left: 20 }}> No Upcoming Appointments</Text>

                </View>
            </View>
        )
    }
    onOkHandle = () => {
        this.setState({
            isModalVisible: false
        })
    }
    render() {
        let { locationAddress, getAllData } = this.state;
        let locatedAddress = [locationAddress.locality, locationAddress.country, locationAddress.adminArea]
        return (
            <>
                <StatusBar style={{ backgroundColor: Colors.Primary }} />
                <Header navigation={this.props.navigation} home={true} pageName={'SearchLocation'} />
                <ScrollView style={{}}>
                    <View style={{ padding: 20 }}>
                        <Text style={{ fontFamily: 'Poppins-Bold', fontSize: 18, }}>What are you looking for?</Text>
                        <View style={{ flexDirection: 'row', borderColor: Colors.placeholderColor, borderWidth: 1, justifyContent: 'space-between', borderRadius: 30, paddingHorizontal: 10, marginTop: 10 }}>
                            <TextInput
                                numberOfLines={1}
                                style={{ fontFamily: 'Poppins-Medium', fontSize: 12, maxWidth: Dims.DeviceWidth / 1.3 }}
                                placeholder='Search for Primary Care,Iv,etc...'
                                onChangeText={(addressInfo) => this.setState({ addressInfo })}
                                value={this.state.addressInfo} />
                            <Text style={{ alignSelf: 'center' }}>
                                <AntDesign
                                    name="search1"
                                    size={25}
                                    color={Colors.Primary}
                                    style={{}}
                                /></Text>
                        </View>
                        {this.swiperonHandle()}
                        {this.expertiseHandle()}
                        {this.upcomingHandle()}
                        {this.upcomingService()}

                    </View>
                    <View style={{ height: 50 }}></View>
                    <View style={styles.centeredView}>
                        <Modal
                            animated
                            animationType="slide"
                            transparent={true}
                            visible={this.state.isModalVisible}
                            style={{
                                opacity: 0.5,
                            }}
                            onRequestClose={() => {
                                this.setState({ isModalVisible: !this.state.isModalVisible, isOpacity: false })
                            }}>
                            <View style={styles.centeredView}>
                                <View style={styles.modalView}>
                                    <View style={{ height: Dims.DeviceHeight / 5, width: Dims.DeviceWidth - 40, borderTopLeftRadius: 60, borderTopRightRadius: 60, alignSelf: 'center' }}>
                                        <Text style={{ fontFamily: 'Poppins-Bold', textAlign: 'center', margin: 10, fontSize: 15 }}>Nobody is Available</Text>
                                        <Text style={{ fontFamily: 'Poppins-Medium', textAlign: 'center', margin: 10, fontSize: 13 }}>Please continue to  your offer services.</Text>
                                        <TouchableOpacity style={{ backgroundColor: Colors.orangeColor, width: 150, borderRadius: 30, alignSelf: 'center', top: 10 }} onPress={() => { this.onOkHandle() }}>
                                            <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.White, textAlign: 'center', margin: 10 }}>Continue</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </Modal>
                    </View>
                </ScrollView>
                <Footer navigation={this.props.navigation} discountData={this.state.discountData} nearByData={this.state.nearByShopData} home={true} />
                <DropdownAlert
                    ref={(ref) => (this.dropdown = ref)}
                    containerStyle={{
                        backgroundColor: '#FF0000',
                    }}
                    imageSrc={'https://facebook.github.io/react/img/logo_og.png'}
                />
            </>
        );
    }
}

const styles = StyleSheet.create({
    dotView: {
        flexDirection: 'row',
        alignSelf: 'flex-end'
    },
    logo: {
        height: 120,
        width: 120,
        borderRadius: 100,
    },
    nurseLogo: {
        height: 280,
        left: -150
    },
    welcomeText: { fontFamily: 'Poppins-Medium', fontSize: 16, color: Colors.Primary, letterSpacing: 4, top: 10 },
    defaultInput: {
        color: Colors.Black,
        fontSize: 12,
        top: 10,
        justifyContent: 'center',

    },
    textInputView: {
        paddingLeft: 15,
    },
    container: {
        flexDirection: 'row',
    },
    innerContainer: {
        flexDirection: 'row',
        alignItems: 'stretch',

    },
    hidePasswordText: {
        fontSize: 16,
        textAlign: 'center', fontFamily: 'Poppins-Medium',
        flex: 0.9
    },
    headerFooterContainer: {
        alignItems: 'center',
    },
    inputText: {
        alignSelf: 'center',
        width: Dims.DeviceWidth * 0.5,
        height: 50,
        borderColor: Colors.placeholderColor,
        borderWidth: 1,
        fontSize: 16,
        textAlign: 'center',
        backgroundColor: '#fcfcfc',
        fontFamily: 'Poppins-Medium',
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
    },
    modalView: {
        marginHorizontal: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        shadowColor: Colors.greyColor,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
});

export default Home1;
