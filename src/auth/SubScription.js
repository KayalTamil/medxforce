import React, { Component } from 'react';
import {
    Text,
    View,
    StatusBar,
    Image, StyleSheet, BackHandler, TouchableOpacity, TextInput, FlatList, KeyboardAvoidingView, ScrollView
} from 'react-native';
import { Colors } from '../assets/Colors';
import { Dims } from '../components/Dims';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as RNLocalize from 'react-native-localize';
import { CustomPicker } from 'react-native-custom-picker';
import nodeEmoji from 'node-emoji';
import { getAllCountries } from 'react-native-country-picker-modal';
import OTPTextView from 'react-native-otp-textinput';
var planItems = [
    {
        "id": 1,
        "month": "6 months",
        "amount": 599,
        "discountAmount": 1599
    },
    {
        "id": 2,
        "month": "12 months",
        "amount": 999,
        "discountAmount": 2999
    },

]
class LoginVerify extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;

        this.state = {
            options: [],
            mobileNumber: '1234567890',
            otpInput: '',
            subscripionPlanData: planItems
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        const deviceCountry = RNLocalize.getCountry();
        getAllCountries().then(cl => {
            this.setState({ options: cl });
            var userCountryData = cl
                .filter(country => country.name === "India")
                .pop();
            this.setState({ value: userCountryData }, () => {
            });
            // this.props.onPickerLoad(userCountryData);
        });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.navigate('Home')
        return true;
    };

    _renderSectorItem = (item, index) => {
        return (
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('Location') }} style={{ flexDirection: 'column', backgroundColor: Colors.White, padding: 10, marginTop: 10, borderRadius: 10 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 12 }}>{item.month}</Text>
                    <Text style={{ fontFamily: 'Poppins-Bold', fontSize: 8, color: Colors.redColor }}>{'% INAUGURAL OFFER'}</Text>
                </View>
                <Text style={{ alignSelf: 'flex-end' }}>
                    <MaterialIcons
                        name="arrow-forward-ios"
                        size={20}
                        color={Colors.Primary}
                    />
                </Text>
                <View style={{ flexDirection: 'row', }}>
                    <Text style={{ fontFamily: 'Poppins-Bold', fontSize: 20, color: Colors.Primary, letterSpacing: 3 }}><Text style={{ fontFamily: 'none' }}>{'\u20B9'}</Text>{item.amount}</Text>
                    <Text style={{ fontFamily: 'Poppins-Medium', left: 15, top: 8, fontSize: 12, textDecorationLine: 'line-through', letterSpacing: 3 }}><Text style={{ fontFamily: 'none' }}>{'\u20B9'}</Text>{item.discountAmount}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <>
                <StatusBar style={{ backgroundColor: Colors.Primary }} />
                <ScrollView contentInsetAdjustmentBehavior="automatic" keyboardShouldPersistTaps="always"  >
                    <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "position" : null} style={{ flex: 1 }}>
                        <View>
                            <View style={[styles.dotView, { justifyContent: 'space-between' }]}>
                                <TouchableOpacity onPress={() => { this.handleBackPress() }} style={[styles.dotView, { alignSelf: 'center' }]}>
                                    <Text style={{ marginLeft: 15 }}>
                                        <MaterialIcons
                                            name="verified-user"
                                            size={20}
                                            color={Colors.Primary}
                                        />
                                    </Text>
                                    <Text style={{ fontFamily: 'Poppins-Bold', color: Colors.secondaryTextColor, marginLeft: 5, fontSize: 12, letterSpacing: 1 }}>
                                        VERIFIED
                                    </Text>
                                </TouchableOpacity>
                                <View style={styles.dotView}>
                                    <Entypo
                                        name="dot-single"
                                        size={40}
                                        color={Colors.Primary}
                                    />
                                    <Entypo
                                        name="dot-single"
                                        size={40}
                                        color={Colors.Primary}
                                        style={{ marginLeft: -15 }}
                                    />
                                    <Entypo
                                        name="dot-single"
                                        size={40}
                                        color={Colors.Primary}
                                        style={{ marginLeft: -15 }}
                                    />
                                </View>
                            </View>
                            <View style={{ justifyContent: 'space-evenly', flexDirection: 'row', paddingHorizontal: 20, }}>
                                <View style={{ flex: 0.5, top: 30 }}>
                                    <Text style={{ fontFamily: 'Poppins-Bold', color: Colors.Primary }}> HI Tamizh,</Text>
                                    <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.placeholderColor, fontSize: 9.5 }}>Quote your hourly pricing per list.</Text>
                                    <View style={{ flexDirection: 'row', marginVertical: 10 }}>
                                        <TextInput
                                            style={styles.inputText1}
                                            editable={false}
                                            defaultValue={'\u20B9'}
                                        />
                                        <TextInput
                                            keyboardType="number-pad"
                                            style={styles.inputText}
                                            placeholder={'Min.'}
                                            autoCapitalize="none"
                                            returnKeyType="next"
                                            placeholderTextColor={Colors.lightWhiteColor}
                                            onChangeText={(email) => this.setState({ email })}
                                        // onSubmitEditing={() => this.passWord.focus()}
                                        />
                                    </View>
                                    <View style={{ flexDirection: 'row', marginVertical: 10 }}>
                                        <TextInput
                                            style={styles.inputText1}
                                            editable={false}
                                            defaultValue={'\u20B9'}
                                        />
                                        <TextInput
                                            keyboardType="number-pad"
                                            style={styles.inputText}
                                            placeholder={'Max.'}
                                            autoCapitalize="none"
                                            returnKeyType="next"
                                            placeholderTextColor={Colors.lightWhiteColor}
                                            onChangeText={(email) => this.setState({ email })}
                                        // onSubmitEditing={() => this.passWord.focus()}
                                        />
                                    </View>
                                </View>
                                <View style={{ flex: 0.5, }}>
                                    <Image
                                        source={require('../assets/images/nurseRight.png')}
                                        style={[styles.nurseLogo, { width: 200, height: 300 }]}
                                        resizeMode="cover"
                                    />
                                </View>
                            </View>
                            <View style={{ backgroundColor: Colors.Primary, borderTopLeftRadius: 60, borderTopRightRadius: 60, marginTop: -70, }}>
                                <Text style={{ fontFamily: 'Poppins-ExtraBold', letterSpacing: 7, color: Colors.White, textAlign: 'center', margin: 20, }}>SUBSCRIPTION</Text>
                                <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.White, textAlign: 'center', margin: 10, letterSpacing: 1 }}>Select a plan to activate {'\n'}your account</Text>
                                <FlatList
                                    style={{ paddingHorizontal: 20, borderRadius: 30, height: Dims.DeviceHeight / 2 }}
                                    keyExtractor={(item, index) => item.id}
                                    renderItem={({ item, index }) => this._renderSectorItem(item, index)}
                                    data={this.state.subscripionPlanData}
                                />
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
                <View style={{
                    alignItems: 'center',
                    position: 'absolute',
                    flex: 1,
                    bottom: 0,
                    width: '100%',
                    backgroundColor: Colors.Primary,
                    height: 30,
                    justifyContent: 'center'
                }}>
                    {/* <TouchableOpacity onPress={() => { this.props.navigation.navigate('LoginVerify') }} style={{ flexDirection: 'row', justifyContent: 'center', height: 50 }}>
                        <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 16, color: Colors.White, width: 210 }}></Text>
                        <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 14, color: Colors.White }}>Pay Later</Text>
                        <Text style={{ width: 30, height: 30, borderRadius: 50, backgroundColor: Colors.White, textAlign: 'center', left: 20, top: -5 }}>
                            <MaterialIcons
                                name="arrow-right-alt"
                                size={28}
                                color={Colors.Primary}
                            // style={{ marginLeft: -15 }}
                            /></Text>
                    </TouchableOpacity> */}
                    <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.White, fontSize: 9 }}>By continuing you agree to our <Text style={{ fontFamily: 'Poppins-Bold', textDecorationLine: 'underline' }}>Terms & Conditions</Text> and <Text style={{ fontFamily: 'Poppins-Bold', textDecorationLine: 'underline' }}>Payment Policy</Text> </Text>

                </View>
                <DropdownAlert
                    ref={(ref) => (this.dropdown = ref)}
                    containerStyle={{
                        backgroundColor: '#FF0000',
                    }}
                    imageSrc={'https://facebook.github.io/react/img/logo_og.png'}
                />
            </>
        );
    }
}

const styles = StyleSheet.create({
    dotView: {
        flexDirection: 'row',
    },
    backView: {
        flexDirection: 'row',
    },
    logo: {
        height: Dims.DeviceHeight / 3,
        width: Dims.DeviceWidth - 50
    },
    headerLogo: {
        height: 25,
        width: 150,
    },
    nurseLogo: {
        // height: 300,
        // left: -170
    },
    welcomeText: { fontFamily: 'Poppins-Medium', fontSize: 16, color: Colors.Primary, letterSpacing: 4, top: 10 },
    defaultInput: {
        color: Colors.Black,
        fontSize: 12,
        top: 10,
        justifyContent: 'center',

    },
    textInputView: {
        paddingLeft: 15,
    },
    container: {
        flexDirection: 'row',
    },
    innerContainer: {
        flexDirection: 'row',
        alignItems: 'stretch',

    },
    hidePasswordText: {
        fontSize: 16,
        textAlign: 'center', fontFamily: 'Poppins-Medium',
        flex: 0.9
    },
    headerFooterContainer: {
        alignItems: 'center',
    },
    inputText: {
        width: 100,
        height: 40,
        fontSize: 10,
        // backgroundColor: '#fcfcfc',
        fontFamily: 'Poppins-Medium',
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30,
        backgroundColor: Colors.Primary,
        color: Colors.White
    },
    inputText1: {
        alignSelf: 'center',
        width: 40,
        height: 40,
        fontSize: 16,
        // fontFamily: 'Poppins-Medium',
        textAlign: 'center',
        backgroundColor: '#fcfcfc',
        borderTopLeftRadius: 30,
        borderBottomLeftRadius: 30,
        backgroundColor: Colors.Primary,
        color: Colors.White
    },
    textInputContainer: {
        marginHorizontal: 40,
    },
    roundedTextInput: {
        height: 70,
        fontFamily: 'Poppins-Medium'
    },
});

export default LoginVerify;
