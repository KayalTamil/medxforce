import React, { Component } from 'react';
import {
    Text,
    View,
    StatusBar,
    Image, StyleSheet, BackHandler, TouchableOpacity, TextInput, Picker, Alert, Modal, FlatList
} from 'react-native';
import { Colors } from '../assets/Colors';
import { Dims } from '../components/Dims';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';

import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as RNLocalize from 'react-native-localize';
import { CustomPicker } from 'react-native-custom-picker';
import nodeEmoji from 'node-emoji';
import { getAllCountries } from 'react-native-country-picker-modal';
import { ScrollView } from 'react-native-gesture-handler';
import { Card } from 'react-native-shadow-cards';
import DateTimePicker from '@react-native-community/datetimepicker';
import { RadioButton } from 'react-native-paper';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FloatingLabel from 'react-native-floating-labels';
import { HttpHelper } from '../HelperApi/Api/HTTPHelper';
import { RegisterUrl, RegisterOtpUrl, RegisterMobileUrl, RegisterMobileVerifyUrl, CategoryUrl } from '../HelperApi/Api/APIConfig';
import OTPTextView from 'react-native-otp-textinput';
const googleApiKey = 'AIzaSyAFn3Q1LhhC0C8pfWhNyHwfHWYFT7S2s_M';
const ASPECT_RATIO = Dims.DeviceWidth / Dims.DeviceHeight;
const LATITUDE_DELTA = 0.0899;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const CARD_HEIGHT = Dims.DeviceHeight / 6;
const CARD_WIDTH = CARD_HEIGHT - 50;
var moment = require('moment');
const date = new Date();

var staticCategory = [
    {
        "id": 1,
        "categoryName": 'Tamizh',
    },
    {
        "id": 2,
        "categoryName": 'Srinija',
    },
]
var staticGender = [
    {
        "id": 1,
        "genderName": 'Mr.',
    },
    {
        "id": 2,
        "genderName": 'Miss.',
    },
]
var staticEducational = [
    {
        "id": 1,
        "eduName": 'B.E',
    },
    {
        "id": 2,
        "eduName": 'B.S.C',
    },
    {
        "id": 3,
        "eduName": 'M.E',
    },
]
var staticExperince = [
    {
        "id": 1,
        "noOfYear": '1',
    },
    {
        "id": 2,
        "noOfYear": '2',
    },
    {
        "id": 3,
        "noOfYear": '3',
    },
]
class Register extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            options: [],
            getCategoryData: [],
            selectCategoryId: '',
            selectGenderId: "Mr.",
            getGenderData: staticGender,
            getEducationalData: staticEducational,
            selectEduId: "",
            checkedPrefer: false,
            checkedMale: true,
            checkedFemale: false,
            selectExpe: "",
            getExpeData: staticExperince,
            addressPincode: '',
            addressCity: '',
            addressState: "",
            addressCountry: '',
            addressInfo: '',
            lastName: '',
            mailId: '',
            mobileNo: '',
            isModalVisible: false,
            otpInput: '',
            isVerified: false,
            timer: null,
            counter: 0

        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        let timer = setInterval(this.tick, 1000);
        this.setState({ timer });
        this.onHandleCategory()
        const deviceCountry = RNLocalize.getCountry();
        getAllCountries().then(cl => {
            this.setState({ options: cl });
            var userCountryData = cl
                .filter(country => country.name === "India")
                .pop();
            this.setState({ value: userCountryData }, () => {
            });
            // this.props.onPickerLoad(userCountryData);
        });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
        clearInterval(this.state.timer);
        this.onHandleCategory()
    }

    handleBackPress = () => {
        this.props.navigation.navigate('Login')
        return true;
    };
    tick = () => {
        if (this.state.isModalVisible) {
            this.setState({
                counter: this.state.counter + 1
            });
        } else {
            this.setState({
                counter: this.state.counter
            });
        }

    }
    onValueChange = selectedValue => {
        this.setState({ value: selectedValue });
    };

    updatePicker = (value, type) => {
        if (type === "category") {
            this.setState({
                selectCategoryId: value
            })
        } else if (type === "gender") {
            this.setState({
                selectGenderId: value
            })
        } else if (type === "educational") {
            this.setState({
                selectEduId: value
            })
        } else if (type === "experience") {
            this.setState({
                selectExpe: value
            })
        }

    }
    renderItem = (type) => {
        if (type === "category") {
            if (this.state.getCategoryData && this.state.getCategoryData.length > 0) {
                return this.state.getCategoryData.map(item => (
                    <Picker.Item label={item} value={item} fontFamily={'Poppins-Medium'} style={{ fontFamily: 'Poppins-Medium' }} key={item.id} />
                ));
            }
        } else if (type === "gender") {
            if (this.state.getGenderData != null && this.state.getGenderData.length > 0) {
                return this.state.getGenderData.map(item => (
                    <Picker.Item label={item.genderName} value={item.genderName} style={{ fontFamily: 'Poppins-Medium' }} key={item.id} />
                ));
            }
        } else if (type === "educational") {
            if (this.state.getEducationalData != null && this.state.getEducationalData.length > 0) {
                return this.state.getEducationalData.map(item => (
                    <Picker.Item label={item.eduName} value={item.eduName} style={{ fontFamily: 'Poppins-Medium' }} key={item.id} />
                ));
            }
        }
        else if (type === "experience") {
            if (this.state.getExpeData != null && this.state.getExpeData.length > 0) {
                return this.state.getExpeData.map(item => (
                    <Picker.Item label={item.noOfYear} value={item.noOfYear} style={{ fontFamily: 'Poppins-Medium' }} key={item.id} />
                ));
            }
        }

    }
    handleCategory = () => {
        return (
            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between' }}>
                <View style={{ backgroundColor: Colors.White, width: Dims.DeviceWidth - 50, borderRadius: 10 }}>
                    <Picker
                        selectedValue={this.state.selectCategoryId}
                        mode="dropdown"
                        itemTextStyle={{ fontFamily: 'Poppins-Medium' }}
                        textStyle={{ fontFamily: 'Poppins-Medium' }}
                        style={{
                            height: 50,
                            padding: 20,
                            width: Dims.DeviceWidth - 50,
                            color: Colors.Primary,
                        }}
                        placeholderTextColor={Colors.placeholderColor}
                        onValueChange={itemValue => {
                            setTimeout(() => {
                                this.updatePicker(itemValue, 'category');
                            }, 0);
                        }}>
                        <Picker.Item
                            color={Colors.Primary}
                            label={'Select Category'}
                            value={0}
                        />
                        {this.renderItem('category')}
                    </Picker>
                </View>
                {/* <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontFamily: 'Poppins-Medium', alignSelf: 'center' }}>Step{'  '}</Text>
                    <Text style={{ height: 15, width: 15, backgroundColor: Colors.Primary, borderRadius: 50, fontSize: 8, textAlign: 'center', alignItems: 'center', color: Colors.White, alignSelf: 'center' }}>1</Text>
                    <Text style={{ alignSelf: 'center' }}>
                        <AntDesign
                            name="minus"
                            size={40}
                            color={Colors.placeholderColor}
                            style={{}}
                        /></Text>
                    <Text style={{ height: 15, width: 15, backgroundColor: Colors.placeholderColor, borderRadius: 50, fontSize: 8, textAlign: 'center', alignItems: 'center', color: Colors.White, alignSelf: 'center' }}>2</Text>

                </View> */}
            </View>
        )
    }
    handleChange = (event, type) => {
        if (type === "firstName") {
            this.setState({ firstName: event.nativeEvent.text });
        } else if (type === "lastName") {
            this.setState({ lastName: event.nativeEvent.text });
        } else if (type === "mobileNo") {
            this.setState({ mobileNo: event.nativeEvent.text });
        } else if (type === "mailId") {
            this.setState({ mailId: event.nativeEvent.text });
        } else if (type === "city") {
            this.setState({ city: event.nativeEvent.text });
        } else if (type === "pincode") {
            this.setState({ pincode: event.nativeEvent.text });
        } else if (type === "save") {
            this.setState({ save: event.nativeEvent.text });
        }
    };
    handleFirstName = () => {
        return (
            <View style={[styles.inputWraps, { marginHorizontal: 20, borderBottomColor: Colors.White, borderBottomWidth: 1, }]}>
                <View style={{ alignSelf: 'center' }}>
                    <Picker
                        selectedValue={this.state.selectGenderId}
                        mode="dropdown"
                        itemTextStyle={{ fontFamily: 'Poppins-Medium' }}
                        textStyle={{ fontFamily: 'Poppins-Medium' }}
                        style={{
                            color: Colors.lightWhiteColor,
                            width: Dims.DeviceWidth / 3.5,
                            textDecorationLine: 'underline'
                        }}
                        placeholderTextColor={Colors.Black}
                        onValueChange={itemValue => {
                            setTimeout(() => {
                                this.updatePicker(itemValue, 'gender');
                            }, 0);
                        }}>
                        {this.renderItem('gender')}
                    </Picker>
                </View>
                <View style={{ flex: 1 }}>
                    <FloatingLabel
                        labelStyle={[styles.labelStyle]}
                        inputStyle={styles.inputStyle}
                        onChange={event => this.handleChange(event, 'firstName')}
                        style={styles.floatingLabelView}
                        value={this.state.firstName}
                    >
                        First Name
                    </FloatingLabel>
                </View>
            </View>

        )
    }
    handleLastName = () => {
        return (
            <View style={[styles.inputWraps, { margin: 20, borderBottomColor: Colors.White, borderBottomWidth: 1, }]}>
                <View style={{ flex: 1 }}>
                    <FloatingLabel
                        labelStyle={[styles.labelStyle]}
                        inputStyle={styles.inputStyle}
                        onChange={event => this.handleChange(event, 'lastName')}
                        style={styles.floatingLabelView}
                        value={this.state.lastName}
                    >
                        Last Name
                    </FloatingLabel>
                </View>
            </View>
        )
    }
    handleDob = () => {
        return (

            <View style={[styles.inputWraps, { margin: 20, borderBottomColor: Colors.White, borderBottomWidth: 1, }]}>
                <View style={{ flex: 1 }}>
                    <TouchableOpacity onPress={() => this.setState({ showDate: true })} style={{ flex: 1, flexDirection: 'row', left: 10, }}>
                        <View style={[styles.inputWraps, { flexDirection: 'row', marginVertical: 10 }]}>
                            <>
                                <Text style={[styles.TextInput, { borderWidth: 0, }]}>{this.state.dateTime ? this.state.dateTime : <Text style={{ color: Colors.textColor }}>Date of Birth</Text>}</Text>
                                <Text style={{ textAlign: 'center', alignSelf: 'center', fontFamily: 'Poppins-Bold' }}><AntDesign color={Colors.textColor} size={22} name="calendar" /></Text>
                            </>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    handleQualification = () => {
        return (
            <View style={{ margin: 20 }}>
                <Picker
                    selectedValue={this.state.selectEduId}
                    mode="dropdown"
                    style={{
                        height: 50,
                        padding: 20,
                        color: Colors.lightWhiteColor,

                    }}
                    placeholderTextColor={Colors.placeholderColor}
                    onValueChange={itemValue => {
                        setTimeout(() => {
                            this.updatePicker(itemValue, 'educational');
                        }, 0);
                    }}>
                    <Picker.Item
                        color={Colors.lightWhiteColor}
                        label={'Educational Qualification'}
                        value={0}
                    />
                    {this.renderItem('educational')}

                </Picker>
                <View style={{ borderColor: Colors.White, borderWidth: 0.5 }}></View>
            </View >
        )
    }
    handleExperience = () => {
        return (
            <View style={{ margin: 20 }}>
                <Picker
                    selectedValue={this.state.selectExpe}
                    mode="dropdown"
                    style={{
                        height: 50,
                        padding: 20,
                        color: Colors.lightWhiteColor,
                        // width: Dims.DeviceWidth - 40,

                    }}
                    placeholderTextColor={Colors.placeholderColor}
                    onValueChange={itemValue => {
                        setTimeout(() => {
                            this.updatePicker(itemValue, 'experience');
                        }, 0);
                    }}>
                    <Picker.Item
                        // color={Colors.white}
                        label={'Experience'}
                        value={0}
                    />
                    {this.renderItem('experience')}
                </Picker>
                <View style={{ borderColor: Colors.White, borderWidth: 0.5 }}></View>
            </View >
        )
    }
    handleMobileVerify = () => {
        let { isVerified } = this.state
        return (
            <View style={[styles.inputWraps, { marginHorizontal: 20, borderBottomColor: Colors.White, borderBottomWidth: 1, flexDirection: 'row' }]}>
                <View style={{ flex: 1 }}>
                    <FloatingLabel
                        labelStyle={[styles.labelStyle]}
                        inputStyle={styles.inputStyle}
                        onChange={event => this.handleChange(event, 'mobileNo')}
                        style={styles.floatingLabelView}
                        value={this.state.mobileNo}
                    >Mobile No.</FloatingLabel>
                </View>
                <TouchableOpacity onPress={() => this.onHandleVerify()} style={{ flex: 0.3, justifyContent: 'center', alignSelf: 'center', }}>
                    {!isVerified ?
                        <Text style={{ fontSize: 13, fontFamily: 'Poppins-Medium', textAlign: 'center', color: '#f6100b', top: 5, letterSpacing: 1 }}><Text>
                            <AntDesign
                                name="infocirlceo"
                                size={14}
                                color={'#f6100b'}
                                style={{}}
                            /></Text>{' '}Verify</Text> :
                        <Text style={{ fontSize: 13, fontFamily: 'Poppins-Medium', textAlign: 'center', color: Colors.White, top: 5, letterSpacing: 1 }}><Text>
                            <FontAwesome
                                name="check-circle-o"
                                size={10}
                                color={Colors.White}
                                style={{}}
                            /></Text>{' '}Verified</Text>
                    }
                </TouchableOpacity>



            </View>
        )
    }
    onHandleAddress = (manualAddress) => {
        this.setState({ city: manualAddress, predictions: [], isEnable: true }, () => {
            var url =
                'https://maps.googleapis.com/maps/api/geocode/json?address=' + manualAddress + '&key=' +
                googleApiKey;
            fetch(url)
                .then(response => response.json())
                .then(responseJson => {
                    console.log(responseJson, 'responseJson');
                    if (responseJson && responseJson.results[0] && responseJson.results[0].geometry) {
                        this.setState({
                            region: {
                                latitude: responseJson.results[0].geometry.location.lat,
                                longitude: responseJson.results[0].geometry.location.lng,
                                latitudeDelta: LATITUDE_DELTA,
                                longitudeDelta: LONGITUDE_DELTA,
                                mapData: responseJson.results[0].geometry.location,
                                isEnable: true
                            },
                            isManualAddress: true
                        })
                    }
                })
        })
    }
    onChangeDestination = async (event) => {
        let destination = event.nativeEvent.text
        this.setState({ destination })
        const placeApiUrl = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?key=' + googleApiKey + '&input=' + destination + '&location=' + this.state.latitude + ',' + this.state.longitude + '&radius=2000';
        try {
            const result = await fetch(placeApiUrl);
            const json = await result.json();
            console.log(json, 'json');
            this.setState({
                predictions: json.predictions
            })
        } catch (err) {
            console.error(err)
        }
    }
    _renderItem = (item) => {
        return (
            <View style={styles.autoCompleteView}>
                <TouchableOpacity onPress={() => this.onHandleAddress(item.description)}>
                    <Text numberOfLines={4} style={{ fontSize: 15, padding: 5, fontFamily: 'Poppins-Medium' }}>
                        {item.description}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
    handleCity = () => {
        return (
            <>
                <View style={[styles.inputWraps, { marginHorizontal: 20, borderBottomColor: Colors.White, borderBottomWidth: 1, flexDirection: 'row', justifyContent: 'space-between' }]}>
                    <View style={{ flex: 1 }}>
                        <FloatingLabel
                            labelStyle={[styles.labelStyle]}
                            inputStyle={styles.inputStyle}
                            onChange={event => this.onChangeDestination(event)}
                            style={styles.floatingLabelView}
                            value={this.state.city}
                        >City</FloatingLabel>
                    </View>
                    <View style={{ flex: 0.1, alignSelf: 'center', }}>
                        <Text style={{ fontSize: 13, fontFamily: 'Poppins-Medium', textAlign: 'center', color: '#f6100b', top: 5, letterSpacing: 1 }}><Text>
                            <AntDesign
                                name="right"
                                size={14}
                                color={Colors.White}
                                style={{}}
                            /></Text></Text>
                    </View>

                </View>
                <ScrollView nestedScrollEnabled={true}>
                    <View>
                        {this.state.predictions && this.state.predictions.length > 0 && (
                            <FlatList
                                style={{ marginTop: 10 }}
                                showsVerticalScrollIndicator={true}
                                keyExtractor={(item, index) => item.id}
                                renderItem={({ item }) => this._renderItem(item)}
                                data={this.state.predictions}
                                extraData={this.state}
                            />
                        )}
                    </View>
                </ScrollView>
            </>
        )
    }
    handleGenderInfo = () => {
        return (
            <View style={{ margin: 20 }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontSize: 12, alignSelf: 'center', fontFamily: 'Poppins-Bold', color: Colors.White, }}>Gender</Text>

                    <RadioButton
                        value="first"
                        tintColor={Colors.White}
                        color={Colors.White}
                        uncheckedColor={"#F0F0F0"}

                        status={this.state.checkedMale ? 'checked' : 'unchecked'}
                        onPress={(checkedMale) => this.setState({ checkedMale: !this.state.checkedMale, checkedFemale: false, checkedPrefer: false })}
                    />
                    <Text style={{ fontSize: 10, alignSelf: 'center', fontFamily: 'Poppins-Medium', color: Colors.White }}>Male</Text>
                    <RadioButton
                        value="second"
                        color={Colors.White}
                        uncheckedColor={"#F0F0F0"}
                        tintColor={Colors.White}
                        status={this.state.checkedFemale ? 'checked' : 'unchecked'}
                        onPress={(checkedFemale) => this.setState({ checkedFemale: !this.state.checkedFemale, checkedMale: false, checkedPrefer: false })}
                    /><Text style={{ fontSize: 10, alignSelf: 'center', fontFamily: 'Poppins-Medium', color: Colors.White }}>Female</Text>
                    <RadioButton
                        value="second"
                        color={Colors.White}
                        uncheckedColor={"#F0F0F0"}
                        tintColor={Colors.White}
                        status={this.state.checkedPrefer ? 'checked' : 'unchecked'}
                        onPress={(checkedPrefer) => this.setState({ checkedPrefer: !this.state.checkedPrefer, checkedMale: false, checkedFemale: false })}
                    /><Text style={{ fontSize: 10, alignSelf: 'center', fontFamily: 'Poppins-Medium', color: Colors.White }}>Prefer not to say</Text>
                </View>
            </View>
        )
    }
    handlePermanentInfo = () => {
        let { addressCity, addressCountry, addressPincode, addressState, addressInfo } = this.state
        return (
            <View style={{ marginVertical: 15 }}>
                <View style={{ flexDirection: 'row' }}>
                    <RadioButton
                        value="first"
                        tintColor={Colors.Primary}
                        disabled={addressCity && addressCountry && addressPincode && addressState && addressInfo ? false : true}
                        status={this.state.checkedPrefer ? 'checked' : 'unchecked'}
                        onPress={(checkedPrefer) => this.setState({ checkedPrefer: !this.state.checkedPrefer })}
                    />
                    <Text style={{ alignSelf: 'center', fontFamily: 'Poppins-Medium' }}>Same as address for communication</Text>

                </View>
            </View>
        )
    }
    handleEmailVerify = () => {
        return (
            <View style={[styles.inputWraps, { margin: 20, borderBottomColor: Colors.White, borderBottomWidth: 1, }]}>
                <View style={{ flex: 1 }}>
                    <FloatingLabel
                        labelStyle={[styles.labelStyle]}
                        inputStyle={styles.inputStyle}
                        onChange={event => this.handleChange(event, 'mailId')}
                        style={styles.floatingLabelView}
                        value={this.state.mailId}
                    >
                        Mail Id
                    </FloatingLabel>
                </View>
            </View>
        )
    }
    handleAddressInformation = () => {
        return (
            <Card style={{ borderRadius: 10, marginVertical: 10 }}>
                <Text style={{ fontFamily: 'Poppins-Medium', paddingHorizontal: 15, top: 10 }}>Address for Communication</Text>
                <TextInput style={{ textAlignVertical: 'top', fontFamily: 'Poppins-Medium', marginLeft: 10 }}
                    // editable={this.state.masterProduct ?false:true} 
                    multiline={true}
                    numberOfLines={4}
                    onChangeText={(addressInfo) => this.setState({ addressInfo })}
                    value={this.state.addressInfo} />
            </Card>
        )
    }
    handlePincode = () => {
        return (
            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between' }}>
                <Card style={{ borderRadius: 10, marginVertical: 10, flex: 0.4 }}>
                    <TextInput style={{ textAlignVertical: 'top', fontFamily: 'Poppins-Medium', marginLeft: 10 }}
                        placeholder="PinCode"
                        keyboardType='number-pad'
                        onChangeText={(addressPincode) => this.setState({ addressPincode })}
                        value={this.state.addressPincode} />
                </Card>
                <Card style={{ borderRadius: 10, marginVertical: 10, flex: 0.5 }}>
                    <TextInput style={{ textAlignVertical: 'top', fontFamily: 'Poppins-Medium', marginLeft: 10 }}
                        placeholder="City"
                        onChangeText={(addressCity) => this.setState({ addressCity })}
                        value={this.state.addressCity} />
                </Card>
            </View>
        )
    }
    handleState = () => {
        return (
            <View style={{ flexDirection: 'row' }}>
                <Card style={{ borderRadius: 10, marginVertical: 10 }}>
                    <TextInput style={{ textAlignVertical: 'top', fontFamily: 'Poppins-Medium', marginLeft: 10 }}
                        placeholder="State"
                        onChangeText={(addressState) => this.setState({ addressState })}
                        value={this.state.addressState} />
                </Card>

            </View>
        )
    }
    handleCountry = () => {
        return (
            <View style={{ flexDirection: 'row' }}>
                <Card style={{ borderRadius: 10, marginVertical: 10 }}>
                    <TextInput style={{ textAlignVertical: 'top', fontFamily: 'Poppins-Medium', marginLeft: 10 }}
                        placeholder="Country"
                        onChangeText={(addressCountry) => this.setState({ addressCountry })}
                        value={this.state.addressCountry} />
                </Card>

            </View>
        )
    }
    handlePermanentAddressInformation = () => {
        let { addressInfo, checkedPrefer } = this.state;
        console.log(checkedPrefer, 'lllllllllllllll');
        return (
            <Card style={{ borderRadius: 10, marginVertical: 10 }}>
                <Text style={{ fontFamily: 'Poppins-Medium', paddingHorizontal: 15, top: 10 }}>PermanentAddress</Text>
                <TextInput style={{ textAlignVertical: 'top', fontFamily: 'Poppins-Medium', marginLeft: 10 }}
                    // editable={this.state.masterProduct ?false:true} 
                    multiline={true}
                    numberOfLines={4}
                    onChangeText={(permanentAddressInfo) => this.setState({ permanentAddressInfo })}
                    value={checkedPrefer ? addressInfo : this.state.permanentAddressInfo} />
            </Card>
        )
    }
    handlePermanentPincode = () => {
        let { checkedPrefer, addressPincode, addressCity } = this.state;
        return (
            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between' }}>
                <Card style={{ borderRadius: 10, marginVertical: 10, flex: 0.4 }}>
                    <TextInput style={{ textAlignVertical: 'top', fontFamily: 'Poppins-Medium', marginLeft: 10 }}
                        keyboardType='number-pad'
                        placeholder="PinCode"
                        onChangeText={(permanentAddressPincode) => this.setState({ permanentAddressPincode })}
                        value={checkedPrefer ? addressPincode : this.state.permanentAddressPincode} />
                </Card>
                <Card style={{ borderRadius: 10, marginVertical: 10, flex: 0.5 }}>
                    <TextInput style={{ textAlignVertical: 'top', fontFamily: 'Poppins-Medium', marginLeft: 10 }}
                        placeholder="City"
                        onChangeText={(permanentAddressCity) => this.setState({ permanentAddressCity })}
                        value={checkedPrefer ? addressCity : this.state.permanentAddressCity} />
                </Card>
            </View>
        )
    }
    handlePermanentState = () => {
        let { checkedPrefer, addressState, addressCity } = this.state;

        return (
            <View style={{ flexDirection: 'row' }}>
                <Card style={{ borderRadius: 10, marginVertical: 10 }}>
                    <TextInput style={{ textAlignVertical: 'top', fontFamily: 'Poppins-Medium', marginLeft: 10 }}
                        placeholder="State"
                        onChangeText={(permanentAddressState) => this.setState({ permanentAddressState })}
                        value={checkedPrefer ? addressState : this.state.permanentAddressState} />
                </Card>

            </View>
        )
    }
    handlePermanentCountry = () => {
        return (
            <View style={{ flexDirection: 'row' }}>
                <Card style={{ borderRadius: 10, marginVertical: 10 }}>
                    <TextInput style={{ textAlignVertical: 'top', fontFamily: 'Poppins-Medium', marginLeft: 10 }}
                        placeholder="Country"
                        onChangeText={(permanentAddressCountry) => this.setState({ permanentAddressCountry })}
                        value={this.state.permanentAddressCountry} />
                </Card>

            </View>
        )
    }
    returnText = item => {
        const emoji = nodeEmoji.get(item.flag);
        return (
            <View style={{ flexDirection: 'row', }}>
                <Text style={{ fontFamily: 'Poppins-Medium', top: 5 }}>+ {item.callingCode}</Text>
                <Text style={{ left: 10, top: 5 }}>
                    <Entypo
                        name="triangle-down"
                        size={20}
                        color={Colors.darkColor}
                    />
                </Text>
            </View>
        )
    };
    onValueChange = selectedValue => {
        this.setState({ value: selectedValue });
    };
    renderOption(settings) {
        const { item, getLabel } = settings;
        const emoji = nodeEmoji.get(item.flag);
        return (
            <View style={{}}>
                <View style={styles.innerContainer}>
                    <Text
                        style={{ fontSize: 20, paddingLeft: 20 }}
                        allowFontScaling={false}>
                        {emoji}
                    </Text>
                    <Text
                        style={{
                            fontSize: 18,
                            alignSelf: 'flex-start',
                            paddingLeft: 20
                        }}>
                        +{item.callingCode}
                    </Text>
                    <Text style={{ fontSize: 18, alignSelf: 'flex-start' }}>
                        {item.name}
                    </Text>
                </View>
            </View>
        );
    }
    renderHeader = () => {
        return (
            <View style={styles.headerFooterContainer}>
                {/* <Text style={{ fontSize: 18, }}>Select your Country</Text> */}
            </View>
        );
    };
    relocatingAddress = () => {
        let { firstName, lastName, dateTime, mobileNo, mailId, city, selectEduId, selectExpe, selectGenderId, isVerified } = this.state
        if (firstName) {
            if (lastName) {
                if (dateTime) {
                    if (mobileNo) {
                        if (!isVerified) {
                            if (mailId) {
                                if (city) {
                                    if (selectEduId) {
                                        if (selectExpe) {
                                            let obj = {
                                                "title": selectGenderId,
                                                "birthday": dateTime,
                                                "service": "nursing care",
                                                "educationQualification": selectEduId,
                                                "email": mailId,
                                                "experienceInYears": selectExpe,
                                                "firstName": firstName,
                                                "gender": "Male",
                                                "lastName": lastName,
                                                "password": "123",
                                                "permanentAddressSame": false,
                                                "phone": parseInt(mobileNo),
                                                "registerAs": "provider",
                                                "comAddress": "str464ing. 574sg, sddd",
                                                "comAddressType": "communication",
                                                "comCity": "vsp",
                                                "comCountry": "ind",
                                                "comPincode": 530007,
                                                "comState": "state",
                                                "perAddress": "4546, gsgd",
                                                "perAddressType": "permanent",
                                                "perCity": "vis",
                                                "perCountry": "ind",
                                                "perPincode": 5322225,
                                                "perState": "staet"
                                            }
                                            var registerBody = JSON.stringify(obj)
                                            let regsiterData = HttpHelper(RegisterUrl, 'POST', registerBody);
                                            regsiterData.then(RegisterResponse => {
                                                console.log(RegisterResponse, 'RegisterResponseRegisterResponse');
                                                if (RegisterResponse && RegisterResponse.success === true) {
                                                    this.setState({ isRegisterPressed: false }, () => {
                                                        Alert.alert(
                                                            '',
                                                            "Registeration SuccessFully",
                                                            [
                                                                {
                                                                    text: 'OK', onPress: () => {
                                                                        this.props.navigation.navigate('Login',)
                                                                    },

                                                                },
                                                            ],
                                                            { cancelable: false }
                                                        );
                                                    })
                                                } else {
                                                    this.setState({ isRegisterPressed: false }, () => {
                                                        this.dropdown.alertWithType('error', 'Error!', RegisterResponse.data)
                                                    })
                                                }
                                            })

                                        } else {
                                            this.dropdown.alertWithType('error', 'Error!', 'Please select your Experience')

                                        }

                                    } else {
                                        this.dropdown.alertWithType('error', 'Error!', 'Please select your Eductaional Qualification')

                                    }
                                } else {
                                    this.dropdown.alertWithType('error', 'Error!', 'Please enter your City Address')

                                }


                            } else {
                                this.dropdown.alertWithType('error', 'Error!', 'Please enter your Email')

                            }
                        } else {
                            this.dropdown.alertWithType('error', 'Error!', 'Please verify your mobile number')
                        }
                    } else {
                        this.dropdown.alertWithType('error', 'Error!', 'Please enter your Mobile Number')

                    }

                } else {
                    this.dropdown.alertWithType('error', 'Error!', 'Please choose your DoB')

                }
            } else {
                this.dropdown.alertWithType('error', 'Error!', 'Please enter your last name')

            }
        } else {
            this.dropdown.alertWithType('error', 'Error!', 'Please enter your first name')

        }
    }
    closeCancelModal = () => {
        this.setState({
            isModalVisible: false
        })
    }
    renderField(settings) {
        const { selectedItem, defaultText, getLabel, clear } = settings;
        return (
            <View style={styles.container}>
                <View>
                    {!selectedItem && <Text style={[{ fontSize: 18 }]}>{defaultText}</Text>}
                    {selectedItem && (
                        <View style={styles.innerContainer}>
                            <Text style={[{ fontSize: 15, color: Colors.Text, textAlign: 'center', alignSelf: 'center' }]}>
                                {getLabel(selectedItem)}
                            </Text>
                        </View>
                    )}
                </View>
            </View>
        );
    }
    renderLoginPanel() {
        return (
            <>
                <View style={[styles.container, {}]}>
                    <View
                        style={[styles.textInputView, { height: 50, flexDirection: 'row', width: 80 }]}>
                        <CustomPicker
                            modalStyle={styles.countryModalStyle}
                            placeholder={'Select your Country'}
                            style={[styles.defaultInput]}
                            value={this.state.value}
                            options={this.state.options}
                            getLabel={item => this.returnText(item)}
                            headerTemplate={this.renderHeader}
                            fieldTemplate={this.renderField}
                            optionTemplate={this.renderOption}
                            onValueChange={selectedCode => {
                                this.onValueChange(selectedCode);
                            }}
                        />
                    </View>
                </View>
            </>
        );
    }
    handleNextStep = () => {
        return (
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('Login') }} style={{ backgroundColor: Colors.mainBackground, width: Dims.DeviceWidth / 1.5, alignSelf: 'center', borderRadius: 10, marginVertical: 50 }}>
                <Card style={{ backgroundColor: Colors.mainBackground, width: Dims.DeviceWidth / 1.5, alignSelf: 'center', alignItems: 'center', justifyContent: 'center', height: 50, flexDirection: 'row' }}>
                    <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 16, color: Colors.White, textAlign: 'center' }}>Next Step </Text>
                    <Text style={{ alignItems: 'flex-end', left: 40 }}><MaterialIcons
                        name="arrow-right-alt"
                        size={25}
                        color={Colors.White}
                    // style={{ marginLeft: -15 }}
                    />  </Text>
                </Card>
            </TouchableOpacity>
        )
    }
    onHandleVerify = () => {
        let { mobileNo, otpInput } = this.state;
        if (mobileNo) {
            console.log(mobileNo);
            let otpData = HttpHelper(`${RegisterMobileUrl}+91${mobileNo}`, 'POST', '');
            otpData.then(response => {
                this.setState({ isModalVisible: true, otp: response.data })
            })

        } else {
            this.dropdown.alertWithType('error', 'Error!', 'Please enter your OTP.')
        }
    }
    onHandleCategory = () => {
        //    let {getCategoryData} = this.state;
        let getCategoryDatas = HttpHelper(`${CategoryUrl}`, 'GET', '');
        getCategoryDatas.then(response => {
            console.log(response, 'response');
            if (response) {
                this.setState({ getCategoryData: response })
            } else {
                this.setState({ getCategoryData: [] })
            }
        })


    }
    onHandleOtpVerify = () => {
        //    let {getCategoryData} = this.state;
        let { mobileNo, otpInput } = this.state;
        if (mobileNo && otpInput.length === 4) {
            console.log(`${RegisterMobileVerifyUrl}${otpInput}/+91${mobileNo}`);
            let otpData = HttpHelper(`${RegisterMobileVerifyUrl}${otpInput}/+91${mobileNo}`, 'GET', '');
            otpData.then(response => {
                console.log(response, 'llllll');
                if (response) {
                    this.setState({ isModalVisible: false, isVerified: true })

                } else {
                    this.dropdown.alertWithType('error', 'Error!', 'Please enter Valid OTP.')
                }
            })

        } else {
            this.dropdown.alertWithType('error', 'Error!', 'Please enter your OTP.')
        }


    }
    render() {
        let { getCategoryData, counter } = this.state;
        return (
            <>
                <StatusBar style={{ backgroundColor: Colors.Primary }} />
                <ScrollView>
                    <View style={{ backgroundColor: Colors.Primary }}>

                        <TouchableOpacity onPress={() => this.handleBackPress()}>
                            <Text style={{ fontFamily: 'Poppins-Bold', fontSize: 20, color: Colors.White, textAlign: 'left', padding: 20, letterSpacing: 2 }}>
                                <Fontisto
                                    name="arrow-left-l"
                                    size={20}
                                    color={Colors.White}
                                    style={{}}
                                />{'   '}SIGN UP</Text>
                        </TouchableOpacity>
                        <View style={{ padding: 20 }}>
                            {this.handleCategory()}
                        </View>
                        <View style={{}}>
                            <ScrollView style={this.state.keyboardState === "opened" ? styles.deliver1 : styles.deliver}>
                                <Text style={{ height: 10 }}></Text>

                                <View style={{ width: Dims.DeviceWidth - 40, alignSelf: 'center' }}>
                                    {this.handleFirstName()}
                                    {this.handleLastName()}
                                    {this.handleDob()}
                                    {this.handleGenderInfo()}
                                    {this.handleMobileVerify()}
                                    {this.handleEmailVerify()}
                                    {this.handleCity()}
                                    {this.handleQualification()}
                                    {this.handleExperience()}
                                </View>
                                <View style={styles.centeredView}>
                                    <Modal
                                        animationType="slide"
                                        transparent={true}
                                        visible={this.state.isModalVisible}
                                        style={{}}
                                        onRequestClose={() => {
                                            this.setState({ isModalVisible: !this.state.isModalVisible })
                                        }}>
                                        <View style={styles.centeredView}>
                                            <View style={styles.modalView}>
                                                <TouchableOpacity onPress={() => { this.closeCancelModal() }} style={{ alignItems: 'flex-end', top: -25, left: 30 }}>
                                                    <View>
                                                        <AntDesign color={Colors.mainBackground} size={22} name="closecircle" />
                                                    </View>
                                                </TouchableOpacity>
                                                <View style={{ height: Dims.DeviceHeight / 2.5, width: Dims.DeviceWidth - 40, borderTopLeftRadius: 60, borderTopRightRadius: 60, alignSelf: 'center' }}>
                                                    <TouchableOpacity onPress={() => { this.setState({ isModalVisible: false }) }}>
                                                        <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.Primary, textAlign: 'center', margin: 10 }}>Enter the <Text style={{ fontFamily: 'Poppins-Bold' }}>One Time Password</Text>{'\n'}sent to {this.state.mobileNo}  <Entypo
                                                            name="edit"
                                                            size={15}
                                                            color={Colors.Primary}
                                                        /></Text>
                                                    </TouchableOpacity>
                                                    <OTPTextView
                                                        handleTextChange={(text) => this.setState({ otpInput: text })}
                                                        containerStyle={styles.textInputContainer}
                                                        textInputStyle={styles.roundedTextInput}
                                                        inputCount={4}
                                                        inputCellLength={1}
                                                        tintColor={Colors.Primary}
                                                        offTintColor={Colors.lightWhiteColor}
                                                    />
                                                    <TouchableOpacity onPress={() => { this.onHandleOtpVerify() }} style={{ flexDirection: 'row', justifyContent: 'center', marginTop: Dims.DeviceHeight * 0.03 }}>
                                                        <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 16, color: Colors.Primary, width: 150 }}></Text>
                                                        <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 14, color: Colors.Primary, }}>Verify</Text>
                                                        <Text style={{ alignSelf: 'center', width: 30, height: 30, borderRadius: 50, backgroundColor: Colors.Primary, textAlign: 'center', left: 20, top: -5 }}>
                                                            <MaterialIcons
                                                                name="arrow-right-alt"
                                                                size={28}
                                                                color={Colors.White}
                                                            // style={{top: -15 }}
                                                            /></Text>
                                                    </TouchableOpacity>
                                                    {counter <= 10 ? (
                                                        <Text style={{ textAlign: 'center', fontFamily: 'Poppins-Bold', color: Colors.Primary }}>Resend OTP in{'  '} <Text style={{ color: Colors.redColor }}>{this.state.counter}s</Text></Text>) : (
                                                        <Text>''</Text>
                                                    )}

                                                    {counter >= 10 ? (
                                                        <TouchableOpacity>

                                                            <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.Primary, margin: 10, left: 30, top: Dims.DeviceHeight * 0.04, fontSize: 12 }}>Didn't receive OTP? <Text style={{ fontFamily: 'Poppins-Bold', textDecorationLine: 'underline' }}>Click to resend</Text></Text>


                                                        </TouchableOpacity>
                                                    ) :
                                                        <Text></Text>}
                                                </View>
                                            </View>
                                        </View>
                                    </Modal>
                                </View>
                                <View style={styles.btn}>
                                    <TouchableOpacity onPress={() => this.relocatingAddress()} style={styles.loginBtn}>
                                        <Text style={styles.loginText}>Register</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{}}>
                                    <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.White, fontSize: 9, textAlign: 'center', }}><Text style={{ fontFamily: 'Poppins-Bold', textDecorationLine: 'underline' }}>Our Terms & Conditions</Text>  </Text>

                                </View>
                            </ScrollView>
                        </View>
                    </View>
                    {this.state.showDate &&
                        <DateTimePicker
                            testID="dateTimePicker"
                            value={date}
                            mode={'date'}
                            format="HH:mm a"
                            placeholder="Choose Date"
                            is24Hour={false}
                            locale={"SV"}
                            onChange={(event, value) => {
                                let newDate = moment(String(value)).format("YYYY-MM-DD");
                                this.setState({
                                    dateTime: newDate,
                                    show: false,
                                    showDate: false,
                                    showMode: false,
                                })
                            }}
                        />
                    }
                </ScrollView>
                <DropdownAlert
                    ref={(ref) => (this.dropdown = ref)}
                    containerStyle={{ backgroundColor: '#FF0000' }}
                    imageSrc={'https://facebook.github.io/react/img/logo_og.png'}
                />
            </>
        );
    }
}

const styles = StyleSheet.create({
    dotView: {
        flexDirection: 'row',
        alignSelf: 'flex-end'
    },
    logo: {
        height: 100,
        width: 200
    },
    nurseLogo: {
        height: Dims.DeviceHeight / 2.5,
        width: Dims.DeviceWidth / 2
    },
    modalView: {
        marginHorizontal: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        borderWidth: 1, borderColor: '#118FA0',
        // alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    inputWraps: {
        flexDirection: 'row',
        flex: 1,
        // paddingHorizontal: 10
    },
    TextInput: {
        flex: 1,
        // padding: 10,
        fontSize: 12,
        fontFamily: 'Poppins-Medium',
        borderColor: Colors.placeholderColor,
        // borderWidth: 0.2
        color: Colors.White
    },
    welcomeText: {
        fontFamily: 'Poppins-Medium',
        fontSize: 12, color: Colors.Primary,
        letterSpacing: 2,
        top: 10,
        marginLeft: '7%',
        marginBottom: 10
    },
    defaultInput: {
        color: Colors.Black,
        fontSize: 12,
        top: 10,
        justifyContent: 'center',

    },
    textInputView: {
        paddingLeft: 15,
    },
    container: {
        flexDirection: 'row',
    },
    innerContainer: {
        flexDirection: 'row',
        alignItems: 'stretch',

    },
    hidePasswordText: {
        fontSize: 16,
        textAlign: 'center', fontFamily: 'Poppins-Medium',
        flex: 0.9
    },
    headerFooterContainer: {
        alignItems: 'center',
    },
    inputText: {
        alignSelf: 'center',
        width: Dims.DeviceWidth * 0.5,
        height: 50,
        borderColor: Colors.lightGreyColor,
        borderWidth: 1,
        fontSize: 12,
        textAlign: 'center',
        backgroundColor: '#fcfcfc',
        fontFamily: 'Poppins-Medium',
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30
    },
    deliver: {
        position: 'relative',
        bottom: 0,
        backgroundColor: Colors.Primary,
        // borderTopLeftRadius: 60,
        // borderTopRightRadius: 60,
        // height: Dims.DeviceHeight - 100,s
        width: Dims.DeviceWidth,
        // top:-30
        // marginTop: -40
    },
    deliver1: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        // top: 30,
        backgroundColor: Colors.Primary,
        // borderTopLeftRadius: 60,
        // borderTopRightRadius: 60,
        // height: Dims.DeviceHeight / 0.2,
        width: Dims.DeviceWidth,
        // marginBottom: 100
    },
    location: {
        position: 'relative',
        bottom: 0,
        backgroundColor: Colors.redColor,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        width: Dims.DeviceWidth,
    },
    location1: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        top: 30,
        backgroundColor: Colors.Primary,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        height: 100,
        width: Dims.DeviceWidth,
        marginBottom: 100
    },
    loginBtn: {
        width: Dims.DeviceWidth - 50,
        paddingVertical: 10,
        borderRadius: 2,
        backgroundColor: Colors.white
    },
    confirmButton: {
        width: Dims.DeviceWidth - 50,
        paddingVertical: 10,
        borderRadius: 2,
        backgroundColor: Colors.Primary
    },
    container2: {
        alignSelf: 'center'
    },
    gradient: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 7,
    },
    btn: {
        height: 50,
        alignSelf: 'center',
        margin: 20
    },
    loginText: {
        color: Colors.Primary,
        alignSelf: 'center',
        // justifyContent: 'center',
        fontFamily: 'Poppins-Medium',
        letterSpacing: 3,
        textAlign: 'center',
    },
    closePanel: {
        flexWrap: 'wrap',
        top: 20

    },
    textInputContainer: {
        marginHorizontal: 50,
    },
    roundedTextInput: {
        height: 70,
        fontFamily: 'Poppins-Medium',
        color: Colors.Primary
    },
    labelStyle: {
        color: Colors.textColor,
        // textAlignVertical: 'center',
        fontSize: 14,
        fontFamily: 'Poppins-Medium',
        // borderBottomColor: Colors.White,
        letterSpacing: 1

    },
    floatingLabelView: {
        // marginHorizontal: 20,
    },

    hidePasswordText: {
        fontSize: 16,
        textAlign: 'center', fontFamily: 'Poppins-Medium',
        textAlignVertical: 'center',
        // color:Colors.placeholderColor
        fontSize: 16,

    },
    inputStyle: {
        borderWidth: 0,
        // margin: 10,
        height: 45,
        fontFamily: 'Poppins-Medium',
        fontSize: 12,
        // borderBottomColor: Colors.white,
        // borderBottomWidth: 1,
        color: Colors.White

    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        // alignItems: "center",
        // marginTop: 22,
        // marginHorizontal: 10,
    },
    autoCompleteView: {
        marginTop: 10,
        alignSelf: 'center',
        width: Dims.DeviceWidth * 0.8,
        // height: 25,
        paddingHorizontal: 20,
        fontSize: 16,
        textAlign: 'left',
        backgroundColor: '#fcfcfc',
        fontFamily: 'Poppins-Medium',
        backgroundColor: Colors.lightGreyColor,
        borderRadius: 10
    },
});

export default Register;
