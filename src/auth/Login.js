import React, { Component } from 'react';
import {
  Text,
  View,
  StatusBar,
  Image, StyleSheet, BackHandler, TouchableOpacity, TextInput, Alert
} from 'react-native';
import { Colors } from '../assets/Colors';
import { Dims } from '../components/Dims';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as RNLocalize from 'react-native-localize';
import { CustomPicker } from 'react-native-custom-picker';
import nodeEmoji from 'node-emoji';
import { getAllCountries } from 'react-native-country-picker-modal';
import { ScrollView } from 'react-native-gesture-handler';
import { HttpHelper } from '../HelperApi/Api/HTTPHelper';
import { LoginUrl, RegisterOtpUrl } from '../HelperApi/Api/APIConfig';
class Login extends Component {
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;

    this.state = {
      options: [],
      mobileNo: ''
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    const deviceCountry = RNLocalize.getCountry();
    getAllCountries().then(cl => {
      this.setState({ options: cl });
      var userCountryData = cl
        .filter(country => country.name === "India")
        .pop();
      this.setState({ value: userCountryData }, () => {
      });
      // this.props.onPickerLoad(userCountryData);
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    Alert.alert(
      '',
      'Are you sure you want to exit the application ?', [{
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => BackHandler.exitApp()
      },], {
      cancelable: false
    }
    )
    return true;
  };
  returnText = item => {
    const emoji = nodeEmoji.get(item.flag);
    return (
      <View style={{ flexDirection: 'row', }}>
        <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.Primary, top: 5 }}>+ {item.callingCode}</Text>
        <Text style={{ left: 10, top: 5 }}>
          <Entypo
            name="triangle-down"
            size={20}
            color={Colors.Primary}
          />
        </Text>
      </View>
    )
  };
  onValueChange = selectedValue => {
    this.setState({ value: selectedValue });
  };
  renderOption(settings) {
    const { item, getLabel } = settings;
    const emoji = nodeEmoji.get(item.flag);
    return (
      <View style={{ padding: 5 }}>
        <View style={styles.innerContainer}>
          <Text
            style={{ fontSize: 20, paddingLeft: 20 }}
            allowFontScaling={false}>
            {emoji}
          </Text>
          <Text
            style={{
              fontSize: 18,
              alignSelf: 'flex-start',
              paddingLeft: 20,
              color: Colors.Primary, width: 100
            }}>
            +{item.callingCode}{' '}
          </Text>
          <Text style={{ fontSize: 18, alignSelf: 'flex-start', fontFamily: 'Poppins-Bold', width: 200 }}>
            {item.name}
          </Text>
        </View>
      </View>
    );
  }
  renderHeader = () => {
    return (
      <View style={styles.headerFooterContainer}>
        <Text style={{ fontSize: 18, padding: 5, fontFamily: 'Poppins-Medium', color: Colors.Primary }}>Select your Country</Text>
      </View>
    );
  };
  renderField(settings) {
    const { selectedItem, defaultText, getLabel, clear } = settings;
    return (
      <View style={styles.container}>
        <View>
          {!selectedItem && <Text style={[{ fontSize: 18 }]}>{defaultText}</Text>}
          {selectedItem && (
            <View style={styles.innerContainer}>
              <Text style={[{ fontSize: 15, color: Colors.Text, textAlign: 'center', alignSelf: 'center' }]}>
                {getLabel(selectedItem)}
              </Text>
            </View>
          )}
        </View>
      </View>
    );
  }
  renderLoginPanel() {
    return (
      <>
        <View style={[styles.container, { width: 80, borderWidth: 1, borderColor: Colors.lightGreyColor, borderTopLeftRadius: 30, borderBottomLeftRadius: 30 }]}>
          <View
            style={[styles.textInputView, { height: 50, flexDirection: 'row', }]}>
            <CustomPicker
              modalStyle={styles.countryModalStyle}
              placeholder={'Select your Country'}
              style={[styles.defaultInput]}
              value={this.state.value}
              options={this.state.options}
              getLabel={item => this.returnText(item)}
              headerTemplate={this.renderHeader}
              fieldTemplate={this.renderField}
              optionTemplate={this.renderOption}
              onValueChange={selectedCode => {
                this.onValueChange(selectedCode);
              }}
            />
          </View>
        </View>
      </>
    );
  }
  _onLoginHandle = () => {
    let { mobileNo, value } = this.state;
    if (mobileNo && value) {
      console.log(LoginUrl + '+' + value.callingCode[0] + String(mobileNo),'LoginUrl + '+' + value.callingCode[0] + String(mobileNo)');
      let loginData = HttpHelper(LoginUrl + '+' + value.callingCode[0] + String(mobileNo), 'POST', '');
      loginData.then(loginData => {
        console.log(loginData,'loginData');

        if (loginData && loginData.success === true) {
          this.props.navigation.navigate('LoginVerify', { mobileNo: mobileNo, value: value });
        } else {
          this.dropdown.alertWithType('error', 'Error!', 'Please enter valid mobile number')
        }
      })
    } else {
      this.dropdown.alertWithType('error', 'Error!', 'Please enter mobile number')
    }
  }

  onNavigateHandle = () => {
    this.props.navigation.navigate('Register')

  }
  render() {
    return (
      <>
        <StatusBar style={{ backgroundColor: Colors.Primary }} />
        <ScrollView style={{ backgroundColor: '#FFFFFFF' }}>
          <View style={styles.dotView}>
            <Entypo
              name="dot-single"
              size={40}
              color={Colors.Primary}
            />
            <Entypo
              name="dot-single"
              size={40}
              color={Colors.placeholderColor}
              style={{ marginLeft: -15 }}
            />
            <Entypo
              name="dot-single"
              size={40}
              color={Colors.placeholderColor}
              style={{ marginLeft: -15 }}
            />
          </View>
          <View style={{ height: Dims.DeviceHeight - 120 }}>
            <View>
              <View style={{ marginHorizontal: 20 }}>
                <Text style={styles.welcomeText}>Hi, Welcome back</Text>
                <Image
                  source={require('../assets/images/logo.png')}
                  style={styles.logo}
                  resizeMode="contain"
                />
              </View>
              <View style={{ margin: '10%' }}>
                <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 12, color: Colors.placeholderColor, textAlign: 'center' }}>Enter your registered phone number,{'\n'}we will send <Text style={{ fontFamily: 'Poppins-Bold' }}>One Time Password</Text></Text>
                <View style={[styles.container, { marginVertical: 25, marginHorizontal: 13 }]}>
                  {this.renderLoginPanel()}
                  <View style={[styles.inputText, { flexDirection: 'row' }]}>
                    <TextInput
                      style={styles.hidePasswordText}
                      placeholder={'Phone Number'}
                      autoCapitalize="none"
                      returnKeyType="send"
                      maxLength={15}
                      keyboardType="default"
                      placeholderTextColor={Colors.lightGreyColor}
                      onChangeText={(mobileNo) => this.setState({ mobileNo, isPassword: false, isPressed: false })}
                      ref={(input) => (this.passWord = input)}
                      defaultValue={this.state.mobileNo}
                      onSubmitEditing={() => this._onLoginHandle()}
                    />
                  </View>
                </View>
                <TouchableOpacity onPress={() => { this._onLoginHandle() }} style={{ alignSelf: 'center', flexDirection: 'row' }}>
                  <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 12, marginLeft: Dims.DeviceWidth / 3, color: Colors.Primary }}>Request OTP</Text>
                  <Text style={{ width: 30, height: 30, borderRadius: 50, backgroundColor: Colors.Primary, textAlign: 'center', left: 15, top: -5 }}>
                    <MaterialIcons
                      name="arrow-right-alt"
                      size={28}
                      color={Colors.White}
                    // style={{ top:-5 }}
                    /></Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={{ flexDirection: 'column', position: 'absolute', bottom: 0 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', position: 'absolute', bottom: 0 }}>
              <View style={{ flex: 0.5, justifyContent: 'flex-start', zIndex: 9999999999, left: '-5%' }}>
                <Image
                  source={require('../assets/images/nurse.png')}
                  style={[styles.nurseLogo, {}]}
                  resizeMode="cover"
                />
              </View>
              <TouchableOpacity onPress={() => this.onNavigateHandle()} style={{ flex: 0.5, alignSelf: 'flex-start', top: '30%', right: '15%' }}>
                <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 11, color: Colors.placeholderColor }}>Not registered yet?<Text style={{ color: Colors.Primary, textDecorationLine: 'underline' }}> Create an Account</Text></Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ backgroundColor: Colors.Primary, borderTopLeftRadius: 60, borderTopRightRadius: 60, height: 50 }}>
            <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.White, fontSize: 9, textAlign: 'right', top: 30, right: 20 }}><Text style={{ fontFamily: 'Poppins-Bold', textDecorationLine: 'underline' }}>Our Terms & Conditions</Text>  </Text>

          </View>
        </ScrollView>

        <DropdownAlert
          ref={(ref) => (this.dropdown = ref)}
          containerStyle={{
            backgroundColor: '#FF0000',
          }}
          imageSrc={'https://facebook.github.io/react/img/logo_og.png'}
        />
      </>
    );
  }
}

const styles = StyleSheet.create({
  dotView: {
    flexDirection: 'row',
    alignSelf: 'flex-end'
  },
  logo: {
    height: Dims.DeviceHeight / 4 - 100,
    width: Dims.DeviceWidth - 50
  },
  nurseLogo: {
    height: Dims.DeviceHeight / 2.5,
    width: Dims.DeviceWidth / 2
    // width:130

  },
  welcomeText:
    { fontFamily: 'Poppins-Medium', fontSize: 12, color: Colors.Primary, letterSpacing: 2, top: 10, marginLeft: '7%', marginBottom: 10 },
  defaultInput: {
    color: Colors.Black,
    fontSize: 12,
    top: 10,
    justifyContent: 'center',
    width: 80

  },
  textInputView: {
    paddingLeft: 15,
  },
  container: {
    flexDirection: 'row',
  },
  innerContainer: {
    flexDirection: 'row',
    alignItems: 'stretch',

  },
  hidePasswordText: {
    fontSize: 16,
    textAlign: 'center', fontFamily: 'Poppins-Medium',
    flex: 0.9
  },
  headerFooterContainer: {
    alignItems: 'center',
  },
  inputText: {
    alignSelf: 'center',
    width: Dims.DeviceWidth * 0.5,
    height: 50,
    borderColor: Colors.lightGreyColor,
    borderWidth: 1,
    fontSize: 12,
    textAlign: 'center',
    backgroundColor: '#fcfcfc',
    fontFamily: 'Poppins-Medium',
    borderTopRightRadius: 30,
    borderBottomRightRadius: 30
  },
  countryModalStyle: {
    // width:80
  }
});

export default Login;
