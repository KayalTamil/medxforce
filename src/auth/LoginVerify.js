import React, { Component } from 'react';
import {
    Text,
    View,
    StatusBar,
    Image, StyleSheet, BackHandler, TouchableOpacity, TextInput, KeyboardAvoidingView, ScrollView, Keyboard, SafeAreaView, Alert
} from 'react-native';
import { Colors } from '../assets/Colors';
import { Dims } from '../components/Dims';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as RNLocalize from 'react-native-localize';
import { CustomPicker } from 'react-native-custom-picker';
import nodeEmoji from 'node-emoji';
import { getAllCountries } from 'react-native-country-picker-modal';
import OTPTextView from 'react-native-otp-textinput';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { HttpHelper } from '../HelperApi/Api/HTTPHelper';
import { LoginUrl, LoginOtpUrl } from '../HelperApi/Api/APIConfig';
import * as Animatable from 'react-native-animatable';
import RNOtpVerify from 'react-native-otp-verify';

class LoginVerify extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;

        this.state = {
            options: [],
            mobileNumber: params && params.mobileNo ? params.mobileNo : '',
            otpInput: '',
            keyboardState: 'closed',
            value: params && params.value ? params.value : {},
            timer: null,
            counter: 0
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
        let timer = setInterval(this.tick, 1000);
        this.setState({ timer });
        this.startListeningForOtp()
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
        clearInterval(this.state.timer);

    }
    tick = () => {
        this.setState({
            counter: this.state.counter + 1
        });
    }
    _keyboardDidShow = () => {
        this.setState({ keyboardState: 'opened' });
    }

    _keyboardDidHide = () => {
        this.setState({ keyboardState: 'closed' });
    }
    handleBackPress = () => {
        this.props.navigation.navigate('Login')
        return true;
    };
    getHash = () =>
        RNOtpVerify.getHash()
            .then(console.log)
            .catch(console.log);

    startListeningForOtp = () =>
        RNOtpVerify.getOtp()
            .then(p => {
                console.log(p, 'llllllll');
                RNOtpVerify.addListener(this.otpHandler)
            }
            )
            .catch(p => console.log(p));

    otpHandler = (message) => {
        console.log(message, 'ppppp');
        const otpInput = /(\d{4})/g.exec(message)[1];
        this.setState({ otpInput });
        RNOtpVerify.removeListener();
        Keyboard.dismiss();
    }

    componentWillUnmount() {
        RNOtpVerify.removeListener();
    }

    onVerifyHandle = () => {
        let { otpInput, mobileNumber, value } = this.state;
        if (mobileNumber && otpInput.length === 4 && value) {
            this.props.navigation.navigate('Location');
            let mobileNo = '+' + value.callingCode[0] + mobileNumber
            let obj = {
                "deviceInfo": {
                    "deviceId": "1545874445",
                    "deviceType": "DEVICE_TYPE_ANDROID",
                    "notificationToken": "6767546444dddd5454"
                },
                "username": mobileNo,
                "password": otpInput
            }
            obj = JSON.stringify(obj)
            let loginData = HttpHelper(LoginOtpUrl, 'POST', obj);
            loginData.then(response => {
                if (response) {
                    this.props.navigation.navigate('Location');
                } else {
                    this.dropdown.alertWithType('error', 'Error!', response.data)
                }
            })
        } else {
            this.dropdown.alertWithType('error', 'Error!', 'Please enter mobile number')
        }
    }


    render() {
        let { counter } = this.state;
        return (
            <>
                <StatusBar style={{ backgroundColor: Colors.White }} />
                <SafeAreaView backgroundColor={Colors.White} />
                <ScrollView contentInsetAdjustmentBehavior="automatic" keyboardShouldPersistTaps="always">
                    <View style={[styles.dotView, { justifyContent: 'space-between' }]}>
                        <TouchableOpacity onPress={() => { this.handleBackPress() }} style={[styles.dotView, { top: 20, width: 150 }]}>
                            <Text style={{ marginLeft: 15, top: 8 }}>
                                <Fontisto
                                    name="arrow-left-l"
                                    size={20}
                                    color={Colors.Primary}
                                />
                            </Text>
                            <Text style={{ left: 10 }}>
                                <Image
                                    source={require('../assets/images/logo.png')}
                                    style={styles.headerLogo}
                                    resizeMode="contain"
                                />
                            </Text>
                        </TouchableOpacity>
                        <View style={[styles.dotView, { top: 20 }]}>
                            <Entypo
                                name="dot-single"
                                size={40}
                                color={Colors.Primary}
                            />
                            <Entypo
                                name="dot-single"
                                size={40}
                                color={Colors.Primary}
                                style={{ marginLeft: -15 }}
                            />
                            <Entypo
                                name="dot-single"
                                size={40}
                                color={Colors.placeholderColor}
                                style={{ marginLeft: -15 }}
                            />
                        </View>
                    </View>
                    <View style={{ left: 20, height: 10, top: 40, zIndex: 99999 }}>
                        <Animatable.Image
                            iterationCount={100}
                            ref={"close"}
                            delay={50}
                            animation={"bounce"}
                            source={require('../assets/images/phone.png')}
                            style={styles.logo}

                            resizeMode="contain"
                        />
                        {/* <Image
                            
                            
                            resizeMode="contain"
                        /> */}
                    </View>
                    <View style={{ marginHorizontal: 20, height: Dims.DeviceHeight / 3 }}>
                        <Image
                            source={require('../assets/images/alert.png')}
                            style={styles.logo1}
                            resizeMode="contain"
                        />
                    </View>
                    <View style={{ height: Dims.DeviceHeight / 1.5, backgroundColor: Colors.Primary, borderTopLeftRadius: 60, borderTopRightRadius: 60 }}>
                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('Login') }}>
                            <Text style={{ fontFamily: 'Poppins-Bold', letterSpacing: 5, color: Colors.White, textAlign: 'center', margin: 20, }}>LOGIN VERIFICATION</Text>
                            <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.White, textAlign: 'center', margin: 10 }}>Enter the <Text style={{ fontFamily: 'Poppins-Bold' }}>One Time Password</Text>{'\n'}sent to {this.state.mobileNumber}  <Entypo
                                name="edit"
                                size={15}
                                color={Colors.White}
                            /></Text>
                        </TouchableOpacity>
                        <OTPTextView
                            handleTextChange={(text) => this.setState({ otpInput: text })}
                            containerStyle={styles.textInputContainer}
                            textInputStyle={styles.roundedTextInput}
                            inputCount={4}
                            inputCellLength={1}
                            tintColor={Colors.White}
                            offTintColor={Colors.lightWhiteColor}
                        />
                        <TouchableOpacity onPress={() => { this.onVerifyHandle() }} style={{ flexDirection: 'row', justifyContent: 'center', marginTop: Dims.DeviceHeight * 0.03 }}>
                            <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 16, color: Colors.White, width: 150 }}></Text>
                            <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 14, color: Colors.White, }}>Verify</Text>
                            <Text style={{ alignSelf: 'center', width: 30, height: 30, borderRadius: 50, backgroundColor: Colors.White, textAlign: 'center', left: 20, top: -5 }}>
                                <MaterialIcons
                                    name="arrow-right-alt"
                                    size={28}
                                    color={Colors.Primary}
                                // style={{top: -15 }}
                                /></Text>
                        </TouchableOpacity>
                        {counter <= 10 ? (
                            <Text style={{ textAlign: 'center', fontFamily: 'Poppins-Bold', color: Colors.White }}>Resend OTP in{'  '} <Text style={{ color: Colors.redColor }}>{this.state.counter}s</Text></Text>) : (
                            <Text>''</Text>
                        )}

                        {counter >= 10 ? (
                            <TouchableOpacity>

                                <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.White, margin: 10, left: 30, top: Dims.DeviceHeight * 0.04, fontSize: 12 }}>Didn't receive OTP? <Text style={{ fontFamily: 'Poppins-Bold', textDecorationLine: 'underline' }}>Click to resend</Text></Text>


                            </TouchableOpacity>
                        ) :
                            <Text></Text>}

                    </View>
                </ScrollView>
                <View style={this.state.keyboardState === "opened" ? {
                    position: 'absolute',
                    flex: 1,
                    top: Dims.DeviceHeight - 10,
                    bottom: 0,
                    width: '100%',

                } : {
                    position: 'absolute',
                    flex: 1,
                    bottom: 0,
                    width: '100%',
                    backgroundColor: Colors.Primary,
                    height: 50,
                    justifyContent: 'center'
                }}>

                    <Text style={{ fontFamily: 'Poppins-Medium', color: Colors.White, fontSize: 9, textAlign: 'center' }}><Text style={{ fontFamily: 'Poppins-Bold', textDecorationLine: 'underline' }}>Our Terms & Conditions</Text>  </Text>

                </View>
                <DropdownAlert
                    ref={(ref) => (this.dropdown = ref)}
                    containerStyle={{
                        backgroundColor: '#FF0000',
                    }}
                    imageSrc={'https://facebook.github.io/react/img/logo_og.png'}
                />
            </>
        );
    }
}

const styles = StyleSheet.create({
    dotView: {
        flexDirection: 'row',
        // height: 50
    },
    backView: {
        flexDirection: 'row',
    },
    logo: {
        height: Dims.DeviceHeight / 5,
        width: Dims.DeviceWidth - 50
    },
    logo1: {
        height: Dims.DeviceHeight / 3.5,
        width: Dims.DeviceWidth - 50
    },
    headerLogo: {
        height: 28,
        width: 120,
        // bottom: 10
    },
    nurseLogo: {
        height: 350,
        left: -170
    },
    welcomeText: { fontFamily: 'Poppins-Medium', fontSize: 16, color: Colors.Primary, letterSpacing: 4, top: 10 },
    defaultInput: {
        color: Colors.Black,
        fontSize: 12,
        top: 10,
        justifyContent: 'center',

    },
    textInputView: {
        paddingLeft: 15,
    },
    container: {
        flexDirection: 'row',
    },
    innerContainer: {
        flexDirection: 'row',
        alignItems: 'stretch',

    },
    hidePasswordText: {
        fontSize: 16,
        textAlign: 'center', fontFamily: 'Poppins-Medium',
        flex: 0.9
    },
    headerFooterContainer: {
        alignItems: 'center',
    },
    inputText: {
        alignSelf: 'center',
        width: Dims.DeviceWidth * 0.5,
        height: 50,
        borderColor: Colors.placeholderColor,
        borderWidth: 1,
        fontSize: 16,
        textAlign: 'center',
        backgroundColor: '#fcfcfc',
        fontFamily: 'Poppins-Medium',
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30
    },
    textInputContainer: {
        marginHorizontal: 50,
    },
    roundedTextInput: {
        height: 70,
        fontFamily: 'Poppins-Medium',
        color: Colors.white
    },
});

export default LoginVerify;
