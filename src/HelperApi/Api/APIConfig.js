import Config from './Config'

let url = Config.authUrl;
let dashUrl = Config.dashUrl;
let agoraaUrl = Config.agaroUrl;
let personsUrl = Config.personUrl;

export const RegisterUrl = url + "register";
export const LoginUrl = url + "mobile/login/";
export const LoginOtpUrl = url + "mobile/login/verify/";
export const RegisterMobileUrl = url + "mobile/registration/";
export const RegisterMobileVerifyUrl = url + "mobile/registration/verify/";
export const CategoryUrl = dashUrl + 'findNames';
export const AgoraUrl = agoraaUrl;
export const FindAllUrl = dashUrl + 'findAll';
export const PersonUrl = personsUrl + 'service?';
export const AgoraRetriveUrl = 'https://medxforce.herokuapp.com/medx/api/agora/retrieveToken';

export const RegisterOtpUrl = url + "mobile/otp/+919999999999";

