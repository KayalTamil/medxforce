import React, { useState, Component } from 'react';
import { TouchableOpacity, FlatList, Dimensions, Picker, TextInput, BackHandler, Alert, AppRegistry, StyleSheet, ScrollView, Text, Image, View } from 'react-native';
import Icon from 'react-native-vector-icons/Fontisto';
import { Colors } from '../assets/Colors';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Ionicons from 'react-native-vector-icons/Ionicons';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Dims } from '../components/Dims';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DeviceInfo from 'react-native-device-info';
const { width: screenWidth } = Dimensions.get('window');
import { Card } from 'react-native-shadow-cards';
import { color } from 'react-native-reanimated';


class Footer extends Component {
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            getUser: ''
        }
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }
    render() {
        let { getUser } = this.state;
        return (
            <>
                <Card style={styles.loginBtn2}>
                    
                    <TouchableOpacity onPress ={() =>{this.props.navigation.navigate('Home')}} style={{ width: '20%', left: 10 }}>
                        <Entypo
                            name='home'
                            color={this.props.home ? Colors.Primary : Colors.footerTextColor}
                            size={20} style={{ justifyContent: 'center', marginTop: '11%', left: 4 }} />
                        <Text style={this.props.home ?[styles.footerText,{color:Colors.Primary}] :[styles.footerText,{color:Colors.footerTextColor}]}>{'Home'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress ={() =>{this.props.navigation.navigate('History')}} style={{ width: '20%', left: 10 }}>
                        <Fontisto
                            name='calculator'
                            color={this.props.history ? Colors.Primary : '#96a2ae'}
                            size={20} style={{ justifyContent: 'center', marginTop: '11%', left: 8 }} />
                        <Text style={this.props.history ? { fontSize: 10, width: '100%', color: Colors.Primary, marginTop: '7%', alignSelf: 'center', fontFamily: 'Poppins-Bold' } : { fontSize: 10, width: '100%', color: Colors.footerTextColor, marginTop: '7%', alignSelf: 'center', fontFamily: 'Poppins-Bold' }}>{'HIstory'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.imabot} onPress={() => this.props.navigation.navigate('Home')}>
                        <Image resizeMode={"contain"}
                            source={require("../assets/images/bottomLogo.png")} style={{
                                height: "100%",
                                width: "100%",
                            }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ width: '20%', left: 35 }}>
                        <Ionicons
                            name='md-notifications'
                            color='#96a2ae'
                            size={20} style={{ justifyContent: 'center', marginTop: '11%', left: 8 }} />
                        <Text style={{ fontSize: 10, width: '100%', color: '#96a2ae', marginTop: '7%', left: -10, fontFamily: 'Poppins-Bold' }}>{'Notifications'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ width: '20%', left: 25 }}>
                        <FontAwesome
                            name='user-o'
                            color='#96a2ae'
                            size={20} style={{ justifyContent: 'center', marginTop: '11%', left: 8 }} />
                        <Text style={{ fontSize: 10, width: '100%', color: '#96a2ae', marginTop: '7%', alignSelf: 'center', fontFamily: 'Poppins-Bold' }}>{'Profile'}</Text>
                    </TouchableOpacity>
                </Card>

            </>
        );
    }
};

export default Footer

const styles = StyleSheet.create({
    footerText:{ fontSize: 10, width: '100%',  marginTop: '7%', alignSelf: 'center', fontFamily: 'Poppins-Bold' },
    imabot: {
        height: "80%",
        width: "15%",
    },
    imabots: {
        height: "100%",
        width: "100%",
        borderRadius: 95,
    },
    loginBtn2: {
        height: 60,

        justifyContent: 'space-evenly',
        alignItems: 'center',
        position: 'absolute',
        flex: 1,
        bottom: 0,
        width: '100%',

        flexDirection: 'row',
    },
    loginBtn: {
        flex: 0.35,
        height: 60,
        borderWidth: 2,
        borderColor: Colors.borderColor,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.white,
        alignContent: 'center',
    },
    red: {
        // marginTop: 40,
        fontSize: 20,
        color: '#C81111',
        fontWeight: 'bold',
    },
    btn: {
        fontSize: 12,
        fontFamily: 'Poppins-Medium',
        color: Colors.White
    },
    app: {
        backgroundColor: '#2f4f4f',
    },
    row: {
        flexDirection: 'row',
    },
    column: {
        flexDirection: 'column',
    },
    mainview: {
        backgroundColor: '#F9F9F9'
    },
    ImageStyle: {
        padding: 10,
        margin: 0,
        height: 5,
        width: 5,
        alignItems: 'center'
    },
    ImageStyle1: {
        padding: 10,
        margin: 0,
        height: 5,
        width: 5,
        alignItems: 'center'
    },
    ImageStyle2: {
        padding: 10,
        margin: 50,
        height: 100,
        width: 100,
        alignItems: 'center'
    },
    wishlist: {
        justifyContent: "space-between",
        backgroundColor: "#F7861E",
        padding: 5,
        margin: 0,
        borderRadius: 10,
        flexDirection: 'row'
    },
});