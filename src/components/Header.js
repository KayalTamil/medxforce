import React, { Component } from 'react';
import {
  Text,
  View,
  StatusBar,
  Image, StyleSheet, BackHandler, TouchableOpacity, Alert, TextInput, FlatList, DeviceEventEmitter, KeyboardAvoidingView, Switch
} from 'react-native';
import { Colors } from '../assets/Colors';
import { Dims } from '../components/Dims';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as RNLocalize from 'react-native-localize';
import Geolocation from 'react-native-geolocation-service';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Footer from '../components/Footer';
import { Card } from 'react-native-shadow-cards';
import { HttpHelper } from '../HelperApi/Api/HTTPHelper';
import { LocationUrl } from '../HelperApi/Api/APIConfig';
var moment = require('moment');
import Geocoder from 'react-native-geocoder';
import SplashScreen from 'react-native-splash-screen';



class Header extends Component {
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    this.state = {
      locationAddress: []
    };
  }

  componentDidMount() {
    SplashScreen.hide();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    let locationData = HttpHelper(`${LocationUrl}/8054140508766732296`, 'GET', '');
    locationData.then(locationResponse => {
      console.log(locationResponse.lattitude, '11');
      if (locationResponse) {
        var dataLatLng = {
          lat: Number(locationResponse.lattitude),
          lng: Number(locationResponse.longitude)
        };
        Geocoder.geocodePosition(dataLatLng).then(responseJson => {
          console.log(responseJson, 'responseJson');
          if (responseJson) {
            this.setState(
              {
                locationAddress: responseJson && responseJson[0] ? responseJson[0] : '',
                selectAddress: responseJson && responseJson[0] ? responseJson[0] : '',
                getDataAddress: responseJson && responseJson[0] ? responseJson[0] : [],
                isSelect: true,
                position: responseJson[0].position
              });
          } else {
          }
        })
          .catch(err => console.log(err))
      }
    })

  }





  render() {
    let { locationAddress, getBannerImage } = this.state;
    let locatedAddress = [locationAddress.locality, locationAddress.country, locationAddress.adminArea];
    return (
      <>
        <StatusBar style={{ backgroundColor: Colors.Primary }} />

        <View style={{ height: 70, backgroundColor: Colors.Primary, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text onPress={() => this.props.navigation.navigate(this.props.pageName)} style={{ alignSelf: 'center', marginLeft: 10, fontFamily: 'Poppins-Medium', color: Colors.White, }}>
            <AntDesign
              name={!this.props.listView ? "menu-unfold" : 'arrowleft'}
              size={25}
              color={Colors.White}
              style={{}}
            /></Text>
          {(this.props.history || this.props.listView) ? (
            <View style={{ justifyContent: 'center' }}>
              <Text style={{ color: Colors.White, fontFamily: 'Poppins-Medium', }}>{this.props.title}</Text>
            </View>
          ) :

            <View style={{ flexDirection: 'column', paddingVertical: 5 }}>
              <Text style={{ textAlign: 'center', color: Colors.White, fontFamily: 'Poppins-Medium' }}>Current Locations</Text>

              <Text numberOfLines={2} style={{ maxWidth: 220, textAlign: 'center', color: Colors.White, fontFamily: 'Poppins-Medium', }}>{locatedAddress ? locatedAddress.join(',') : ''}</Text>
            </View>
          }

          <View style={{ flexDirection: 'column', marginRight: 10, alignSelf: 'center' }}>
            <Text style={{ textAlign: 'center' }}>
              <FontAwesome5
                name="headset"
                size={20}
                color={Colors.White}
                style={{}}
              /></Text>
            <Text style={{ textAlign: 'center', color: Colors.White, fontFamily: 'Poppins-Medium', fontSize: 10 }}>Help me</Text>
          </View>
        </View>

        <DropdownAlert
          ref={(ref) => (this.dropdown = ref)}
          containerStyle={{
            backgroundColor: '#FF0000',
          }}
          imageSrc={'https://facebook.github.io/react/img/logo_og.png'}
        />
      </>
    );
  }
}

const styles = StyleSheet.create({
  dotView: {
    flexDirection: 'row',
    alignSelf: 'flex-end'
  },
  logo: {
    height: 120,
    width: 120,
    borderRadius: 100,
  },
  nurseLogo: {
    height: 280,
    left: -150
  },
  welcomeText: { fontFamily: 'Poppins-Medium', fontSize: 16, color: Colors.Primary, letterSpacing: 4, top: 10 },
  defaultInput: {
    color: Colors.Black,
    fontSize: 12,
    top: 10,
    justifyContent: 'center',

  },
  textInputView: {
    paddingLeft: 15,
  },
  container: {
    flexDirection: 'row',
  },
  innerContainer: {
    flexDirection: 'row',
    alignItems: 'stretch',

  },
  hidePasswordText: {
    fontSize: 16,
    textAlign: 'center', fontFamily: 'Poppins-Medium',
    flex: 0.9
  },
  headerFooterContainer: {
    alignItems: 'center',
  },
  inputText: {
    alignSelf: 'center',
    width: Dims.DeviceWidth * 0.5,
    height: 50,
    borderColor: Colors.placeholderColor,
    borderWidth: 1,
    fontSize: 16,
    textAlign: 'center',
    backgroundColor: '#fcfcfc',
    fontFamily: 'Poppins-Medium',
    borderTopRightRadius: 30,
    borderBottomRightRadius: 30
  },
});

export default Header;
