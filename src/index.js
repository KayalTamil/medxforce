import 'react-native-gesture-handler';
import React, { Component } from 'react';

import { createAppContainer, createSwitchNavigator } from 'react-navigation';
// Import Screens
import Login from '../src/auth/Login';
import Register from '../src/auth/Register';
import LoginVerify from '../src/auth/LoginVerify';
import SubScription from '../src/auth/SubScription';
import Home from '../src/container/Home';
import Location from '../src/container/Location';
import RegisterLocation from '../src/auth/registerLocation';
import MapLocation from '../src/container/MapLocation';
import UserLocation from '../src/container/userLocation';
import OrderStatus from '../src/container/OrderStatus';
import Slider from '../src/container/Slider';
import ListView from '../src/container/ListView';
import History from '../src/container/History';
import CallScreen from '../src/container/CallSdk';

const App = createSwitchNavigator({
    Home: {
        screen: Home,
    },
    Slider: {
        screen: Slider,
    },
    ListView: {
        screen: ListView,
    },
    
    Location: {
        screen: Location,
    },
    UserLocation: {
        screen: UserLocation,
    },
    
    OrderStatus: {
        screen: OrderStatus,
    },
    
    Location: {
        screen: Location,
    },
    Login: {
        screen: Login,
    },
    Location: {
        screen: Location,
    },
  
    LoginVerify: {
        screen: LoginVerify,
    },
    SubScription: {
        screen: SubScription,
    },

    Location: {
        screen: Location,
    },
    RegisterLocation: {
        screen: RegisterLocation,
    },
    Register: {
        screen: Register,
    },
   
    History: {
        screen: History,
    },
    CallScreen: {
        screen: CallScreen,
    },
})

export default createAppContainer(App);
